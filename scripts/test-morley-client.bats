#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

setup_file() {
  export tempdir="$(mktemp -d --tmpdir="$PWD")"
  # Note: can't export a bash array from setup_file, so we set it twice
  tezos_client_args=(-E "$TASTY_CLEVELAND_NODE_ENDPOINT" -d "$tempdir")

  export sender="test-morley-client-sender"
  export receiver="test-morley-client-receiver"

  tezos-client ${tezos_client_args[@]} gen keys $sender -f
  tezos-client ${tezos_client_args[@]}  gen keys $receiver -f

  tezos-client "${tezos_client_args[@]}" import secret key moneybag "$TASTY_CLEVELAND_MONEYBAG_SECRET_KEY" --force

  tezos-client "${tezos_client_args[@]}" transfer 1 from moneybag to $sender --burn-cap 1
}

# TODO: remove when bats-1.2.1 or newer is available in LTS of most
# popular Linux distros
setup() {
  if [ -z "$tempdir" ]; then
    echo "# You seem to be using bats-1.2.0 or earlier. This will take a while." >&3
    echo "# to run this test faster (and cleaner), use bats-1.2.1 or newer" >&3
    setup_file
  fi
}

# Note: can't export a bash array from setup_file, so we set it twice
tezos_client_args=(-E "$TASTY_CLEVELAND_NODE_ENDPOINT" -d "$tempdir")

password="12345"
confirm_password=$(cat <<EOF
"$password"
"$password"
EOF
)
enter_password=$(cat <<EOF
"$password"
EOF
)

@test "Morley test implicit transfer logic with 'Unit' parameter" {
    morley-client "${tezos_client_args[@]}" transfer \
                  --from "$sender" \
                  --to "$receiver" --amount 1 --parameter 'Unit'
}

@test "Morley test implicit transfer logic with non-'Unit' parameter" {
    run morley-client "${tezos_client_args[@]}" transfer \
        --from "$sender" \
        --to "$receiver" --amount 1 --parameter 0
    [ "$status" -ne 0 ]
}

@test "Morley test originated contract transfer logic" {
    contract_alias="fresh-contract"
    morley-client "${tezos_client_args[@]}" originate \
                  --contract ./contracts/tezos_examples/attic/add1.tz \
                  --contract-name "$contract_alias" \
                  --initial-storage '0' \
                  --from "$sender"

    morley-client "${tezos_client_args[@]}" transfer \
                  --from "$sender" \
                  --to "$contract_alias" \
                  --amount 1 \
                  --parameter 1
}

@test "Morley test manual fee provision" {
    run morley-client "${tezos_client_args[@]}" transfer \
        --from "$sender" \
        --to "$receiver" --amount 1 --parameter 0 --fee 1000000
    [ "$status" -ne 0 ]
}

@test "Morley-client should report the same balance as tezos-client" {
    # A balance >= 4294.967296 ꜩ is required to trigger #302.
    morley_client_balance=$(morley-client "${tezos_client_args[@]}" get-balance --addr "$sender")
    tezos_client_balance=$(tezos-client "${tezos_client_args[@]}" get balance for "$sender")
    echo "morley_client_balance=${morley_client_balance}" > test-morley-client.log
    echo "tezos_client_balance=${tezos_client_balance}" >> test-morley-client.log
    [ "$morley_client_balance" = "$tezos_client_balance" ]
}

@test "morley-client work with password protected accounts" {
   echo "$confirm_password" | tezos-client "${tezos_client_args[@]}" gen keys password-protected --force --encrypted
   tezos-client "${tezos_client_args[@]}" transfer 1 from moneybag to password-protected --burn-cap 0.257
   echo "$enter_password" | tezos-client "${tezos_client_args[@]}" reveal key for password-protected
   echo "$enter_password" | morley-client "${tezos_client_args[@]}" transfer \
     --from password-protected --to moneybag --amount 1 --parameter Unit
}

# Simple smoke tests to check that the command generally works
@test "get-block-header" {
    morley-client "${tezos_client_args[@]}" get-block-header # default
    morley-client "${tezos_client_args[@]}" get-block-header --block-id head # head
    morley-client "${tezos_client_args[@]}" get-block-header --block-id genesis # genesis
    morley-client "${tezos_client_args[@]}" get-block-header --block-id 100 # level
    morley-client "${tezos_client_args[@]}" get-block-header --block-id head~100 # depth
    head="$(morley-client "${tezos_client_args[@]}" get-block-header --block-id head | sed -rn 's/.*"hash":"([^"]+)".*/\1/p')"
    morley-client "${tezos_client_args[@]}" get-block-header --block-id "$head" #hash
}

@test "get-block-header-invalid" {
    run morley-client "${tezos_client_args[@]}" get-block-header --block-id 'invalid-hash'
    [[ "$output" == *"failed to parse block ID, try passing block's hash, level or 'head'"* ]]
    [ $status -ne 0 ]
}

# Simple smoke tests to check that the command generally works
@test "get-block-operations" {
    morley-client "${tezos_client_args[@]}" get-block-operations
    morley-client "${tezos_client_args[@]}" get-block-operations --block-id genesis
    morley-client "${tezos_client_args[@]}" get-block-operations --block-id 100
    morley-client "${tezos_client_args[@]}" get-block-operations --block-id head~100
}
