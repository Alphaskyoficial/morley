#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

set -euo pipefail

docker_dir="$HOME/.morley"
mnt_dir="/mnt"
mkdir -p "$docker_dir"
mkdir -p "$docker_dir/db"
docker_image="registry.gitlab.com/morley-framework/morley"

maybe_pull_image() {
    if [[ ! -f "$docker_pull_timestamp"
           || 3600 -le $(($(date "+%s") - $(cat "$docker_pull_timestamp"))) ]];
    then
        pull_image
    fi
}

pull_image() {
    docker pull "$docker_image"
    date "+%s" >| "$docker_pull_timestamp"
}

if ! docker -v > /dev/null 2>&1 ; then
    echo "Docker does not seem to be installed."
    exit 1
fi

newargs=()
for arg in "$@"
do
    if [[ "$arg" = "--latest" ]];
    then
        latest_flag=true
    else
        newargs+=("$arg")
    fi
done

# Drop --latest from arguments if it is presented
set -- "${newargs[@]+"${newargs[@]}"}"

# We use MORLEY_IMAGE env variable in order to define custom local morley image.
# This functionality is mostly used for testing script and docker image
# for current version of morley source code
if [[ -n "${MORLEY_IMAGE-}" ]];
then
    docker_image="$MORLEY_IMAGE"
else
    if [[ -n "${latest_flag-}" ]];
    then
        docker_image="$docker_image:latest"
        docker_pull_timestamp="$docker_dir/docker_pull_latest.timestamp"
    else
        docker_image="$docker_image:production"
        docker_pull_timestamp="$docker_dir/docker_pull_production.timestamp"
    fi
fi

if [[ -z "${MORLEY_IMAGE-}" ]];
then
    maybe_pull_image
fi

manpage() {
    docker run --rm "$docker_image" morley --help
    echo ""
    echo "In order to be able to pass files with stack information inside 'repl' command"
    echo "you should pass '--map-dir <filepath>'. Thus given directory will be"
    echo "associated with /<directory-name> directory inside the docker container."
    echo "For example: 'morley.sh repl --map-dir stacks'."
    echo "After that you can work with files inside '/stacks' directory, e.g."
    echo "':dumpstack stacks/stack.json'. Note, that stack snapshot files that"
    echo "aren't located in the 'stacks' directory will be deleted once the repl is finished."
    echo ""
    echo "You can pass --docker_debug to see additional informations such as"
    echo "arguments that are being passed to docker run"
    echo ""
    echo "You can pass --latest to use docker image that is built from latest version"
    echo "of master branch. Otherwise latest version from production branch"
    echo "will be used"
}

if [[ "$#" -eq 0 || "$#" -eq 1 && "$1" = "--latest" ]];
then
    manpage
    exit 0
fi

typeset -a args
typeset -a docker_args

subcommand="$1"
shift
args+=("$subcommand")
default_db_filepath=".db.json"
docker_args=()
# ^ Default json database stored in ~/.morley
while true;
do
  arg="${1-}"
  if [[ -z "$arg" ]];
  then
      break
  fi
  case $arg in
    --contract )
        contract_filepath="$2"
        contract_bn=$(basename "$contract_filepath")
        mkdir -p "$docker_dir/contract/"
        cp "$contract_filepath" "$docker_dir/contract/$contract_bn"
        args+=("$arg" "$mnt_dir/contract/$contract_bn")
        shift 2
        ;;
    --output | -o)
        output_filepath="$2"
        output_bn=$(basename "$output_filepath")
        mkdir -p "$docker_dir/output/"
        args+=("$arg" "$mnt_dir/output/$output_bn")
        shift 2
        ;;
    --db )
        user_db_filepath="$2"
        shift 2
        ;;
    --docker_debug )
        debug_flag=true
        shift
        ;;
    --map-dir )
        dname="$(basename "$2")"
        mkdir -p "$2"
        filepath="$(realpath "$2")"
        docker_args+=("-v" "$filepath:/$dname")
        shift 2
        ;;
    * )
        args+=("$arg")
        shift
  esac
done
if [[ "${user_db_filepath-}" != "" ]];
then
    dn=$(dirname "$user_db_filepath")
    mkdir -p "$docker_dir/db/$dn"
    touch "$user_db_filepath"
    # ^ Touch in case given user_db_filepath doesn't exist
    ln "$user_db_filepath" "$docker_dir/db/$user_db_filepath"
fi
if [[ "${user_db_filepath-}" = "" ]];
then
    user_db_filepath="$default_db_filepath"
fi

if [[ "$subcommand" == "run"  ||  "$subcommand" == "originate"  ||  "$subcommand" == "transfer" ]];
then
    args+=("--db" "$mnt_dir/db/$user_db_filepath")
fi
if [[ -n "${debug_flag-}" ]];
then
    echo "docker run arguments: ${args[*]}"
fi

set +e
docker run --rm -v "$docker_dir:$mnt_dir" "${docker_args[@]+"${docker_args[@]}"}" -i "$docker_image" morley "${args[@]}"

run_exitcode="$?"
if [[ -n "${output_filepath-}" ]]; then
    cp "$docker_dir/output/$output_bn" "$output_filepath"
fi

set -e

rm -rf "$docker_dir/contract"
rm -rf "$docker_dir/output"
if [[ "$user_db_filepath" != "$default_db_filepath" ]];
then
    rm -rf "$docker_dir/db/"
fi
exit "$run_exitcode"
