#!/usr/bin/env bash

IFS=":"
for p in $PATH; do
  [ -f "$p/$1" ] && [ -x "$p/$1" ] && echo "$p/$1" && exit 0
done
echo "No $1 found in $PATH"
exit 1
