#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

set -e -o pipefail

# This script uploads automatically generated documentation to
# `autodoc/$current_branch` branch.
# This branch should already exist.
# Documentation files are supposed to be in `autodoc/`.

git config --local user.email "hi@serokell.io"
git config --local user.name "CI autodoc generator"
git remote remove auth-origin 2> /dev/null || :
git remote add auth-origin "https://oath2:$AUTODOC_PUSH_TOKEN@gitlab.com/morley-framework/morley.git"
git fetch

our_branch="$CI_COMMIT_REF_NAME"
doc_branch="autodoc/$our_branch"
sha=$(git rev-parse --short HEAD)
# Move documentation files before checkout to avoid conflicts.
mkdir -p upload-autodoc-tmp && mv autodoc upload-autodoc-tmp
git checkout "origin/$doc_branch"
git checkout -B "$doc_branch"
git merge -X theirs "origin/$our_branch" -m "Merge $our_branch"
# Move documentation back.
rm -rf autodoc
mv upload-autodoc-tmp/autodoc . && rm -rf upload-autodoc-tmp
git add ./autodoc
git commit --allow-empty -m "Documentation update for $sha"
git push --set-upstream auth-origin "$doc_branch"
git checkout "@{-2}"
