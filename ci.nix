# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

rec {
  sources = import ./nix/sources.nix;
  # pkgs for builtins.currentSystem
  pkgs = import ./nix/nixpkgs-with-haskell-nix.nix {};
  pkgsStatic = pkgs.pkgsCross.musl64;
  xrefcheck = import sources.xrefcheck;
  weeder-hacks = import sources.haskell-nix-weeder { inherit pkgs; };
  tezos-client = (import "${sources.tezos-packaging}/nix/build/pkgs.nix" {}).ocamlPackages.tezos-client;

  # all local packages and their subdirectories
  # we need to know subdirectories for weeder and for cabal check
  local-packages = [
    { name = "lorentz"; subdirectory = "code/lorentz"; }
    { name = "morley"; subdirectory = "code/morley"; }
    { name = "morley-client"; subdirectory = "code/morley-client"; }
    { name = "morley-large-originator"; subdirectory = "code/morley-large-originator"; }
    { name = "cleveland"; subdirectory = "code/cleveland"; }
    { name = "morley-prelude"; subdirectory = "code/morley-prelude"; }
    { name = "autodoc-sandbox"; subdirectory = "code/autodoc-sandbox"; }
  ];

  # names of all local packages
  local-packages-names = map (p: p.name) local-packages;

  # source with gitignored files filtered out
  projectSrc = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "morley";
    src = ./.;
  };

  # GHC has issues with compiling statically linked executables when Template Haskell
  # is used, this wrapper for ld fixes it. Another solution we could use is to link
  # GHC itself statically, but it seems to break haddock.
  # See https://github.com/input-output-hk/haskell.nix/issues/914#issuecomment-897424135
  linker-workaround = pkgs.writeShellScript "linker-workaround" ''
    # put all flags into 'params' array
    source ${pkgsStatic.stdenv.cc}/nix-support/utils.bash
    expandResponseParams "$@"

    # check if '-shared' flag is present
    hasShared=0
    for param in "''${params[@]}"; do
      if [[ "$param" == "-shared" ]]; then
        hasShared=1
      fi
    done

    if [[ "$hasShared" -eq 0 ]]; then
      # if '-shared' is not set, don't modify the params
      newParams=( "''${params[@]}" )
    else
      # if '-shared' is present, remove '-static' flag
      newParams=()
      for param in "''${params[@]}"; do
        if [[ ("$param" != "-static") ]]; then
          newParams+=( "$param" )
        fi
      done
    fi

    # invoke the actual linker with the new params
    exec x86_64-unknown-linux-musl-cc @<(printf "%q\n" "''${newParams[@]}")
  '';

  # haskell.nix package set
  # parameters:
  # - release -- 'true' for master and producion branches builds, 'false' for all other builds.
  #   This flag basically disables weeder related files production, haddock and enables stripping
  # - static -- build statically linked executables
  # - commitSha, commitDate -- git revision info used during compilation of packages with autodoc
  # - optimize -- 'true' to enable '-O1' ghc flag, we use it for publishing docker image for morley
  hs-pkgs = { release, static ? true, optimize ? false, commitSha ? null, commitDate ? null}:
    let
      haskell-nix = if static then pkgsStatic.haskell-nix else pkgs.haskell-nix;
    in haskell-nix.stackProject {
      src = projectSrc;

      # use .cabal files for building because:
      # 1. haskell.nix fails to work for package.yaml with includes from the parent directory
      # 2. with .cabal files haskell.nix filters out source files for each component, so only the changed components will rebuild
      ignorePackageYaml = true;

      modules = [
        # common options for all local packages:
        {
          packages = pkgs.lib.genAttrs local-packages-names (packageName: {
            ghcOptions = with pkgs.lib;
              # we use O1 for production binaries in order to improve their performance
              # for end-users
              [ (if optimize then "-O1" else "-O0") "-Werror"]
              # override linker to work around issues with Template Haskell
              ++ optionals static [ "-pgml=${linker-workaround}" ]
              # produce *.dump-hi files, required for weeder:
              ++ optionals (!release) ["-ddump-to-file" "-ddump-hi"]
              # do not erase any 'assert' calls
              ++ optionals (!release) ["-fno-ignore-asserts"];
            dontStrip = !release;  # strip in release mode, reduces closure size
            doHaddock = !release;  # don't haddock in release mode

            # in non-release mode collect all *.dump-hi files (required for weeder)
            postInstall = if release then null else weeder-hacks.collect-dump-hi-files;
          });
        }

        {
          # don't haddock dependencies
          doHaddock = false;

          packages.autodoc-sandbox = {
            preBuild = ''
              export MORLEY_DOC_GIT_COMMIT_SHA=${if release then pkgs.lib.escapeShellArg commitSha else "UNSPECIFIED"}
              export MORLEY_DOC_GIT_COMMIT_DATE=${if release then pkgs.lib.escapeShellArg commitDate else "UNSPECIFIED"}
            '';
          };
          # in order to make contracts from ./contracts accessible during 'nix-build'
          packages.cleveland = {
            preBuild = ''
              cp -rT ${projectSrc}/contracts ../../contracts
            '';
          };
        }
      ];
  };

  hs-pkgs-development = hs-pkgs { release = false; };

  # component set for all local packages like this:
  # { morley = { library = ...; exes = {...}; tests = {...}; ... };
  #   morley-prelude = { ... };
  #   ...
  # }
  packages = pkgs.lib.genAttrs local-packages-names (packageName: hs-pkgs-development."${packageName}".components);

  # returns a list of all components (library + exes + tests + benchmarks) for a package
  get-package-components = pkg: with pkgs.lib;
    optional (pkg ? library) pkg.library
    ++ attrValues pkg.exes
    ++ attrValues pkg.tests
    ++ attrValues pkg.benchmarks;

  # per-package list of components
  components = pkgs.lib.mapAttrs (pkgName: pkg: get-package-components pkg) packages;

  # a list of all components from all packages in the project
  all-components = with pkgs.lib; flatten (attrValues components);

  haddock = with pkgs.lib; flatten (attrValues
    (mapAttrs (pkgName: pkg: optional (pkg ? library) pkg.library.haddock) packages));

  # doctests binary for morley, with package-db for running doctests
  morley-doctests =
    let
      # Running doctests fails with "Dynamic loading not supported" error if we
      # compile them statically, so we have to use dynamic build. Sadly, it means
      # that we rebuild GHC and all packages for the sole purpose of running doctests
      hs-pkgs-dynamic = hs-pkgs { release = false; static = false; };
      doctests = hs-pkgs-dynamic.morley.components.tests.doctests;
      configFiles = doctests.passthru.configFiles;
      package-db = "${configFiles}/${configFiles.packageCfgDir}";
    in pkgs.linkFarm "morley-doctest" [
      { name = "bin"; path = "${doctests}/bin"; }
      { name = "package.conf.d"; path = package-db; }
    ];

  # run some executables to produce contract documents
  contracts-doc = { release, commitSha ? null, commitDate ? null }@releaseArgs: pkgs.runCommand "contracts-doc" {
    buildInputs = [
      (hs-pkgs releaseArgs).autodoc-sandbox.components.exes.autodoc-sandbox-registry
    ];
  } ''
    mkdir $out
    cd $out
    mkdir autodoc
    autodoc-sandbox-registry document --name AutodocSandbox --output \
      autodoc/AutodocSandboxContract.md
  '';

  # release version of morley executable
  morley-release = { optimize ? false }:
    (hs-pkgs { release = true; optimize = optimize; }).morley.components.exes.morley;

  # docker image for morley executable
  # 'creationDate' -- creation date to put into image metadata
  # 'optimize' -- compile with optimizations enabled
  morleyDockerImage = { creationDate, tag ? null, optimize ? false }:
    pkgs.dockerTools.buildImage {
    name = "morley";
    contents = morley-release { inherit optimize; };
    created = creationDate;
    inherit tag;
  };

  # nixpkgs has weeder 2, but we use weeder 1
  weeder-legacy = pkgs.haskellPackages.callHackageDirect {
    pkg = "weeder";
    ver = "1.0.9";
    sha256 = "0gfvhw7n8g2274k74g8gnv1y19alr1yig618capiyaix6i9wnmpa";
  } {};

  # a derivation which generates a script for running weeder
  weeder-script = weeder-hacks.weeder-script {
    hs-pkgs = hs-pkgs-development;
    local-packages = local-packages;
    weeder = weeder-legacy;
  };

  # checks if all packages are appropriate for uploading to hackage
  run-cabal-check = pkgs.runCommand "cabal-check" { buildInputs = [ pkgs.cabal-install ]; } ''
    ${pkgs.lib.concatMapStringsSep "\n" ({ name, subdirectory }: ''
      echo 'Running `cabal check` for ${name}'
      cd ${projectSrc}/${subdirectory}
      cabal check
    '') local-packages}

    touch $out
  '';

  # stack2cabal is broken because of strict constraints, set 'jailbreak' to ignore them
  stack2cabal = pkgs.haskell.lib.overrideCabal pkgs.haskellPackages.stack2cabal (drv: {
    jailbreak = true;
    broken = false;
  });
}
