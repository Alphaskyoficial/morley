-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Some conveniences for "Lorentz.StoreClass" module.
--
-- This is not part of the umbrella "Lorentz" module, you have to import
-- this specially.
module Lorentz.StoreClass.Extra
  ( (.)
  ) where

import Prelude ()

import Lorentz.StoreClass

-- | Alias for ':-|'.
--
-- This makes nested field access look just like in other languages.
--
-- Though it may collide with the dot operator from Haskell world,
-- for instance, in tests, so we do not yet provide it directly in
-- "Lorentz.StoreClass".
infixr 8 .
(.) :: FieldRef l -> FieldRef r -> FieldRef (l :-| r)
(.) = (:-|)
