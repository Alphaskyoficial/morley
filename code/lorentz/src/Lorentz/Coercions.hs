-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-unticked-promoted-constructors #-}

-- | Identity transformations between different Haskell types.
module Lorentz.Coercions
  ( -- * Safe coercions
    CanCastTo (..)
  , castDummyG
  , checkedCoerce
  , Castable_
  , Coercible_
  , checkedCoerce_
  , checkedCoercing_
  , allowCheckedCoerceTo
  , allowCheckedCoerce
  , coerceUnwrap
  , unsafeCoerceWrap
  , coerceWrap
  , toNamed
  , fromNamed

    -- * Unsafe coercions
  , MichelsonCoercible
  , forcedCoerce
  , forcedCoerce_
  , gForcedCoerce_
  , fakeCoerce
  , fakeCoercing

    -- * Re-exports
  , Unwrappable (..)
  , Wrappable
  ) where

import qualified Data.Coerce as Coerce
import Data.Constraint ((\\))
import qualified GHC.Generics as G
import Named (NamedF)
import Unsafe.Coerce (unsafeCoerce)

import Lorentz.Address
import Lorentz.Base
import Lorentz.Bytes
import Lorentz.Value
import Lorentz.Wrappable
import Lorentz.Zip
import Morley.Michelson.Typed

----------------------------------------------------------------------------
-- Unsafe coercions
----------------------------------------------------------------------------

-- | Coercion for Haskell world.
--
-- We discourage using this function on Lorentz types, consider using 'Data.Coerce.coerce'
-- instead.
-- One of the reasons forthat is that in Lorentz it's common to declare types as
-- newtypes consisting of existing primitives, and @forcedCoerce@ tends to ignore
-- all phantom type variables of newtypes thus violating their invariants.
forcedCoerce :: Coerce.Coercible a b => a -> b
forcedCoerce = Coerce.coerce

-- | Whether two types have the same Michelson representation.
type MichelsonCoercible a b = ToT a ~ ToT b

-- | Convert between values of types that have the same representation.
--
-- This function is not safe in a sense that this allows
-- * breaking invariants of casted type
--   (example: @UStore@ from morley-upgradeable), or
-- * may stop compile on code changes
--   (example: coercion of pair to a datatype with two fields
--    will break if new field is added).
-- Still, produced Michelson code will always be valid.
--
-- Prefer using one of more specific functions from this module.
forcedCoerce_ :: MichelsonCoercible a b => a : s :-> b : s
forcedCoerce_ = I Nop

gForcedCoerce_ :: MichelsonCoercible (t a) (t b) => t a : s :-> t b : s
gForcedCoerce_ = forcedCoerce_

-- | Convert between two stacks via failing.
fakeCoerce :: s1 :-> s2
fakeCoerce = I (UNIT `Seq` FAILWITH)

fakeCoercing :: (s1 :-> s2) -> s1' :-> s2'
fakeCoercing i = fakeCoerce # iForceNotFail i # fakeCoerce

----------------------------------------------------------------------------
-- Safe coercions
----------------------------------------------------------------------------

-- | Specialized version of 'forcedCoerce_' to unwrap a haskell newtype.
coerceUnwrap
  :: forall a s. Unwrappable a
  => a : s :-> Unwrappabled a : s
coerceUnwrap = forcedCoerce_

-- | Specialized version of 'forcedCoerce_' to wrap a haskell newtype.
--
-- Works under 'Unwrappable' constraint, thus is not safe.
unsafeCoerceWrap
  :: forall a s. Unwrappable a
  => Unwrappabled a : s :-> a : s
unsafeCoerceWrap = forcedCoerce_

-- | Specialized version of 'forcedCoerce_' to wrap into a haskell newtype.
--
-- Requires 'Wrappable' constraint.
coerceWrap
  :: forall a s. Wrappable a
  => Unwrappabled a : s :-> a : s
coerceWrap = forcedCoerce_

-- | Lift given value to a named value.
toNamed :: Label name -> a : s :-> NamedF Identity a name : s
toNamed _ = coerceWrap

-- | Unpack named value.
fromNamed :: Label name -> NamedF Identity a name : s :-> a : s
fromNamed _ = coerceUnwrap

-- Arbitrary coercions
----------------------------------------------------------------------------

-- | Explicitly allowed coercions.
--
-- @a `CanCastTo` b@ proclaims that @a@ can be casted to @b@ without violating
-- any invariants of @b@.
--
-- This relation is reflexive; it /may/ be symmetric or not.
-- It tends to be composable: casting complex types usually requires permission
-- to cast their respective parts; for such types consider using 'castDummyG'
-- as implementation of the method of this typeclass.
--
-- For cases when a cast from @a@ to @b@ requires some validation, consider
-- rather making a dedicated function which performs the necessary checks and
-- then calls 'forcedCoerce'.
class a `CanCastTo` b where
  -- | An optional method which helps passing -Wredundant-constraints check.
  -- Also, you can set specific implementation for it with specific sanity checks.
  castDummy :: Proxy a -> Proxy b -> ()
  castDummy _ _ = ()

-- | Coercion in Haskell world which respects 'CanCastTo'.
checkedCoerce :: forall a b. (CanCastTo a b, Coerce.Coercible a b) => a -> b
checkedCoerce = Coerce.coerce
  where _useCast = castDummy @a @b

-- | Coercion from @a@ to @b@ is permitted and safe.
type Castable_ a b = (MichelsonCoercible a b, CanCastTo a b)

-- | Coercions between @a@ to @b@ are permitted and safe.
type Coercible_ a b = (MichelsonCoercible a b, CanCastTo a b, CanCastTo b a)

-- | Coerce between types which have an explicit permission for that in the
-- face of 'CanCastTo' constraint.
checkedCoerce_ :: forall a b s. (Castable_ a b) => a : s :-> b : s
checkedCoerce_ = forcedCoerce_

-- | Pretends that the top item of the stack was coerced.
checkedCoercing_
  :: forall a b s. (Coercible_ a b)
  => (b ': s :-> b ': s) -> (a ': s :-> a ': s)
checkedCoercing_ f = checkedCoerce_ @a @b # f # checkedCoerce_ @b @a

-- | Locally provide given 'CanCastTo' instance.
allowCheckedCoerceTo :: forall b a. Dict (CanCastTo a b)
allowCheckedCoerceTo =
  unsafeCoerce
    @(Dict $ CanCastTo () ())
    @(Dict $ CanCastTo a b)
    Dict

-- | Locally provide bidirectional 'CanCastTo' instance.
allowCheckedCoerce :: forall a b. Dict (CanCastTo a b, CanCastTo b a)
allowCheckedCoerce =
  Dict \\ allowCheckedCoerceTo @a @b \\ allowCheckedCoerceTo @b @a

-- Incoherent instances are generally evil because arbitrary instance can be
-- picked, but in our case this is exactly what we want: permit cast if
-- /any/ instance matches.
instance {-# INCOHERENT #-} CanCastTo a a where

instance CanCastTo a b =>
         CanCastTo [a] [b]
instance CanCastTo a b =>
         CanCastTo (Maybe a) (Maybe b)
instance (CanCastTo l1 l2, CanCastTo r1 r2) =>
         CanCastTo (Either l1 r1) (Either l2 r2)
instance CanCastTo k1 k2 =>
         CanCastTo (Set k1) (Set k2)
instance (CanCastTo k1 k2, CanCastTo v1 v2) =>
         CanCastTo (Map k1 v1) (Map k2 v2)
instance (CanCastTo k1 k2, CanCastTo v1 v2) =>
         CanCastTo (BigMap k1 v1) (BigMap k2 v2)
instance ( CanCastTo (ZippedStack i1) (ZippedStack i2)
         , CanCastTo (ZippedStack o1) (ZippedStack o2)
         ) =>
         CanCastTo (i1 :-> o1) (i2 :-> o2)
instance (CanCastTo a1 a2) =>
         CanCastTo (ContractRef a1) (ContractRef a2)

instance (CanCastTo a b, f ~ g) => CanCastTo (NamedF f a n) (NamedF g b m)

instance (CanCastTo a1 a2, CanCastTo b1 b2) =>
         CanCastTo (a1, b1) (a2, b2)
instance (CanCastTo a1 a2, CanCastTo b1 b2, CanCastTo c1 c2) =>
         CanCastTo (a1, b1, c1) (a2, b2, c2)
instance (CanCastTo a1 a2, CanCastTo b1 b2, CanCastTo c1 c2, CanCastTo d1 d2) =>
         CanCastTo (a1, b1, c1, d1) (a2, b2, c2, d2)
instance ( CanCastTo a1 a2, CanCastTo b1 b2, CanCastTo c1 c2, CanCastTo d1 d2
         , CanCastTo e1 e2 ) =>
         CanCastTo (a1, b1, c1, d1, e1) (a2, b2, c2, d2, e2)
instance ( CanCastTo a1 a2, CanCastTo b1 b2, CanCastTo c1 c2, CanCastTo d1 d2
         , CanCastTo e1 e2, CanCastTo f1 f2 ) =>
         CanCastTo (a1, b1, c1, d1, e1, f1) (a2, b2, c2, d2, e2, f2)

-- | Implementation of 'castDummy' for types composed from smaller types.
-- It helps to ensure that all necessary constraints are requested in instance
-- head.
castDummyG
  :: (Generic a, Generic b, GCanCastTo (G.Rep a) (G.Rep b))
  => Proxy a -> Proxy b -> ()
castDummyG (_ :: Proxy a) (_ :: Proxy b) = ()
  where _dummy = Dict @(Generic a, Generic b, GCanCastTo (G.Rep a) (G.Rep b))

type family GCanCastTo x y :: Constraint where
  GCanCastTo (G.M1 _ _ x) (G.M1 _ _ y) = GCanCastTo x y
  GCanCastTo (xl G.:+: xr) (yl G.:+: yr) = (GCanCastTo xl yl, GCanCastTo xr yr)
  GCanCastTo (xl G.:*: xr) (yl G.:*: yr) = (GCanCastTo xl yl, GCanCastTo xr yr)
  GCanCastTo G.U1 G.U1 = ()
  GCanCastTo G.V1 G.V1 = ()
  GCanCastTo (G.Rec0 a) (G.Rec0 b) = CanCastTo a b

{- Note about potential use of 'Coercible'.

Alternative to 'CanCastTo' would be using 'Coercible' constraint.

Pros:
* Reflexivity, symmetry and transitivity properties hold automatically.
* Complex structures are handled automatically.

Cons:
* When declaring a datatype type, one should always care to set the corresponding
type role (in most cases it will nominal or representational). Newtypes are
even more difficult to control as they are always coercible if constructor is
in scope.
* Where are some cases where going with 'Coercible' does not work, e.g.
allow @MigrationScriptFrom oldStore@ to @MigrationScript oldStore newStore@.

Despite the mentioned cons, described 'coerce_' may be useful someday.

-}

----------------------------------------------------------------------------
-- Coercions for some basic types
----------------------------------------------------------------------------

instance TAddress p `CanCastTo` Address
instance Address `CanCastTo` TAddress p

instance FutureContract p `CanCastTo` EpAddress

instance Packed a `CanCastTo` ByteString
instance TSignature a `CanCastTo` ByteString
instance Hash alg a `CanCastTo` ByteString

instance CanCastTo a b => Packed a `CanCastTo` Packed b
instance CanCastTo a b => TSignature a `CanCastTo` TSignature b
instance (CanCastTo alg1 alg2, CanCastTo a1 a2) =>
         Hash alg1 a1 `CanCastTo` Hash alg2 a2
