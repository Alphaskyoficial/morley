-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Allows specifying entrypoints without declaring 'ParameterHasEntrypoints'
-- instance.
module Lorentz.Entrypoints.Manual
  ( ParameterWrapper (..)
  ) where

import Lorentz.Constraints
import Lorentz.Entrypoints.Core
import Lorentz.Wrappable
import Morley.Michelson.Typed

-- | Wrap parameter into this to locally assign a way to derive entrypoints for
-- it.
newtype ParameterWrapper (deriv :: Type) cp = ParameterWrapper { unParameterWraper :: cp }
  deriving stock Generic
  deriving anyclass (IsoValue, Unwrappable, Wrappable)

-- Helper for implementing @instance ParameterHasEntrypoints ParameterWrapper@.
data PwDeriv deriv
instance EntrypointsDerivation deriv cp =>
         EntrypointsDerivation (PwDeriv deriv) (ParameterWrapper deriv cp) where
  type EpdAllEntrypoints (PwDeriv deriv) (ParameterWrapper deriv cp) =
    EpdAllEntrypoints deriv cp
  type EpdLookupEntrypoint (PwDeriv deriv) (ParameterWrapper deriv cp) =
    EpdLookupEntrypoint deriv cp
  epdNotes = epdNotes @deriv @cp
  epdCall = epdCall @deriv @cp
  epdDescs = epdDescs @deriv @cp

instance ( NiceParameter cp
         , EntrypointsDerivation epd cp
         , RequireAllUniqueEntrypoints' epd cp
         ) =>
         ParameterHasEntrypoints (ParameterWrapper epd cp) where
  type ParameterEntrypointsDerivation (ParameterWrapper epd cp) = PwDeriv epd
