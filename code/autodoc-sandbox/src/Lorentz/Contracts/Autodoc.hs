-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-orphans #-}

-- | A contract using all imaginery autodoc features.
module Lorentz.Contracts.Autodoc
  ( autodocSandboxContract
  ) where

import Lorentz

data Parameter
  = Action1
  | Action2 Integer
  | ActionNested (Either Integer Natural)
  | ActionHash (Void_ ByteString (Hash Blake2b ByteString, Hash Sha512 ByteString))
  | ActionNewtype MyType
  | NeverMention Never
  deriving stock (Generic)
  deriving anyclass (IsoValue, HasAnnotation)

instance ParameterHasEntrypoints Parameter where
  type ParameterEntrypointsDerivation Parameter = EpdPlain

data Storage = Storage
  { sCounter :: Integer
  , sNothing :: ()
  }
  deriving stock (Generic)
  deriving anyclass (IsoValue, HasAnnotation)

instance TypeHasDoc Storage where
  typeDocMdDescription = "Our storage"

newtype MyType = MyType Integer
  deriving stock (Generic)
  deriving anyclass (IsoValue, HasAnnotation)

instance TypeHasDoc MyType where
  typeDocMdDescription = "My type."

[errorDoc| "initError" exception "Some error at the beginning" |]

data SpecialEntrypointKind
instance EntrypointKindHasDoc SpecialEntrypointKind where
  entrypointKindPos = 5003
  entrypointKindSectionName = "Special entrypoint"
  entrypointKindSectionDescription = Just "Just some test nested entrypoints."

autodocSandboxContract :: Contract Parameter Storage
autodocSandboxContract = defaultContract . docGroup "Autodoc sandbox" $ do
  contractGeneralDefault
  doc $ dStorage @Storage
  doc $ DDescription "Sandbox contract."

  entrypointSection "Prior checks" (Proxy @CommonContractBehaviourKind) $ do
    doc $ DDescription "Some prior checks necessary because of reasons."
    initCheck

  unpair
  entryCase (Proxy @PlainEntrypointsKind)
    ( #cAction1 /-> do
        doc $ DDescription "Nullary constructor case."
        nil; pair
    , #cAction2 /-> do
        doc $ DDescription "Simple case."
        drop @Integer
        nil; pair
    , #cActionNested /-> do
        doc $ DDescription "Nested entrypoints case."
        entryCase (Proxy @SpecialEntrypointKind)
          ( #cLeft /-> do
              doc $ DDescription "LHS case."
              drop
          , #cRight /-> do
              doc $ DDescription "RHS case."
              drop
          )
        nil; pair
    , #cActionHash /-> do
        doc $ DDescription "Check hashing algorithm section."
        void_ $ do dup; blake2B; dip sha512; pair
    , #cActionNewtype /-> do
        doc $ DDescription "Check mere newtype rendering."
        drop @MyType
        nil; pair
    , #cNeverMention /-> do
        doc $ DDescription "Check for use of never type."
        drop @Never
        nil; pair
    )

-- This helps to check the case when an error occurs outside of any entrypoint
initCheck :: s :-> s
initCheck = do
  push True; if_ nop (failCustom_ #initError)
