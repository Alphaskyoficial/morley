-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Main
  ( main
  ) where

import Prelude

import qualified Data.Map as Map
import Main.Utf8 (withUtf8)
import qualified Options.Applicative as Opt
import Options.Applicative.Help.Pretty (Doc, linebreak)

import qualified Lorentz as L
import Lorentz.ContractRegistry
import Lorentz.Contracts.Autodoc

contracts :: ContractRegistry
contracts = ContractRegistry $ Map.fromList
  [ "AutodocSandbox" ?:: ContractInfo
    { ciContract = autodocSandboxContract
    , ciIsDocumented = True
    , ciStorageParser = Nothing
    , ciStorageNotes = Nothing
    }
  ]

programInfo :: L.DGitRevision -> Opt.ParserInfo CmdLnArgs
programInfo gitRev = Opt.info (Opt.helper <*> argParser contracts gitRev) $
  mconcat
  [ Opt.fullDesc
  , Opt.footerDoc usageDoc
  ]

usageDoc :: Maybe Doc
usageDoc = Just $ mconcat
   [ "You can use help for specific COMMAND", linebreak
   , "EXAMPLE:", linebreak
   , "  autodoc-sandbox print --help", linebreak
   ]

main :: IO ()
main = withUtf8 $ do
  let gitRev = $(L.mkDGitRevision) L.morleyRepoSettings
  cmdLnArgs <- Opt.execParser (programInfo gitRev)
  runContractRegistry contracts cmdLnArgs `catchAny` (die . displayException)
