# Autodoc sandbox

This mini-project exists to test autodoc features.
With it, we can
1. Visually assess the result;
2. Ensure that cross-references are correct.

Thus, the contract here is supposed to contain all imaginary use-cases.

Note, that this is an inherent part of the Morley repository - relying on it is the only way to catch some bugs at merge request stage without involving other projects.
