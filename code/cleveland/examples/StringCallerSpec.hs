#!/usr/bin/env stack
{- stack
  script
  --resolver snapshot.yaml
  --package base-noprelude
  --package text
  --package fmt
  --package hspec
  --package hspec-hedgehog
  --package hedgehog
  --package cleveland
  --package morley
  --package morley-prelude
  --ghc-options "-hide-package base"
-}
-- Note that stack shebang and its arguments list cannot be separated with a newline
-- because otherwise arguments won't be used.

-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE OverloadedStrings, QuasiQuotes, TypeApplications #-}

-- TODO [#566]: Use nettest engine
{-# OPTIONS_GHC -Wno-deprecations #-}

module StringCallerSpec where

import Data.Text (Text)
import Test.Hspec (Spec, hspec, it, parallel)
import Test.Hspec.Hedgehog (forAll, hedgehog, modifyMaxSuccess)

import Hedgehog.Gen.Michelson (genMText)
import Morley.Michelson.Text (MText, mt)
import Morley.Michelson.Typed
import qualified Morley.Michelson.Untyped as Untyped
import Morley.Tezos.Address (Address, mformatAddress, unsafeParseAddress)
import Morley.Tezos.Core (timestampFromSeconds)
import Test.Cleveland.Instances ()
import Test.Cleveland.Michelson
  (IntegrationalScenario, TxData(..), TxParam(..), expectBalance, expectError,
  expectMichelsonFailed, expectStorageUpdateConst, genesisAddress, integrationalTestExpectation,
  integrationalTestProp, originate, setNow, specWithUntypedContract, transfer)

main :: IO ()
main = hspec spec

spec :: Spec
spec = parallel $
  specWithUntypedContract "contracts/string_caller.tz" $
    \stringCaller -> specWithUntypedContract "contracts/fail_or_store_and_transfer.tz" $
      \failOrStoreAndTransfer -> specImpl stringCaller failOrStoreAndTransfer

specImpl :: Untyped.Contract -> Untyped.Contract -> Spec
specImpl stringCaller failOrStoreAndTransfer = do
  let scenario = integrationalScenario stringCaller failOrStoreAndTransfer
  let prefix =
        "stringCaller calls failOrStoreAndTransfer and updates its storage with "
  let suffix =
        " and properly updates balances. But fails if failOrStoreAndTransfer's"
        <> " balance is ≥ 1300 and NOW is ≥ 500"
  it (prefix <> "a constant" <> suffix) $
    integrationalTestExpectation (scenario constStr)

  -- The test is trivial, so it's kinda useless to run it many times
  modifyMaxSuccess (const 2) $
    it (prefix <> "an arbitrary value" <> suffix) $ hedgehog $ do
      str <- forAll genMText
      integrationalTestProp (scenario str)
  where
    constStr = [mt|caller|]

integrationalScenario :: Untyped.Contract -> Untyped.Contract -> MText -> IntegrationalScenario
integrationalScenario stringCaller failOrStoreAndTransfer str = do
  let
    initFailOrStoreBalance = 900
    initStringCallerBalance = 500

  -- Originate both contracts
  failOrStoreAndTransferAddress <-
    originate failOrStoreAndTransfer "failOrStoreAndTransfer" (Untyped.ValueString [mt|hello|]) initFailOrStoreBalance
  stringCallerAddress <-
    originate stringCaller "stringCaller"
    (Untyped.ValueString $ mformatAddress failOrStoreAndTransferAddress)
    initStringCallerBalance

  -- NOW = 500, so stringCaller shouldn't fail
  setNow (timestampFromSeconds 500)

  -- Transfer 100 tokens to stringCaller, it should transfer 300 tokens
  -- to failOrStoreAndTransfer
  let
    newValue = Untyped.ValueString str
    txData = TxData
      { tdSenderAddress = genesisAddress
      , tdParameter = TxUntypedParam newValue
      , tdAmount = 100
      , tdEntrypoint = DefEpName
      }
    transferToStringCaller = transfer txData stringCallerAddress
  transferToStringCaller

  -- Check balances and storage of 'failOrStoreAndTransfer'
  let
    -- `stringCaller.tz` transfers 300 mutez.
    -- 'failOrStoreAndTransfer.tz' transfers 5 tokens.
    -- Also 100 tokens are transferred from the genesis address.
    expectedStringCallerBalance = 500 - 300 + 100
    expectedFailOrStoreBalance = 900 + 300 - 5
    expectedConstAddrBalance = 5

  expectStorageUpdateConst failOrStoreAndTransferAddress newValue
  expectBalance failOrStoreAndTransferAddress expectedFailOrStoreBalance
  expectBalance stringCallerAddress expectedStringCallerBalance
  expectBalance constAddr expectedConstAddrBalance

  -- Now let's transfer 100 tokens to stringCaller again.
  err <- expectError transferToStringCaller

  -- This time execution should fail, because failOrStoreAndTransfer should fail
  -- because its balance is greater than 1300.
  expectMichelsonFailed failOrStoreAndTransferAddress err

  -- Now let's set NOW to 600 and expect stringCaller to fail
  setNow (timestampFromSeconds 600)
  err <- expectError transferToStringCaller
  void $ expectMichelsonFailed stringCallerAddress err

-- Address hardcoded in 'failOrStoreAndTransfer.tz'.
constAddr :: Address
constAddr = unsafeParseAddress "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU"
