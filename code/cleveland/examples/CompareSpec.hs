#!/usr/bin/env stack
{- stack
  script
  --resolver snapshot.yaml
  --package base-noprelude
  --package text
  --package fmt
  --package hspec
  --package hspec-hedgehog
  --package hedgehog
  --package cleveland
  --package morley
  --package morley-prelude
  --ghc-options "-hide-package base"
-}
-- Note that stack shebang and its arguments list cannot be separated with a newline
-- because otherwise arguments won't be used.

-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE OverloadedStrings #-}

module CompareSpec where

import Fmt (pretty)
import Hedgehog (Gen, MonadTest, Property, forAll, property, (===))
import Test.Hspec (Spec, hspec, it)
import Test.Hspec.Hedgehog (hedgehog)

import Hedgehog.Gen.Tezos.Core (genMutez)
import Morley.Michelson.Typed (ToT, fromVal)
import Morley.Tezos.Core (Mutez)
import Test.Cleveland.Michelson
  (ContractReturn, contractProp, dummyContractEnv, failedTest, specWithTypedContract)

type Parameter = (Mutez, Mutez)
type Storage = [Bool]

main :: IO ()
main = hspec spec

genParameter :: Gen Parameter
genParameter = (,) <$> genMutez <*> genMutez

spec :: Spec
spec = do
  specWithTypedContract "contracts/compare.tz" $ \contract -> do
    it "Random check" $ hedgehog $ do
      inputParam <- forAll genParameter
      contractProp contract (validate inputParam) dummyContractEnv inputParam initStorage
  where
    initStorage :: Storage
    initStorage = []

    mkExpected :: Parameter -> Storage
    mkExpected (a, b) = [a == b, a > b, a < b, a >= b, a <= b]

    validate
      :: MonadTest m
      => Parameter
      -> ContractReturn (ToT Storage)
      -> m ()
    validate p (Right ([], l), _) = fromVal l === mkExpected p
    validate _ (Left e, _) =
      failedTest $ "Unexpected failure of the sctipt: " <> pretty e
    validate _ _ =
      failedTest "Invalid result of the script"
