#!/usr/bin/env stack
{- stack
  script
  --resolver snapshot.yaml
  --package base-noprelude
  --package text
  --package fmt
  --package hspec
  --package hspec-hedgehog
  --package hedgehog
  --package cleveland
  --package morley
  --package morley-prelude
  --ghc-options "-hide-package base"
-}
-- Note that stack shebang and its arguments list cannot be separated with a newline
-- because otherwise arguments won't be used.

-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE OverloadedStrings, RecordWildCards #-}

-- TODO [#566]: Use nettest engine
{-# OPTIONS_GHC -Wno-deprecations #-}

module EnvironmentSpec
  ( spec
  ) where

import Hedgehog (MonadGen, Property, forAll, property, (===))
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Hspec (Spec, hspec, it)
import Test.Hspec.Hedgehog (hedgehog)

import Morley.Michelson.Interpret (RemainingSteps(..))
import Morley.Michelson.Typed
import qualified Morley.Michelson.Untyped as Untyped
import Morley.Tezos.Address (Address, mformatAddress)
import Morley.Tezos.Core (Mutez, Timestamp, timestampFromSeconds, unsafeAddMutez)
import Test.Cleveland.Instances ()
import Test.Cleveland.Michelson
  (IntegrationalScenario, TxData(..), TxParam(..), expectError, expectMichelsonFailed,
  genesisAddress, integrationalTestProp, originate, setMaxSteps, setNow, specWithUntypedContract,
  transfer)

main :: IO ()
main = hspec spec

spec :: Spec
spec = specWithUntypedContract "contracts/environment.tz" specImpl

data Fixture = Fixture
  { fNow :: !Timestamp
  , fMaxSteps :: !RemainingSteps
  , fPassOriginatedAddress :: !Bool
  , fBalance :: !Mutez
  , fAmount :: !Mutez
  } deriving (Show)

genFixture :: MonadGen m => m Fixture
genFixture = do
  fNow <- timestampFromSeconds <$> (Gen.integral $ Range.linear 100000 111111)
  fMaxSteps <- RemainingSteps <$> (Gen.integral $ Range.linear 1015 1028)
  fPassOriginatedAddress <- Gen.bool
  fBalance <- Gen.enum 1 1234
  fAmount <- Gen.enum 1 42
  return Fixture {..}

shouldExpectFailed :: Fixture -> Bool
shouldExpectFailed fixture =
  or
    [ fBalance fixture `unsafeAddMutez` fAmount fixture > 1000
    , fNow fixture < timestampFromSeconds 100500
    , fPassOriginatedAddress fixture
    , fAmount fixture < 15
    ]

specImpl :: Untyped.Contract -> Spec
specImpl environment = do
  let scenario = integrationalScenario environment
  it description $ hedgehog $ do
    fixture <- forAll genFixture
    integrationalTestProp $ scenario fixture
  where
    description =
      "This contract fails under conditions described in a comment at the " <>
      "beginning of this contract."

integrationalScenario :: Untyped.Contract -> Fixture -> IntegrationalScenario
integrationalScenario contract fixture = do
  -- First of all let's set desired gas limit and NOW
  setNow $ fNow fixture
  setMaxSteps $ fMaxSteps fixture

  -- Then let's originate the 'environment.tz' contract
  environmentAddress <-
    originate contract "environment" Untyped.ValueUnit (fBalance fixture)

  -- And transfer tokens to it
  let
    param
      | fPassOriginatedAddress fixture = environmentAddress
      | otherwise = genesisAddress
    txData = TxData
      { tdSenderAddress = genesisAddress
      , tdParameter = TxUntypedParam $ Untyped.ValueString (mformatAddress param)
      , tdAmount = fAmount fixture
      , tdEntrypoint = DefEpName
      }
  if (shouldExpectFailed fixture)
  then do
    err <- expectError $ transfer txData environmentAddress
    void $ expectMichelsonFailed environmentAddress err
  else do
    transfer txData environmentAddress
