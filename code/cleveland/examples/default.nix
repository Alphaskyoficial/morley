# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

rec {
  pkgs = import ../../../nix/nixpkgs-with-haskell-nix.nix {};
  pkgsStatic = pkgs.pkgsCross.musl64;
  haskell-nix = pkgsStatic.haskell-nix;

  hs-pkgs = haskell-nix.stackProject {
    src = haskell-nix.haskellLib.cleanGit {
      # location relative to the repository root, to get the .gitignore file
      src = ../../..;
      subDir = "code/cleveland/examples";
    };
    modules = [{
      # don't haddock dependencies
      doHaddock = false;

      # ./contracts directory is used during testing:
      packages.edsl-demo.preCheck = "cp -r --no-target-directory ${./contracts} ./contracts";
    }];
  };

  edsl-demo-test-original = hs-pkgs.edsl-demo.components.tests.edsl-demo-test;

  # workaround for building a package without a library or an executable component
  # haskell.nix issue: https://github.com/input-output-hk/haskell.nix/issues/362
  edsl-demo-test-fixed = edsl-demo-test-original.overrideAttrs (oldAttrs: {
    installPhase = ''
      runHook preInstall

      mkdir -p $out/bin
      cp dist/build/edsl-demo-test/edsl-demo-test $out/bin/

      runHook postInstall
    '';
  });

  # derivation which runs the test suite
  run-test = haskell-nix.haskellLib.check edsl-demo-test-fixed;
}
