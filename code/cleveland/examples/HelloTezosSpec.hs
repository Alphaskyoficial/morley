#!/usr/bin/env stack
{- stack
  script
  --resolver snapshot.yaml
  --package base-noprelude
  --package text
  --package fmt
  --package hspec
  --package cleveland
  --package morley
  --package morley-prelude
  --ghc-options "-hide-package base"
-}
-- Note that stack shebang and its arguments list cannot be separated with a newline
-- because otherwise arguments won't be used.

-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE OverloadedStrings, QuasiQuotes, TypeApplications #-}

module HelloTezosSpec where

import Data.Text (Text)
import Fmt (pretty)
import Test.Hspec (Spec, expectationFailure, hspec, it, shouldBe)

import Morley.Michelson.Text (mt)
import Morley.Michelson.Typed (toVal)
import Test.Cleveland.Michelson (contractProp, dummyContractEnv, specWithTypedContract)

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  specWithTypedContract "contracts/hello_tezos.tz" $ \contract -> do
    it "Puts 'Hello Tezos!' to its storage" $
      contractProp contract validate' dummyContractEnv () [mt||]
  where
    validate' (res, _) =
      case res of
        Left err -> expectationFailure $
          "Unexpected contract failure: " <> pretty err
        Right (_operations, val) ->
          val `shouldBe` toVal [mt|Hello Tezos!|]
