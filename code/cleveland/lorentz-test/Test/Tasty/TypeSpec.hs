-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tasty integration for TypeSpec.
module Test.Tasty.TypeSpec
  ( typeTest
  ) where

import Test.Tasty.Providers (IsTest(..), TestName, TestTree, singleTest, testPassed)
import Test.TypeSpec.Core (PrettyTypeSpec, TypeSpec(..))

data TypeTestCase =
  forall expectation. PrettyTypeSpec expectation =>
  TypeTestCase (TypeSpec expectation)

instance IsTest TypeTestCase where
  -- Implementation here is similar to the one from HUnit integration
  run _ (TypeTestCase spec) _ =
    -- According to documentation of TypeSpec, the only sensible thing we can
    -- do with spec value is to print it.
    -- TypeSpec test suites are ensured at compile time.
    return . testPassed $ show spec

  testOptions = pure []

-- | Turn a 'TypeSpec' into tasty test case.
typeTest
  :: PrettyTypeSpec expectation
  => TestName -> TypeSpec expectation -> TestTree
typeTest name spec = singleTest name (TypeTestCase spec)
