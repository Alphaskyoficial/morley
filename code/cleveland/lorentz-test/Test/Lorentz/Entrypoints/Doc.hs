-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests on autodoc for entrypoints.
module Test.Lorentz.Entrypoints.Doc
  ( test_ParamBuildingSteps_are_correct
  , test_Finalization_check
  , unit_Uncallables_detection
  ) where

import Control.Spoon (teaspoon)
import Test.HUnit (Assertion, assertBool, (@?=))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Lorentz ((/->), (:->))
import qualified Lorentz as L
import Lorentz.Annotation
import Lorentz.Doc
import Lorentz.Entrypoints
import Lorentz.Entrypoints.Doc
import Lorentz.Value
import Morley.Michelson.Untyped (EpName(..))
import Test.Cleveland.Doc.Lorentz

-- Parameters
----------------------------------------------------------------------------

data MySub
  = Dos1
  | Dos2
  deriving stock (Generic)
  deriving anyclass (IsoValue, HasAnnotation)

instance TypeHasDoc MySub where
  typeDocMdDescription = "MySub"

mySubImpl :: '[MySub] :-> '[]
mySubImpl = L.entryCase (Proxy @PlainEntrypointsKind)
  ( #cDos1 /-> L.nop
  , #cDos2 /-> L.nop
  )

data MyPlainEps
  = Do1 Integer
  | Do2 MySub
  deriving stock (Generic)
  deriving anyclass (IsoValue, HasAnnotation)

instance TypeHasDoc MyPlainEps where
  typeDocMdDescription = "MyPlainEps"

instance ParameterHasEntrypoints MyPlainEps where
  type ParameterEntrypointsDerivation MyPlainEps = EpdPlain

myPlainImplDumb :: '[MyPlainEps] :-> '[]
myPlainImplDumb = L.entryCase (Proxy @PlainEntrypointsKind)
  ( #cDo1 /-> L.drop
  , #cDo2 /-> mySubImpl
  )

myPlainImpl :: '[MyPlainEps] :-> '[]
myPlainImpl = finalizeParamCallingDoc myPlainImplDumb

data MyRecursiveEps
  = Dor1 Integer
  | Dor2 MySub
  deriving stock (Generic)
  deriving anyclass (IsoValue, HasAnnotation)

instance ParameterHasEntrypoints MyRecursiveEps where
  type ParameterEntrypointsDerivation MyRecursiveEps = EpdRecursive

myRecursiveImpl :: '[MyRecursiveEps] :-> '[]
myRecursiveImpl = finalizeParamCallingDoc $
  L.entryCase (Proxy @PlainEntrypointsKind)
  ( #cDor1 /-> L.drop
  , #cDor2 /-> mySubImpl
  )

data MyDelegateEps
  = Dod1 Integer
  | Dod2 MyPlainEps
  deriving stock (Generic)
  deriving anyclass (IsoValue, HasAnnotation)

instance ParameterHasEntrypoints MyDelegateEps where
  type ParameterEntrypointsDerivation MyDelegateEps = EpdDelegate

myDelegateImpl :: '[MyDelegateEps] :-> '[]
myDelegateImpl = finalizeParamCallingDoc $
  L.entryCase (Proxy @PlainEntrypointsKind)
  ( #cDod1 /-> L.drop
  , #cDod2 /-> myPlainImplDumb
  )

myDelegateImplFinalizedTwice :: '[MyDelegateEps] :-> '[]
myDelegateImplFinalizedTwice = finalizeParamCallingDoc $
  L.entryCase (Proxy @PlainEntrypointsKind)
  ( #cDod1 /-> L.drop
  , #cDod2 /-> myPlainImpl
  )

data MyDefEps
  = Do0
  | Default
  deriving stock (Generic)
  deriving anyclass (IsoValue, HasAnnotation)

instance TypeHasDoc MyDefEps where
  typeDocMdDescription = "MyDefEps"

instance ParameterHasEntrypoints MyDefEps where
  type ParameterEntrypointsDerivation MyDefEps = EpdPlain

myDefImplDumb :: '[MyDefEps] :-> '[]
myDefImplDumb =
  L.entryCase (Proxy @PlainEntrypointsKind)
  ( #cDo0 /-> L.nop
  , #cDefault /-> L.nop
  )

myDefImpl :: '[MyDefEps] :-> '[]
myDefImpl = finalizeParamCallingDoc myDefImplDumb

data MyRecursiveDefEps
  = Dord1 MySub
  | Dord2 MyDefEps
  deriving stock (Generic)
  deriving anyclass (IsoValue, HasAnnotation)

instance ParameterHasEntrypoints MyRecursiveDefEps where
  type ParameterEntrypointsDerivation MyRecursiveDefEps = EpdRecursive

myRecursiveDefImpl :: '[MyRecursiveDefEps] :-> '[]
myRecursiveDefImpl = finalizeParamCallingDoc $
  L.entryCase (Proxy @PlainEntrypointsKind)
  ( #cDord1 /-> mySubImpl
  , #cDord2 /-> myDefImplDumb
  )

-- Tests
----------------------------------------------------------------------------

-- Similar to 'ParamBuildingSteps', but without details irrelevant for testing
data ParamBuildingType
    -- | Wrap into constructor with given name
    -- NB: starts with capital letter
  = PbtWrapIn Text
    -- | Call given entrypoint
    -- NB: starts with lowercase letter
  | PbtCallEntrypoint Text
    -- | Does something weird
  | PbtCustom
    -- | Entrypoint cannot be called
  | PbtUncallable
  deriving stock (Show, Eq)

pbsType :: ParamBuildingStep -> ParamBuildingType
pbsType = \case
  PbsWrapIn ctor _ -> PbtWrapIn ctor
  PbsCallEntrypoint (unEpName -> ep) -> PbtCallEntrypoint ep
  PbsCustom _ -> PbtCustom
  PbsUncallable _ -> PbtUncallable

getAllBuildingSteps :: (i :-> o) -> [[ParamBuildingType]]
getAllBuildingSteps instr =
  map pbsType . epaBuilding <$> allContractDocItems (buildDocTest instr)

test_ParamBuildingSteps_are_correct :: [TestTree]
test_ParamBuildingSteps_are_correct =
  [ testCase "Simple entrypoints without direct calling" $
      getAllBuildingSteps myPlainImplDumb
        @?= [ [PbtWrapIn "Do1"]
            , [PbtWrapIn "Do2"]
            , [PbtWrapIn "Do2", PbtWrapIn "Dos1"]
            , [PbtWrapIn "Do2", PbtWrapIn "Dos2"]
            ]

  , testCase "Simple entrypoints" $
      getAllBuildingSteps myPlainImpl
        @?= [ [PbtCallEntrypoint "do1"]
            , [PbtCallEntrypoint "do2"]
            , [PbtCallEntrypoint "do2", PbtWrapIn "Dos1"]
            , [PbtCallEntrypoint "do2", PbtWrapIn "Dos2"]
            ]
  , testCase "Recursive entrypoints" $
      getAllBuildingSteps myRecursiveImpl
        @?= [ [PbtCallEntrypoint "dor1"]
            , [PbtCallEntrypoint "", PbtWrapIn "Dor2"]
            , [PbtCallEntrypoint "dos1"]
            , [PbtCallEntrypoint "dos2"]
            ]
  , testCase "Delegate entrypoints" $
      getAllBuildingSteps myDelegateImpl
        @?= [ [PbtCallEntrypoint "dod1"]
            , [PbtCallEntrypoint "dod2"]
            , [PbtCallEntrypoint "do1"]
            , [PbtCallEntrypoint "do2"]
            , [PbtCallEntrypoint "do2", PbtWrapIn "Dos1"]
            , [PbtCallEntrypoint "do2", PbtWrapIn "Dos2"]
            ]

  , testGroup "With explicit default"
    [ testCase "Simple" $
        getAllBuildingSteps myDefImpl
          @?= [ [PbtCallEntrypoint "do0"]
              , [PbtCallEntrypoint ""]
              ]
    , testCase "Recursive" $
        getAllBuildingSteps myRecursiveDefImpl
          @?= [ [PbtUncallable]  -- Dord1 itself
              , [PbtCallEntrypoint "dos1"]
              , [PbtCallEntrypoint "dos2"]
              , [PbtUncallable]  -- Dord2 itself
              , [PbtCallEntrypoint "do0"]
              , [PbtCallEntrypoint ""]
              ]
    ]
  ]

test_Finalization_check :: [TestTree]
test_Finalization_check =
  [ testCase "Cannot apply second time" $
      assertBool "Finalization unexpectedly didn't fail second time" $
        isNothing $ teaspoon myDelegateImplFinalizedTwice
  ]

-- | Note [doc for uncallable entrypoints]:
-- This test is a proof of that sometimes not all entrypoints are callable.
unit_Uncallables_detection :: Assertion
unit_Uncallables_detection =
  expectDocTestFailure testAllEntrypointsAreCallable
    myRecursiveDefImpl
