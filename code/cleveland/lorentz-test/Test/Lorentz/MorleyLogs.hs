-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for checking MorleyLogs processing.
module Test.Lorentz.MorleyLogs
  ( test_MorleyLogs
  ) where

import Prelude hiding (drop, swap)
import Test.Tasty (TestTree, testGroup)

import Lorentz hiding (assert)
import Morley.Michelson.Interpret (MorleyLogs(..))
import Morley.Michelson.Untyped (Contract)
import Morley.Michelson.Untyped.Value (Value'(..))
import Test.Cleveland
import Test.Cleveland.Michelson (testTreesWithUntypedContract)
import Test.Cleveland.Tasty


test_MorleyLogs :: IO [TestTree]
test_MorleyLogs =
  testTreesWithUntypedContract "../../contracts/empties.tz" $ \withoutLogs ->
  testTreesWithUntypedContract "../../contracts/single_log.mtz" $ \withSingleLog ->
  testTreesWithUntypedContract "../../contracts/multiple_logs.mtz" $ \withMultiLogs ->
  pure
  [ testGroup "Checking MorleyLogs processing"
    [ emulatedScenarioCaps "Calling contract with single log" $ do
        idAddr <- originateS withSingleLog
        call idAddr CallDefault ()
        call idAddr CallDefault ()

        getMorleyLogs @@== fmap MorleyLogs [["log"], ["log"]]

    , emulatedScenarioCaps "Calling several contracts with and without logs" $ do
        idAddrW <- originateW withoutLogs
        idAddrS <- originateS withSingleLog
        idAddrM <- originateM withMultiLogs
        call idAddrW CallDefault ()
        call idAddrS CallDefault ()
        call idAddrM CallDefault ()

        getAddressMorleyLogs idAddrW @@== fmap MorleyLogs [[]]
        getAddressMorleyLogs idAddrS @@== fmap MorleyLogs [["log"]]
        getAddressMorleyLogs idAddrM @@== fmap MorleyLogs [["log1", "log2", "log3"]]
        getMorleyLogs @@== fmap MorleyLogs [[], ["log"], ["log1", "log2", "log3"]]

    , emulatedScenarioCaps "Calling several contracts to check the logging order" $ do
        idAddrS <- originateS withSingleLog
        idAddrM <- originateM withMultiLogs
        call idAddrS CallDefault ()
        getAddressMorleyLogs idAddrS @@== fmap MorleyLogs [["log"]]
        call idAddrM CallDefault ()
        getAddressMorleyLogs idAddrM @@== fmap MorleyLogs [["log1", "log2", "log3"]]
        call idAddrS CallDefault ()
        getAddressMorleyLogs idAddrS @@== fmap MorleyLogs [["log"], ["log"]]

        getMorleyLogs @@== fmap MorleyLogs [["log"], ["log1", "log2", "log3"], ["log"]]

    , emulatedScenarioCaps "Calling contracts in parallel with branchout" $ do
        branchout
          [ ("1", do
            idAddr <- originateS withSingleLog
            call idAddr CallDefault ()

            logs <- getAddressMorleyLogs idAddr
            checkCompares (MorleyLogs ["log"]) elem logs)

          , ("2", do
            idAddr <- originateS withSingleLog
            call idAddr CallDefault ()

            getMorleyLogs @@== fmap MorleyLogs [["log"]])
          ]
    -- This test checks the behavior of MorleyLogs when one
    -- contract is called by another contract. In this case contracts contain only
    -- logs produced by themselves.
    , emulatedScenarioCaps "Calling a contract inside another contract" $ do
        caller <- originateSimple "caller" () callerContract
        target <- originateSimple "target" () targetContract
        call caller CallDefault (toContractRef target)

        getMorleyLogs @@== fmap MorleyLogs [["Caller contract called"],["Target contract called with 5"]]
        getAddressMorleyLogs caller @@== fmap MorleyLogs [["Caller contract called"]]
        getAddressMorleyLogs target @@== fmap MorleyLogs [["Target contract called with 5"]]
      ]
  ]
  where
    originateContract
      :: forall caps base m. MonadCleveland caps base m
      => AliasHint
      -> Morley.Michelson.Untyped.Contract
      -> m (TAddress ())
    originateContract name c = do
      addr <- originateUntyped $
        UntypedOriginateData name 100 ValueUnit c
      return $ toTAddress @() addr

    originateW, originateS, originateM
      :: forall caps base m. MonadCleveland caps base m
      => Morley.Michelson.Untyped.Contract
      -> m (TAddress ())
    originateW = originateContract "without logs"
    originateS = originateContract "with single log"
    originateM = originateContract "with multiple logs"

    callerContract :: Lorentz.Contract (ContractRef Integer) ()
    callerContract = defaultContract $
        car #
        printComment "Caller contract called" #
        (push zeroMutez # push 5 # transferTokens |:| nil) #
        unit # swap # pair

    targetContract :: Lorentz.Contract Integer ()
    targetContract = defaultContract $
        car #
        printComment ("Target contract called with " <> stackRef @0) #
        drop #
        unit # nil # pair
