-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Lorentz.Interpreter
  ( test_Entry_points_lookup
  , test_Entry_points_calling
  ) where

import qualified Data.Map as Map
import System.FilePath ((</>))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Lorentz (EpdPlain, HasAnnotation, ParameterHasEntrypoints(..), toAddress)
import Morley.Michelson.Interpret (ContractEnv(..))
import Morley.Michelson.Text
import Morley.Michelson.TypeCheck (unsafeMkSomeParamType)
import Morley.Michelson.Typed (IsoValue(..))
import qualified Morley.Michelson.Typed as T
import qualified Morley.Michelson.Untyped as U
import Morley.Tezos.Address
import Test.Cleveland
import Test.Cleveland.Michelson (testTreesWithTypedContract)
import Test.Cleveland.Michelson.Dummy (dummyContractEnv)
import Test.Cleveland.Michelson.Unit
import Test.Cleveland.Tasty

data Contract1Parameter
  = Contract11 Integer
  | Contract12 MText
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data Self1Parameter
  = Self11 Integer
  | Self12 ()
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

instance ParameterHasEntrypoints Contract1Parameter where
  type ParameterEntrypointsDerivation Contract1Parameter = EpdPlain

instance ParameterHasEntrypoints Self1Parameter where
  type ParameterEntrypointsDerivation Self1Parameter = EpdPlain

test_Entry_points_lookup :: IO [TestTree]
test_Entry_points_lookup =
  testTreesWithTypedContract (dir </> "call1.mtz") $ \call1 ->
  testTreesWithTypedContract (dir </> "call2.mtz") $ \call2 ->
  testTreesWithTypedContract (dir </> "call3.mtz") $ \call3 ->
  testTreesWithTypedContract (dir </> "call4.mtz") $ \call4 ->
  testTreesWithTypedContract (dir </> "call5.mtz") $ \call5 ->
  testTreesWithTypedContract (dir </> "call6.mtz") $ \call6 ->
  testTreesWithTypedContract (dir </> "call7.mtz") $ \call7 ->
  pure
  [ testGroup "Calling contract without default entrypoint"
    [ testCase "Calling default entrypoint refers to the root" $
        checkProp call1 validateSuccess (addr "simple")
    , testCase "Calling some entrypoint refers this entrypoint" $
        checkProp call7 validateSuccess (addr "simple")
    ]
  , testGroup "Calling contract with default entrypoint"
    [ testCase "Calling default entrypoint works" $
        checkProp call2 validateSuccess (addr "def")
    ]
  , testGroup "Common failures"
    [ testCase "Fails on type mismatch" $
        checkProp call1 validateFailure (addr "def")
    , testCase "Fails on entrypoint not found" $
        checkProp call3 validateFailure (addr "simple")
    ]
  , testGroup "Referring entrypoints groups"
    [ testCase "Can refer entrypoint group" $
        checkProp call4 validateSuccess (addr "complex")
    , testCase "Works with annotations" $
        checkProp call5 validateSuccess (addr "complex")
    , testCase "Does not work on annotations mismatch in 'contract' type argument" $
        checkProp call6 validateFailure (addr "complex")
    ]
  ]
  where
    checkProp contract validator callee =
      contractProp @Address @()
        contract validator
        dummyContractEnv{ ceContracts = Map.fromList env }
        callee ()
    validateFailure = validateMichelsonFailsWith ()

    dir = entrypointsDir
    contractSimpleTy =
      U.Ty (U.TOr "a" "b" (U.Ty U.TInt U.noAnn) (U.Ty U.TNat U.noAnn))
             U.noAnn
    contractComplexTy =
      U.Ty (U.TOr "s" "t" (U.Ty U.TString U.noAnn) contractSimpleTy)
             U.noAnn
    contractWithDefTy =
      U.Ty (U.TOr "a" "default" (U.Ty U.TNat U.noAnn) (U.Ty U.TString U.noAnn))
             U.noAnn
    addr = ContractAddress . mkContractHashHack
    env =
      [ (mkContractHashHack "simple", unsafeMkSomeParamType $ U.ParameterType contractSimpleTy U.noAnn)
      , (mkContractHashHack "complex", unsafeMkSomeParamType $ U.ParameterType contractComplexTy U.noAnn)
      , (mkContractHashHack "def", unsafeMkSomeParamType $ U.ParameterType contractWithDefTy U.noAnn)
      ]

test_Entry_points_calling :: IO [TestTree]
test_Entry_points_calling =
  pure
  [ clevelandScenarioCaps "Calling some entrypoint in CONTRACT" $ do
      call1 <- importContract @Address $ dir </> "call1.mtz"
      callerRef <- originateSimple "caller" () call1

      contract1 <- importContract @Contract1Parameter $ dir </> "contract1.mtz"
      targetRef <- originateSimple "target" 0 contract1

      transfer TransferData
        { tdTo = callerRef
        , tdAmount = 1
        , tdEntrypoint = T.DefEpName
        , tdParameter = toAddress targetRef
        }

      getStorage @Integer targetRef @@== 5

  , clevelandScenarioCaps "Calling some entrypoint in SELF" $  do
      self1 <- importContract @Self1Parameter $ dir </> "self1.mtz"
      contractRef <- originateSimple "self" 0 self1

      transfer TransferData
        { tdTo = contractRef
        , tdAmount = 1
        , tdEntrypoint = T.DefEpName
        , tdParameter = Right @Integer ()
        }

      getStorage @Integer contractRef @@== 5
  ]
  where
    dir = entrypointsDir

entrypointsDir :: FilePath
entrypointsDir = ".." </> ".." </> "contracts" </> "entrypoints"
