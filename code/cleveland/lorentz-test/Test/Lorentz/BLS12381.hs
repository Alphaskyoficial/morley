-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE NoApplicativeDo, RebindableSyntax #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

-- | Tests on BLS12381 primitives accessibility.
module Test.Lorentz.BLS12381
  ( unit_Contract1
  ) where

import Lorentz

import Test.HUnit (Assertion, (@?=))

import qualified Morley.Tezos.Crypto.BLS12381 as BLS

pairingCheckCode :: Lambda (Bls12381G1, Bls12381G2) Bool
pairingCheckCode = do
  dip nil; cons
  push @Natural 1; push @Bls12381Fr 1; mul; push BLS.g2One; neg; mul
  push BLS.g1One; pair; cons
  pairingCheck

unit_Contract1 :: Assertion
unit_Contract1 =
  pairingCheckCode -$ (BLS.g1One, BLS.g2One)
    @?= True
