-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Serialization.Aeson
  ( test_Roundtrip
  ) where

import Data.Aeson (FromJSON, ToJSON, eitherDecode, encode)
import Hedgehog (Gen)
import Test.Tasty (TestTree)

import Hedgehog.Gen.Michelson.Untyped
  (genAnnotation, genContract, genElt, genExpandedOp, genInstrAbstract, genStackFn, genValue)
import Hedgehog.Gen.Tezos.Core (genMutez, genTimestamp)
import Morley.Michelson.Untyped
  (Contract, Elt, ExpandedOp, FieldAnn, InstrAbstract, StackFn, TypeAnn, Value, VarAnn)
import Morley.Tezos.Core (Mutez, Timestamp)

import Test.Util.Hedgehog (roundtripTree)


-- Note: if we want to enforce a particular JSON format, we can extend
-- these test with golden tests (it's easy with `hspec-golden-aeson`).

test
  :: forall a. (Eq a, Show a, ToJSON a, FromJSON a, Typeable a)
  => Gen a -> TestTree
test genA = roundtripTree @a genA encode eitherDecode

test_Roundtrip :: [TestTree]
test_Roundtrip =
  [ -- Core Tezos types
    test @Timestamp genTimestamp
  , test @Mutez genMutez

  -- Michelson types
  , test @StackFn genStackFn
  , test @ExpandedOp genExpandedOp

  -- these are actually all the same thing (Annotation a),
  -- where a is a phantom type,
  -- but let's test them in case they
  -- ever change for some reason
  , test @TypeAnn genAnnotation
  , test @FieldAnn genAnnotation
  , test @VarAnn genAnnotation

  , test @Contract genContract
  , test @(InstrAbstract ExpandedOp) (genInstrAbstract genExpandedOp)
  , test @Value genValue
  , test @(Elt ExpandedOp) (genElt genExpandedOp)
  ]
