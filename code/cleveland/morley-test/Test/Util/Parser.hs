-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Util.Parser
  ( shouldParse
  ) where

import Fmt (build, indentF, pretty, unlinesF)
import Test.HUnit.Base (assertFailure)
import Text.Megaparsec (errorBundlePretty)

import Morley.Michelson.Parser (Parser)
import qualified Morley.Michelson.Parser as Parser

-- | Expect the given text to be successfully parsed.
shouldParse :: Parser a -> Text -> IO a
shouldParse parser text =
  case Parser.parseNoEnv parser "" text of
    Left err ->
      assertFailure . pretty $ unlinesF
        [ "Failed to parse:"
        , indentF 2 $ build text
        , "Error:"
        , indentF 2 $ build $ errorBundlePretty err
        ]
    Right res -> return res
infix 2 `shouldParse`
