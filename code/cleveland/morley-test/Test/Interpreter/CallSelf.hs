-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for the contract that calls self several times.

module Test.Interpreter.CallSelf
  ( test_self_caller
  ) where

import Hedgehog (forAll, property, withTests)
import qualified Hedgehog.Gen as Gen
import Test.HUnit (Assertion, (@?=))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)
import Test.Tasty.Hedgehog (testProperty)

import Morley.Michelson.Interpret (ContractEnv(..), InterpreterState(..), RemainingSteps(..))
import Morley.Michelson.Typed as T
import qualified Morley.Michelson.Untyped as U
import Morley.Tezos.Core (unsafeMkMutez)
import Test.Cleveland
import Test.Cleveland.Michelson (ContractPropValidator, contractProp, testTreesWithContract)
import Test.Cleveland.Michelson.Dummy

import Test.Util.Contracts

test_self_caller :: IO [TestTree]
test_self_caller =
  testTreesWithContract (inContractsDir "call_self_several_times.tz") $ \selfCaller ->
  pure (testImpl selfCaller)

gasForOneExecution :: Num a => a
gasForOneExecution = 19

gasForLastExecution :: Num a => a
gasForLastExecution = 20

type Parameter = 'TInt
type Storage = 'TNat

testImpl ::
  T.Contract Parameter Storage
  -> [TestTree]
testImpl selfCaller =
  [ testCase ("With parameter 1 single execution consumes " <>
      show @_ @Int gasForLastExecution <> " gas") $
    contractProp selfCaller (unitValidator gasForLastExecution) unitContractEnv
    (1 :: Integer) (0 :: Natural)

  , testCase ("With parameter 2 single execution consumes " <>
      show @_ @Int gasForOneExecution <> " gas") $
    contractProp selfCaller (unitValidator gasForOneExecution) unitContractEnv
    (2 :: Integer) (0 :: Natural)

  , testProperty propertyDescription $
    withTests 10 $ property $ do
      callCount <- forAll $ Gen.enum minCalls maxCalls
      clevelandTestProp (clevelandTransferScenario (T.convertContract selfCaller) callCount)
  ]
  where
    -- Environment for unit test
    unitContractEnv = dummyContractEnv
    -- Validator for unit test
    unitValidator ::
      RemainingSteps -> ContractPropValidator Storage Assertion
    unitValidator gasDiff (_, (isRemainingSteps -> remSteps, _)) =
      remSteps @?= ceMaxSteps unitContractEnv - gasDiff

    propertyDescription =
      "calls itself n times, sets storage to n OR fails due to gas limit"

    minCalls = 1
    maxCalls = 10

clevelandTransferScenario :: U.Contract -> Integer -> ClevelandScenario m
clevelandTransferScenario uSelfContract parameter = uncapsCleveland $ do
  let
    uoData = UntypedOriginateData
      { uodName = "self-caller"
      , uodBalance = unsafeMkMutez 1
      , uodStorage = U.ValueInt 0
      , uodContract = uSelfContract
      }
  address <- originateUntyped uoData
  let
    transferData = TransferData
      { tdTo = address
      , tdAmount = minBound
      , tdEntrypoint = DefEpName
      , tdParameter = parameter
      }
  transfer transferData
  let expectedStorage :: Natural = fromIntegral parameter
  getStorage @Natural address @@== expectedStorage
