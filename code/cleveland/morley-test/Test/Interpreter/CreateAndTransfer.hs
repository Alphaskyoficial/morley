-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for 'create_n_transfer.tz' contract. See [#643]
module Test.Interpreter.CreateAndTransfer
  ( test_createAndTranfser
  ) where

import Test.Tasty (TestTree)

import Morley.Tezos.Address
import Test.Cleveland
import Test.Cleveland.Instances ()
import Test.Cleveland.Tasty
import Test.Util.Contracts

test_createAndTranfser :: IO TestTree
test_createAndTranfser =
  pure $ clevelandScenarioCaps "'create_n_transfer.tz' performs transfer after origination" $ do
    contract <- importContract @() @() (contractsDir </> "create_n_transfer.tz")
    createAndTransfer <- originateSimple "createAndTransfer" () contract
    oldBalance <- getBalance constAddr
    transferMoney createAndTransfer 10
    newBalance <- getBalance constAddr
    newBalance - oldBalance @== 1

-- Address hardcoded in 'create_n_transfer.tz'.
constAddr :: Address
constAddr = unsafeParseAddress "tz1NJRjyBXqAmBf94FLTTuQWZGHpmGG4CWKe"
