-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Module, containing spec to test contract_op.tz contract.
module Test.Interpreter.ContractOp
  ( test_contract_op
  ) where

import qualified Data.Map as M
import Fmt (pretty)
import Hedgehog (MonadTest, property, (===))
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)

import Morley.Michelson.Interpret (ContractEnv(..), ContractReturn)
import Morley.Michelson.TypeCheck (SomeParamType(..), unsafeMkSomeParamType)
import Morley.Michelson.Typed (Contract, ToT, fromVal)
import Morley.Michelson.Untyped (ParameterType(..), T(..), Ty(..), noAnn)
import Morley.Tezos.Address
import Test.Cleveland.Instances ()
import Test.Cleveland.Michelson
  (contractProp, dummyContractEnv, failedTest, testTreesWithTypedContract)

import Test.Util.Contracts

-- | Spec to test @contract_op.tz@ contract.
--
-- Test results are confirmed by the reference implementation.
test_contract_op :: IO [TestTree]
test_contract_op =
  testTreesWithTypedContract (inContractsDir "contract_op.tz") $ \contract -> pure $
  [ testProperty "contract not found" $ property $
      contractProp' False [] contract
  ] <>
  map (\(res, paramType) ->
          testProperty (msg res paramType) $ property $
          contractProp' res [(addr, paramType)] contract
      )
  [ (True, unsafeMkSomeParamType $ ParameterType intQ "root")
  , (True, unsafeMkSomeParamType $ ParameterType int "root")
  , (False, unsafeMkSomeParamType $ ParameterType intQ noAnn)
  , (False, unsafeMkSomeParamType $ ParameterType int noAnn)
  , (False, unsafeMkSomeParamType $ ParameterType intP noAnn)
  , (False, unsafeMkSomeParamType $ ParameterType string noAnn)
  , (False, unsafeMkSomeParamType $ ParameterType intP "root")
  , (False, unsafeMkSomeParamType $ ParameterType intQ "another_root")
  ]
  where
    msg isGood paramType =
      "parameter in environment is '" <> pretty paramType <> "', " <>
      bool "" "but "  isGood <> "contract expects '%root int :q'"

    intQ = Ty TInt "q"
    int = Ty TInt noAnn
    intP = Ty TInt "p"
    string = Ty TString noAnn

    addr = unsafeParseContractHash "KT1WsLzQ61xtMNJHfwgCHh2RnALGgFAzeSx9"

    validate
      :: MonadTest m
      => Bool
      -> ContractReturn (ToT Bool)
      -> m ()
    validate ex (Right ([], fromVal -> l), _) = l === ex
    validate _ (Left _, _) = failedTest "Unexpected fail in interepreter"
    validate _ _ = failedTest "Unexpected result of script execution"

    contractProp'
      :: MonadTest m
      => Bool -> [(ContractHash, SomeParamType)] -> Contract (ToT Address) (ToT Bool)
      -> m ()
    contractProp' res ctrs contract =
      contractProp
        contract
        (validate res)
        dummyContractEnv {ceContracts = M.fromList ctrs}
        (ContractAddress addr)
        False
