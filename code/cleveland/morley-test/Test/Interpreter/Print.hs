-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for the @PRINT@ extended command

module Test.Interpreter.Print
  ( test_print_simple
  , test_print_operations
  ) where

import Data.Default (Default(def))
import Fmt ((+|), (|+))
import Test.Tasty (TestTree)

import Morley.Michelson.Interpret (MorleyLogs(unMorleyLogs))
import Morley.Michelson.Text (MText)
import qualified Morley.Michelson.Typed as T
import Morley.Michelson.Typed.Instr
import Morley.Tezos.Address (Address)
import Test.Cleveland
import Test.Cleveland.Tasty (emulatedScenarioCaps)

test_print_simple :: [TestTree]
test_print_simple =
  [ emulatedScenarioCaps "PRINT prints naturals" $ do
      printer <- originateTypedSimple @Natural "printer" () printerContract
      call printer CallDefault (123 :: Natural)
      (fmap unMorleyLogs <$> getMorleyLogs) @@== [["123"]]
  , emulatedScenarioCaps "PRINT prints strings" $ do
      printer <- originateTypedSimple @MText "printer" () printerContract
      call printer CallDefault "hello"
      (fmap unMorleyLogs <$> getMorleyLogs) @@== [["\"hello\""]]
  , emulatedScenarioCaps "PRINT prints right combs" $ do
      printer <- originateTypedSimple @(Integer, (Integer, Integer)) "printer" () printerContract
      call printer CallDefault (1, (2, 3))
      (fmap unMorleyLogs <$> getMorleyLogs) @@== [["{ 1; 2; 3 }"]]
  , emulatedScenarioCaps "PRINT prints sequences" $ do
      printer <- originateTypedSimple @[Integer] "printer" () printerContract
      call printer CallDefault [1..5]
      (fmap unMorleyLogs <$> getMorleyLogs) @@== [["{ 1; 2; 3; 4; 5 }"]]
  ]

test_print_operations :: TestTree
test_print_operations =
    emulatedScenarioCaps "PRINT prints operations" $ do
      addr <- newFreshAddress auto
      printer <- originateTypedSimple @Address "printerOps" () operationPrinter
      call printer CallDefault addr
      (fmap unMorleyLogs <$> getMorleyLogs)
        @@== [["{ Transfer 123 μꜩ tokens to Contract " +| addr |+ " call Call <default>: × }"]]

operationPrinter :: T.Contract 'T.TAddress 'T.TUnit
operationPrinter = T.Contract{..}
  where
    cParamNotes = T.starParamNotes
    cStoreNotes = T.starNotes

    cEntriesOrder = def

    stackRef = PrintComment . one . Right $ mkStackRef @0

    cCode :: Instr '[ 'T.TPair 'T.TAddress 'T.TUnit ] '[ 'T.TPair ('T.TList 'T.TOperation) 'T.TUnit ]
    cCode =
      UNPAIR `Seq`
      CONTRACT T.starNotes T.DefEpName `Seq`
      IF_NONE (PUSH (T.VString "No contract") `Seq` FAILWITH) (
        PUSH (T.VMutez 123) `Seq`
        PUSH T.VUnit `Seq`
        TRANSFER_TOKENS `Seq`
        DIP NIL `Seq`
        CONS `Seq`
        Ext (PRINT stackRef) `Seq`
        DROP `Seq`
        NIL `Seq`
        PAIR
      )

printerContract :: (T.ContainsNestedBigMaps p ~ 'False, T.ConstantScope p) => T.Contract p (T.ToT ())
printerContract = T.Contract{..}
  where
    cParamNotes = T.starParamNotes
    cStoreNotes = T.starNotes

    cEntriesOrder = def

    stackRef = PrintComment . one . Right $ mkStackRef @0

    cCode =
      UNPAIR `Seq`
      Ext (PRINT stackRef) `Seq`
      DROP `Seq`
      NIL `Seq`
      PAIR
