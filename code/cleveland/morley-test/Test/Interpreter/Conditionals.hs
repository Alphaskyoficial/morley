-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Module, containing spec to test conditionals.tz contract.
module Test.Interpreter.Conditionals
  ( test_conditionals
  ) where

import Hedgehog (MonadTest, forAll, property, withTests, (===))
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)

import Hedgehog.Gen.Michelson (genMText)
import Morley.Michelson.Interpret
import Morley.Michelson.Text
import qualified Morley.Michelson.Typed as T
import Test.Cleveland.Instances ()
import Test.Cleveland.Michelson (contractProp, testTreesWithTypedContract)
import Test.Cleveland.Michelson.Dummy (dummyContractEnv)
import Test.Cleveland.Util (eitherIsLeft, eitherIsRight, failedTest)

import Test.Util.Contracts


type Param = Either MText (Maybe Integer)
type ContractResult = ContractReturn (T.ToT MText)

-- | Spec to test conditionals.tz contract.
test_conditionals :: IO [TestTree]
test_conditionals =
  testTreesWithTypedContract (inContractsDir "tezos_examples/attic/conditionals.tz") $ \contract ->
    let
      contractProp' :: MonadTest m => Param -> m ()
      contractProp' inputParam =
        contractProp contract (validate inputParam) dummyContractEnv inputParam
          ("storage" :: MText)
    in pure
    [ testProperty "success 1 test" $
        property $ contractProp' $ Left "abc"
    , testProperty "Random check" $
        withTests 200 $ property $ do
          inputParam <- forAll $ Gen.either
            genMText
            (Gen.maybe (Gen.integral (Range.linearFrom 0 -1000 1000)))
          contractProp' inputParam
    ]
  where
    validate
      :: MonadTest m
      => Param
      -> ContractResult
      -> m ()
    validate (Left a) (Right ([], T.VString b), _) = a === b
    validate (Right Nothing) r = eitherIsLeft $ fst r
    validate (Right (Just a)) r
      | a < 0 = eitherIsLeft $ fst r
      | otherwise = eitherIsRight $ fst r
    validate _ res = failedTest $ "Unexpected result: " <> show res
