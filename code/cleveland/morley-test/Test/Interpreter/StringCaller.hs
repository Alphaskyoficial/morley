-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for the 'stringCaller.tz' contract and its interaction with
-- the 'failOrStoreAndTransfer.tz' contract. Both of them have comments describing
-- their behavior.

module Test.Interpreter.StringCaller
  ( test_stringCaller
  ) where

import Hedgehog (forAll, property, withTests)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Hedgehog (testProperty)

import Hedgehog.Gen.Michelson (genMText)
import Morley.Michelson.Text
import Morley.Michelson.Typed
import qualified Morley.Michelson.Typed as T
import qualified Morley.Michelson.Untyped as U
import Morley.Tezos.Address
import Morley.Tezos.Core
import Test.Cleveland
import Test.Cleveland.Michelson (testTreesWithContract)
import Test.Cleveland.Tasty

import Test.Util.Contracts

test_stringCaller :: IO [TestTree]
test_stringCaller =
  testTreesWithContract (inContractsDir "string_caller.tz") $ \stringCaller ->
  testTreesWithContract (inContractsDir "fail_or_store_and_transfer.tz") $
    \failOrStoreAndTransfer ->
      pure $ [testImpl stringCaller failOrStoreAndTransfer]

testImpl ::
     T.Contract 'TString 'TAddress
  -> T.Contract 'TString 'TString
  -> TestTree
testImpl stringCaller failOrStoreAndTransfer =
  -- failOrStoreAndTransfer's pre-conditions:
  -- balance is >= 1300
  testGroup "calls failOrStoreAndTransfer, updates storage & balance, checks pre-conditions"
    [ clevelandScenarioCaps "when parameter is a constant" $
        scenario (T.convertContract stringCaller) (T.convertContract failOrStoreAndTransfer) constStr

    -- The test is trivial, so it's kinda useless to run it many times
    , testProperty "when parameter is an arbitrary value" $
      withTests 2 $ property $ do
        mtext <- forAll genMText
        clevelandTestProp $ uncapsCleveland (scenario (T.convertContract stringCaller) (T.convertContract failOrStoreAndTransfer) mtext)
    ]
  where
    constStr = "caller"


scenario :: (MonadCleveland caps base m)  => U.Contract -> U.Contract -> MText -> m ()
scenario stringCaller failOrStoreAndTransfer str = do
  let
    initFailOrStoreBalance = 900
    initStringCallerBalance = 500

    failOrStoreData = UntypedOriginateData
      { uodName = "failOrStoreAndTransfer"
      , uodBalance = initFailOrStoreBalance
      , uodStorage = U.ValueString "hello"
      , uodContract = failOrStoreAndTransfer
      }

  failOrStoreAddress <- originateUntyped failOrStoreData

  let
    stringCallerData = UntypedOriginateData
      { uodName = "stringCaller"
      , uodBalance = initStringCallerBalance
      , uodStorage = U.ValueString $ mformatAddress failOrStoreAddress
      , uodContract = stringCaller
      }

  stringCallerAddress <- originateUntyped stringCallerData

  initialConstAddrBalance <- getBalance constAddr

  -- Transfer 100 tokens to stringCaller, it should transfer 300 tokens
  -- to failOrStoreAndTransfer
  let
    transferData = TransferData
      { tdTo = stringCallerAddress
      , tdAmount = unsafeMkMutez 100
      , tdEntrypoint = DefEpName
      , tdParameter = str
      }

    transferToStringCaller = transfer transferData
  transferToStringCaller

  -- Execute operations and check balances and storage of 'failOrStore'
  do
    let
      -- `stringCaller.tz` transfers 300 mutez.
      -- 'failOrStoreAndTransfer.tz' transfers 5 tokens.
      -- Also 100 tokens are transferred from the @moneybag@ address.
      expectedStringCallerBalance = 500 - 300 + 100
      expectedFailOrStoreBalance = 900 + 300 - 5
      expectedConstAddrBalance = initialConstAddrBalance + 5

    getStorage @MText failOrStoreAddress @@== str
    getBalance failOrStoreAddress @@== expectedFailOrStoreBalance
    getBalance stringCallerAddress @@== expectedStringCallerBalance
    getBalance constAddr @@== expectedConstAddrBalance

  -- Now let's transfer 100 tokens to stringCaller again.
  -- This time execution should fail, because failOrStoreAndTransfer should fail
  -- because its balance is greater than 1300.
  expectFailedWith () $
    transferToStringCaller

-- Address hardcoded in 'failOrStoreAndTransfer.tz'.
constAddr :: Address
constAddr = unsafeParseAddress "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU"
