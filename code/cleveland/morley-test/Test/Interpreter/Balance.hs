-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Interpreter.Balance
  ( test_balanceIncludesAmount
  , test_balanceIncludesAmountComplexCase
  ) where

import Hedgehog (Gen, forAll, property, withTests)
import qualified Hedgehog.Gen as Gen
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)

import Morley.Michelson.Typed
import qualified Morley.Michelson.Untyped as U
import Morley.Tezos.Core
import Test.Cleveland
import Test.Cleveland.Michelson (testTreesWithUntypedContract)
import Test.Cleveland.Tasty

import Test.Util.Contracts

data Fixture =
  Fixture
    { fStartingBalance :: Mutez
    , fAmount :: Mutez
    } deriving stock (Show)

genFixture :: Gen Fixture
genFixture = do
  fStartingBalance <- unsafeMkMutez <$> Gen.enum 1000 5000
  fAmount <- unsafeMkMutez <$> Gen.enum 0 1000
  return Fixture{..}

test_balanceIncludesAmount :: IO [TestTree]
test_balanceIncludesAmount = do
  testTreesWithUntypedContract
    (inContractsDir "check_if_balance_includes_incoming_amount.tz") $
      \checker ->
        pure
          [ testProperty "BALANCE includes AMOUNT" $ withTests 50 $ property $ do
              fixture <- forAll genFixture
              clevelandTestProp $ clevelandBalanceTestScenario
                checker fixture
          ]

clevelandBalanceTestScenario :: U.Contract -> Fixture -> ClevelandScenario m
clevelandBalanceTestScenario checker Fixture{..} = uncapsCleveland $ do
  let result = unsafeAddMutez fStartingBalance fAmount

  let
    uoData = UntypedOriginateData
      { uodName = "checkIfBalanceIncludeAmount"
      , uodBalance = fStartingBalance
      , uodStorage = (untypeValue $ toVal ())
      , uodContract = checker
      }

  address <- originateUntyped uoData
  let
    transferData = TransferData
      { tdTo = address
      , tdAmount = fAmount
      , tdEntrypoint = DefEpName
      , tdParameter = result
      }

  transfer transferData
  getBalance address @@== result

test_balanceIncludesAmountComplexCase :: IO [TestTree]
test_balanceIncludesAmountComplexCase = do
  testTreesWithUntypedContract (inContractsDir "balance_test_case_a.tz") $ \contractA ->
    testTreesWithUntypedContract (inContractsDir "balance_test_case_b.tz") $ \contractB ->
      pure
        [ clevelandScenarioCaps "BALANCE returns expected value in nested calls" $ do
            let
              origDataA = UntypedOriginateData
                { uodName = "balance_test_case_a"
                , uodBalance = (unsafeMkMutez 0)
                , uodStorage = (untypeValue $ toVal @[Mutez] [])
                , uodContract = contractA
                }
            let
              origDataB = UntypedOriginateData
                { uodName = "balance_test_case_b"
                , uodBalance = (unsafeMkMutez 0)
                , uodStorage = (untypeValue $ toVal ())
                , uodContract = contractB
                }
            addressA <- originateUntyped origDataA
            addressB <- originateUntyped origDataB
            let
              transferData = TransferData
                { tdTo = addressA
                , tdAmount = unsafeMkMutez 100
                , tdEntrypoint = DefEpName
                , tdParameter = addressB
                }

            transfer transferData

            -- A sends 30 to B, then B sends 5 back to A. A records call to BALANCE at each entry.
            -- We expect that 5 mutez sent back are included in the second call to BALANCE.
            let expectedStorage = [toMutez 75, toMutez 100]
            getStorage @[Mutez] addressA @@== expectedStorage
        ]
