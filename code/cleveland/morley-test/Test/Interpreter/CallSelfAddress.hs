-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Interpreter.CallSelfAddress
  ( test_Call_self_address
  ) where

import Test.Tasty (TestTree)

import Morley.Michelson.Typed
import Morley.Tezos.Core (zeroMutez)
import Test.Cleveland
import Test.Cleveland.Michelson (testTreesWithTypedContract)
import Test.Cleveland.Tasty
import Test.Util.Contracts (inContractsDir)

type ReceiverParameter = 'TLambda 'TUnit 'TAddress
type ReceiverStorage = 'TUnit

type SenderParameter = 'TContract ('TLambda 'TUnit 'TAddress)
type SenderStorage = 'TUnit

test_Call_self_address :: IO [TestTree]
test_Call_self_address =
  testTreesWithTypedContract (inContractsDir "self_address_receiver.tz") $ \receiverContract ->
  testTreesWithTypedContract (inContractsDir "self_address_sender.tz") $ \senderContract ->
  pure
    -- This tests that the SELF_ADDRESS inside a lambda returns the address
    -- of the contract executing the lambda (not the contract defining it).
    -- To do so, two contracts called the sender and the receiver are used.
    -- The sender (this contract) sends the lambda { DROP; SELF_ADDRESS }
    -- to the receiver (see self_address_receiver.tz) who checks that the
    -- returned value is the same as its SELF_ADDRESS.
    [ clevelandScenarioCaps "self_address_sender should call self_address_receiver with no error" $ do
        receiverUntyped <- originateUntyped $ UntypedOriginateData
            { uodName = "Receiver Contract"
            , uodBalance = zeroMutez
            , uodStorage = untypeValue $ toVal ()
            , uodContract = convertContract
                (receiverContract :: Contract ReceiverParameter ReceiverStorage)
            }
        senderUntyped <- originateUntyped $ UntypedOriginateData
            { uodName = "Receiver Contract"
            , uodBalance = zeroMutez
            , uodStorage = untypeValue $ toVal ()
            , uodContract = convertContract
                (senderContract :: Contract SenderParameter SenderStorage)
            }

        let params :: Value SenderParameter
              = VContract receiverUntyped (SomeEpc unsafeEpcCallRoot)

        transfer TransferData
          { tdTo = senderUntyped
          , tdAmount = zeroMutez
          , tdEntrypoint = DefEpName
          , tdParameter = params
          }
    ]
