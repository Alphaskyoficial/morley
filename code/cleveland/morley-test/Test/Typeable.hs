-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests on 'Morley.Util.Typeable'.
module Test.Typeable
  ( test_castIgnoringPhantom
  ) where

import Test.HUnit ((@?=))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Morley.Util.Typeable

data Some = forall x. Typeable x => Some x

data MyType (a :: Type) = MyType Int
  deriving stock (Show, Eq)

data MyType' (a :: Type) = MyType' Double
  deriving stock (Show, Eq)

test_castIgnoringPhantom :: [TestTree]
test_castIgnoringPhantom =
  [ testGroup "Simple type"
    [ testCase "Can cast normal" $
        case (Some $ MyType @Integer 5) of
          Some x -> castIgnoringPhantom x @?= Just (MyType 5)

    , testCase "Cast fails on mismatch" $
        case (Some $ MyType @Integer 5) of
          Some x -> castIgnoringPhantom @MyType' x @?= Nothing
    ]
  ]
