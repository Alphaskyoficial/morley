-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for integrational testing machinery.
module Test.Integrational
  ( spec_Chain_id
  ) where

import Data.Default (def)
import Test.Hspec (Spec, it)

import Morley.Michelson.Runtime.GState (gsChainId, initGState)
import Morley.Michelson.Typed
import Morley.Tezos.Core
import Morley.Util.Named
import Test.Cleveland.Instances ()
import Test.Cleveland.Michelson

spec_Chain_id :: Spec
spec_Chain_id = do
  it "Chain id can be set" $
    integrationalTestExpectation $ do
      let code = DROP `Seq` CHAIN_ID `Seq` SOME `Seq` NIL `Seq` PAIR
      let contract = Contract
            { cCode = code
            , cParamNotes = starParamNotes @'TUnit
            , cStoreNotes = starNotes
            , cEntriesOrder = def
            }

      let expectedChainId = gsChainId initGState

      contractAddr <-
        tOriginate contract "" (toVal (Nothing @ChainId)) 50

      tTransfer (#from .! genesisAddress) (#to .! contractAddr)
        minBound DefEpName (toVal ())

      tExpectStorageConst contractAddr (toVal (Just expectedChainId))
