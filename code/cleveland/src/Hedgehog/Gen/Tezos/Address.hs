-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Address in Tezos.

module Hedgehog.Gen.Tezos.Address
  ( genAddress
  , genContractAddress
  , genKeyAddress
  ) where

import Hedgehog (MonadGen)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import Hedgehog.Gen.Tezos.Crypto (genKeyHash)
import Morley.Tezos.Address (Address(..), ContractHash(..))

genAddress :: MonadGen m => m Address
genAddress = Gen.choice [genKeyAddress, genContractAddress]

genKeyAddress :: MonadGen m => m Address
genKeyAddress = KeyAddress <$> genKeyHash

genContractAddress :: MonadGen m => m Address
genContractAddress = ContractAddress . ContractHash <$> Gen.bytes (Range.singleton 20)
