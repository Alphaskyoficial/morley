-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Hedgehog.Gen.Tezos.Crypto.BLS12381
  ( genBls12381Fr
  , genBls12381G1
  , genBls12381G2
  ) where

import Hedgehog (MonadGen)
import qualified Hedgehog.Gen as Gen

import Morley.Tezos.Crypto.BLS12381
import Test.Cleveland.Util

genBls12381Fr :: MonadGen m => m Bls12381Fr
genBls12381Fr = Gen.enumBounded

genBls12381G1 :: MonadGen m => m Bls12381G1
genBls12381G1 = genRandom generate

genBls12381G2 :: MonadGen m => m Bls12381G2
genBls12381G2 = genRandom generate
