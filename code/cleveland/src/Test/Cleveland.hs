-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Michelson contracts testing on a real Tezos network.
--
-- It defines an interface for writing network testing scenarios and provides
-- two implementations: one of them talks to reference Tezos software and
-- submits operations to real network, the other one converts scenario to
-- the existing integrational testing eDSL.
--
-- Expected usage is the following:
-- 1. Write a scenario using abstract cleveland interface.
-- 2. Make an executable (i. e. something that can be executed:
-- executable or test-suite in .cabal file, normally the latter) which
-- runs this scenario via pure integrational testing engine first (as
-- a quick check for correctness) and then via @Client@ implementation.
-- This executable is supposed to run periodically, but not in each
-- MR on each change.
-- 3. Also run this scenario in existing test-suite using pure implementation.
-- So pure implementation will be used twice: in normal test-suite that
-- runs on each change (which proves that it is /likely/ correct) and in
-- real network test to prevent that test from starting if the scenario
-- is /most likely/ __not__ correct.
-- 4. Note that in order to run it on a real network you should have
-- an address with @moneybag@ alias and it should have sufficient
-- balance to run the scenario.
--
-- TODO:
-- ★ [#50] Collect gas and other statistics in nettest.
-- ★ [#53] Maybe somehow merge with testing eDSL.
-- ★ [#55] Add command line options parsing.

module Test.Cleveland
  ( AliasHint
  , ContractHandler (..)
  , OriginateData (..)
  , UntypedOriginateData (..)
  , TransferData (..)
  , ClevelandScenario
  , EmulatedScenario

  -- * Validation
  , TransferFailure

  -- * Real network implementation based on @tezos-client@ and RPC.
  , runNetworkScenario
  , networkImpl

  -- * @caps@-based commands
  , MonadCleveland
  , MonadOps
  , MonadEmulated
  , ClevelandT
  , EmulatedT
  , withSender
  , withMoneybag
  , runIO
  , resolveAddress
  , newAddress
  , newFreshAddress
  , newRefillableAddress
  , signBytes
  , signBinary
  , originate
  , originateSimple
  , originateUntyped
  , originateUntypedSimple
  , originateTypedSimple
  , originateLarge
  , originateLargeSimple
  , originateLargeUntyped
  , originateLargeUntypedSimple
  , transfer
  , transferMoney
  , call
  , importUntypedContract
  , importContract
  , comment
  , getBalance
  , getMorleyLogs
  , getAddressMorleyLogs
  , getStorage
  , getFullStorage
  , getStorageExpr
  , getAllBigMapValues
  , getAllBigMapValuesMaybe
  , getBigMapSize
  , getBigMapSizeMaybe
  , getBigMapValueMaybe
  , getBigMapValue
  , getPublicKey
  , getChainId
  , advanceTime
  , advanceLevel
  , advanceToLevel
  , getNow
  , getLevel
  , getApproximateBlockInterval
  , branchout
  , offshoot
  , setVotingPowers
  , inBatch
  , uncapsCleveland
  , uncapsEmulated

  -- * Primitives re-exports
  , EntrypointRef (..)
  , VotingPowers
  , mkVotingPowers
  , mkVotingPowersFromMap

  -- * Assertions
  , failure
  , assert
  , (@==)
  , (@/=)
  , (@@==)
  , (@@/=)
  , checkCompares
  , checkComparesWith
  , Showing(..)

    -- * Exception handling
  , attempt
  , catchTransferFailure
  , checkTransferFailure
  , expectTransferFailure
  , expectFailedWith
  , expectError
  , expectCustomError
  , expectCustomError_
  , expectCustomErrorNoArg
  , expectNumericError
  -- ** Exception predicates
  , TransferFailurePredicate
  , shiftOverflow
  , emptyTransaction
  , badParameter
  , failedWith
  , addressIs
  -- ** @FAILWITH@ errors
  -- | Convert the many error formats available in @morley@ and @lorentz@ to t'Morley.Michelson.Typed.Existential.SomeConstant'.
  , constant
  , lerror
  , customError
  , customError_
  , customErrorNoArg
  , numericError

  -- * Helpers
  , auto
  , ep
  , (?-)

  -- * Integration with @hedgehog@
  , clevelandTestProp

  -- * Config (reexports)
  , TezosClientEnv(..)
  , Client.MorleyClientEnv
  , Client.MorleyClientEnv'(..)
  , NetworkEnv (..)
  , MorleyLogs

  -- * AsRPC
  , Client.AsRPC
  , Client.deriveRPC
  , Client.deriveRPCWithStrategy
  , Client.deriveManyRPC
  , Client.deriveManyRPCWithStrategy

  -- * Utilities
  , mapEach
  , forEach
  ) where

import Lorentz (EntrypointRef(..))
import qualified Morley.Client as Client
import Morley.Client.TezosClient.Types (TezosClientEnv(..))
import Morley.Michelson.Interpret (MorleyLogs)
import Morley.Michelson.Runtime (VotingPowers, mkVotingPowers, mkVotingPowersFromMap)
import Test.Cleveland.Instances ()
import Test.Cleveland.Internal.Abstract
import Test.Cleveland.Internal.Caps
import Test.Cleveland.Internal.Client
import Test.Cleveland.Internal.Pure
import Test.Cleveland.Util
