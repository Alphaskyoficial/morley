-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Unit tests for Lorentz.
module Test.Cleveland.Lorentz.Unit
    ( expectContractEntrypoints
    ) where

import Lorentz hiding (contract)

import Test.HUnit (Assertion, assertFailure)

import Morley.Michelson.Typed (convertContract, flattenEntrypoints)
import Test.Cleveland.Michelson.Unit (matchContractEntrypoints)

-- | Expect the given contract to have some specific entrypoints.
expectContractEntrypoints
  :: forall expectedEps contractEps st.
     ( NiceParameterFull expectedEps
     , NiceParameterFull contractEps
     , NiceStorage st
     )
  => Contract contractEps st -> Assertion
expectContractEntrypoints contract =
  withDict (niceParameterEvi @expectedEps) $
  withDict (niceParameterEvi @contractEps) $
  withDict (niceStorageEvi @st) $ do
    let entrypoints = flattenEntrypoints $ parameterEntrypointsToNotes @expectedEps
        contract' = convertContract . toMichelsonContract $ contract
    case matchContractEntrypoints contract' entrypoints of
      Left eps -> assertFailure $ "Some entrypoints were not found " <> show eps
      Right _ -> pass
