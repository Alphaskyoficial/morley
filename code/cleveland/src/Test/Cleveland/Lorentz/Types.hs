-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Basic types for the test framework.
module Test.Cleveland.Lorentz.Types
  ( ContractHandler (..)
  , chNiceParameterEvi
  , chNiceStorageEvi

  , ToStorageType (..)

  -- * Notes
  -- $noteTAddress
  ) where

import Data.Constraint (Dict(..), mapDict)
import Fmt (Buildable(..), (+|), (|+))

import Lorentz.Address
import Lorentz.Constraints
import qualified Morley.Michelson.Typed as T

-- | Handler to a contract.
--
-- This is what you get when originating a contract and that allows further
-- operations with the contract within the test framework.
--
-- Note that this is part of the testing framework and exists solely in Haskell
-- world, so it has no 'T.IsoValue' and related instances and cannot be used in
-- Lorentz code.
data ContractHandler (cp :: Type) (st :: Type) =
  (NiceParameterFull cp, NiceStorage st) =>
  ContractHandler
  { chContractName :: Text
  , chAddress :: Address
  }

deriving stock instance Show (ContractHandler cp st)

instance Buildable (ContractHandler cp st) where
  build (ContractHandler name addr) =
    "<handler to '" +| name |+ "' / " +| addr |+ ">"

instance ToAddress (ContractHandler cp st) where
  toAddress = chAddress

-- TODO [#577]: simplify this instance once fundep in ToTAddress is added
instance (cp' ~ cp) => ToTAddress cp' (ContractHandler cp st) where
  toTAddress = toTAddress . toAddress

instance ToContractRef arg (TAddress cp) => ToContractRef arg (ContractHandler cp st) where
  toContractRef = toContractRef . toTAddress @cp

-- | Extract the evidence in typed Michelson that the parameter type is valid
-- for such scope.
chNiceParameterEvi :: forall param st. ContractHandler param st -> Dict (T.ParameterScope $ T.ToT st)
chNiceParameterEvi ContractHandler{} = mapDict (niceParameterEvi @st) Dict

-- | Extract the evidence in typed Michelson that the storage type is valid
-- for such scope.
chNiceStorageEvi :: forall param st. ContractHandler param st -> Dict (T.StorageScope $ T.ToT st)
chNiceStorageEvi ContractHandler{} = mapDict (niceStorageEvi @st) Dict

-- | Declares that @addr@ points to an entity with a storage.
--
-- @addr@ may fix storage type or may not - in the latter case the caller
-- has to specify the storage type explicitly via type annotation.
class ToAddress addr => ToStorageType st addr where
  -- | Pick proof of that storage type is valid.
  pickNiceStorage :: addr -> Dict (NiceStorage st)

instance NiceStorage st => ToStorageType st Address where
  pickNiceStorage _ = Dict

instance (st ~ st') => ToStorageType st' (ContractHandler cp st) where
  pickNiceStorage ContractHandler{} = Dict

{- $noteTAddress

  == A note on 'TAddress' instance for 'ToStorageType'

  'TAddress' isn't intended to be a part of the Cleveland API.
  In the absolute majority of cases, if one is interested in both parameter
  and storage, then they should use 'ContractHandler', as the storage type
  needs to be known either way. If one isn't interested in storage, they
  presumably wouldn't call functions to get storage. Hence, this instance
  wouldn't be particularly useful. Legacy code using 'TAddress' instead of
  'ContractHandler' should be preferably updated, if possible. If nothing
  else, 'toAddress' can be used as a stopgap measure.
-}
