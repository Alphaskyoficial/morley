-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Functions to import contracts to be used in tests.
module Test.Cleveland.Lorentz.Import
  ( -- * Read, parse, typecheck contracts
    importContract
  , embedContract
  , embedContractM
  , M.ImportContractError (..)

    -- * Read, parse, typecheck values
  , importValue
  , embedValue
  , embedValueM
  , M.ImportValueError (..)

    -- * Notes
    -- $embedDepends
  ) where

import Fmt (build, pretty)
import qualified Language.Haskell.TH as TH

import qualified Lorentz as L
import Lorentz.Base
import Lorentz.Constraints
import qualified Morley.Michelson.Typed as T
import Morley.Util.Markdown
import qualified Test.Cleveland.Michelson.Import as M

mkImportedContract
  :: (NiceParameterFull cp, NiceStorage st)
  => FilePath -> T.Contract (T.ToT cp) (T.ToT st) -> Contract cp st
mkImportedContract path cMichelsonContract = Contract
  { cDocumentedCode =
      L.fakeCoercing $
        L.docGroup "Imported contract" $
          L.doc $ L.DDescription $ "Read from " <> mdTicked (build path)
  , ..
  }

-- | Import contract from a given 'FilePath'.
importContract
  :: forall cp st.
     (NiceParameterFull cp, NiceStorage st)
  => FilePath -> IO (Contract cp st)
importContract file =
  mkImportedContract file <$> M.importContract file

{- | Import a contract at compile time assuming its expected type is known.

Use it like:

> myContract :: Contract Parameter Storage
> myContract = $$(embedContract "my_contract.tz")

or

> let myContract = $$(embedContract @Parameter @Storage "my_contract.tz")

See also the note in "Test.Cleveland.Lorentz.Import#embedDepends"
-}
embedContract
  :: forall cp st. (NiceParameterFull cp, NiceStorage st)
  => FilePath -> TH.TExpQ (Contract cp st)
embedContract path = embedContractM (pure path)

-- | Version of 'embedContract' that accepts a filepath constructor in IO.
--
-- Useful when the path should depend on environmental variables or other
-- user input.
--
-- See also the note in "Test.Cleveland.Lorentz.Import#embedDepends"
embedContractM
  :: forall cp st. (NiceParameterFull cp, NiceStorage st)
  => IO FilePath -> TH.TExpQ (Contract cp st)
embedContractM pathM = do
  path <- TH.runIO pathM
  contract <- M.embedTextFile path
  case M.readContract @(T.ToT cp) @(T.ToT st) path contract of
    Left e ->
      -- Emit a compiler error if the contract cannot be read.
      fail (pretty e)
    Right _ ->
      -- Emit a haskell expression that reads the contract.
      [||
        -- Note: it's ok to use `error` here, because we just proved that the contract
        -- can be parsed+typechecked.
        either (error . pretty) (mkImportedContract path) $
          M.readContract path contract
      ||]

-- | Import a value from a given 'FilePath'
importValue :: forall a . T.IsoValue a => FilePath -> IO a
importValue = fmap T.fromVal . M.importValue

{- | Import a value from a given 'FilePath' at compile time
and embed it as a value using Template Haskell, f. ex.

> let someAddress = $$(embedValue @Address "/path/to/addressFile.tz")

See also the note in "Test.Cleveland.Lorentz.Import#embedDepends"
-}
embedValue :: forall a . T.IsoValue a => FilePath -> TH.TExpQ a
embedValue = embedValueM . pure

-- | A variant of 'embedValue' that accepts 'FilePath' in 'IO'.
--
-- Can be useful when 'FilePath' depends on the environment.
--
-- See also the note in "Test.Cleveland.Lorentz.Import#embedDepends"
embedValueM :: forall a . T.IsoValue a => IO FilePath -> TH.TExpQ a
embedValueM pathM = do
  path <- TH.runIO pathM
  rawValue <- M.embedTextFile path
  case M.readValue @(T.ToT a) path rawValue of
    Left e -> fail (pretty e)
    Right _ ->
          [||
            -- Note: it's ok to use `error` here, because we just proved that the value
            -- can be parsed+typechecked.
            either (error . pretty) T.fromVal $
              M.readValue path rawValue
          ||]

{- $embedDepends

= On 'FilePath' argument with 'embedContract', 'embedValue' and variants #embedDepends#

The 'FilePath' argument is specified relative to the project root (if
using cabal-install or stack, the directory containing the Cabal file and/or @package.yaml@).

As an additional caveat, any files embedded this way are essentially compile-time dependencies. However,
build systems can't track these automatically. In general, it's advisable to add the files used
with 'embedContract', 'embedValue' and variants to the @extra-source-files@ section of the Cabal
file or @package.yaml@, if possible.
-}
