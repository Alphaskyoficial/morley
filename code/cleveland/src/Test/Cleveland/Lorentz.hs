-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Cleveland.Lorentz
  ( -- * Importing a contract
    specWithContract
  , specWithTypedContract
  , specWithUntypedContract
  , importContract
  , embedContract
  , embedContractM

  -- * Importing a value
  , importValue
  , embedValue
  , embedValueM

  -- * Unit testing
  , ContractReturn
  , ContractPropValidator
  , contractProp
  , contractPropVal
  , expectContractEntrypoints

  -- * Basic types
  , ContractHandler (..)
  , chNiceParameterEvi
  , chNiceStorageEvi

  , ToStorageType (..)

  -- * Integrational testing
  -- ** Testing engine
  , IntegrationalScenario
  , IntegrationalScenarioM
  , TestError (..)
  , integrationalTestExpectation
  , integrationalTestProp
  , lOriginate
  , lOriginateEmpty
  , lTransfer
  , lCall
  , lCallEP
  , EntrypointRef (..)
  , lCallDef
  , integrationalFail
  , unexpectedInterpreterError
  , setMaxSteps
  , setNow
  , rewindTime
  , withSender
  , setChainId
  , branchout
  , (?-)
  , offshoot

  -- ** Validators
  , expectNoStorageUpdates
  , lExpectStorageUpdate
  , lExpectBalance
  , lExpectStorage
  , lExpectStorageConst

  -- ** Errors
  , attempt
  , expectError
  , catchExpectedError
  , lExpectMichelsonFailed
  , lExpectFailWith
  , lExpectError
  , lExpectErrorNumeric
  , lExpectCustomError
  , lExpectCustomErrorNumeric
  , lExpectCustomError_
  , lExpectCustomErrorNumeric_
  , lExpectConsumerStorage
  , lExpectViewConsumerStorage
  , lExpectAddressLogs
  , lExpectScenarioLogs

  -- ** Various
  , TxData (..)
  , TxParam (..)
  , genesisAddresses
  , genesisAddress
  , genesisAddress1
  , genesisAddress2
  , genesisAddress3
  , genesisAddress4
  , genesisAddress5
  , genesisAddress6

  -- * Autodoc testing
  , runDocTests
  , testLorentzDoc
  , excludeDocTests

  -- * General utilities
  , failedTest
  , succeededTest
  , eitherIsLeft
  , eitherIsRight
  , meanTimeUpperBoundProp
  , meanTimeUpperBoundPropNF

  -- * Re-exports
  --
  -- | These functions from @Time@ are re-exported here to make it convenient to call
  -- 'meanTimeUpperBoundProp' and 'meanTimeUpperBoundPropNF'.
  , mcs, ms, sec, minute

  -- * Dummy values
  , dummyContractEnv

  -- * Special contracts for testing
  , contractConsumer
  ) where

import Test.Cleveland.Doc.Lorentz
import Test.Cleveland.Lorentz.Consumer
import Test.Cleveland.Lorentz.Import
import Test.Cleveland.Lorentz.Integrational
import Test.Cleveland.Lorentz.Types
import Test.Cleveland.Lorentz.Unit
import Test.Cleveland.Michelson.Dummy
import Test.Cleveland.Michelson.Import
  (specWithContract, specWithTypedContract, specWithUntypedContract)
import Test.Cleveland.Michelson.Unit
import Test.Cleveland.Util
