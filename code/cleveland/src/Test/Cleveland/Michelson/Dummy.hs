-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Dummy data to be used in tests where it's not essential.

module Test.Cleveland.Michelson.Dummy
  ( dummyNow
  , dummyLevel
  , dummyMaxSteps
  , dummyBigMapCounter
  , dummyContractEnv
  , dummyGlobalCounter
  , dummyOrigination
  , dummyChainId
  ) where

import Morley.Michelson.Runtime.Dummy
import Morley.Tezos.Core
