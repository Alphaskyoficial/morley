-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
{-# OPTIONS_GHC -Wno-deprecations #-}

-- | Integration with integrational testing engine (pun intended).
module Test.Cleveland.Internal.Pure
  ( PureM(..)
  , runEmulated
  , clevelandTestProp
  , clevelandImpl
  , emulatedImpl

  -- * Initial environment for Emulated tests
  , initEnv

  -- * Support functions
  , scenarioToIO
  , askState
  , askAliases
  , askInternalState
  , failedInsideBranch
  ) where

import Control.Lens (to)
import Data.Constraint (withDict, (\\))
import qualified Data.Map as Map
import Data.Monoid (Ap(..))
import qualified Data.Set as Set
import Data.Type.Equality (type (:~:)(Refl))
import Fmt (Builder, pretty, unlinesF, (+|), (|+))
import Hedgehog (MonadTest, evalIO)
import Time (Second, toNum, toUnit)

import Lorentz (KnownValue, NiceComparable, pattern DefEpName, toAddress, zeroMutez)
import Lorentz.Entrypoints (TrustEpName(..))
import Morley.Client (Alias(..), AliasHint(..))
import Morley.Client.RPC.AsRPC (rpcStorageScopeEvi, valueAsRPC)
import Morley.Micheline (Expression, toExpression)
import Morley.Michelson.Interpret
  (InterpretError(..), MichelsonFailed(..), MichelsonFailureWithStack(..))
import Morley.Michelson.Runtime (AddressState(..), ContractState(..), ExecutorError'(..))
import Morley.Michelson.Runtime.GState
  (GState(..), asBalance, genesisSecretKey, gsAddressesL, gsChainIdL)
import Morley.Michelson.Typed
  (BigMapId(..), IsoValue, SingI, StorageScope, ToT, Value, Value'(..), castM, dfsFoldMapValue,
  fromVal, requireEq, toVal)
import qualified Morley.Michelson.Typed as T
import Morley.Tezos.Address
import Morley.Tezos.Core (unsafeSubMutez)
import Morley.Tezos.Crypto (SecretKey(..), detSecretKey, sign, toPublic)
import Morley.Util.Named
import Test.Cleveland.Internal.Abstract
import Test.Cleveland.Internal.Exceptions (addCallStack, catchWithCallStack, throwWithCallStack)
import Test.Cleveland.Lorentz
import Test.Cleveland.Michelson.Integrational
  (IntegrationalScenarioM(..), InternalState, ScenarioError(..), addrNameToAddr,
  appendScenarioBranch, emptyScenarioBranch, getAddressScenarioLogs, getScenarioLogs, initIS,
  isGState, isLevel, isNow, modifyLevel, originate, setVotingPowers)
import Test.Cleveland.Util (ceilingUnit)

-- In this implementation we do not prefix aliases, so 'Alias' and 'AliasHint'
-- are identical and conversions between them are safe.
hintToAlias :: AliasHint -> Alias
hintToAlias (AliasHint a) = Alias a

scenarioToIO :: Alias -> ClevelandScenario PureM -> IO ()
scenarioToIO origAlias scenario = do
  env <- newIORef (initEnv origAlias)
  runReaderT (unPureM (scenario clevelandImpl genesisAddress)) env

-- | Run a 'ClevelandScenario' via the "Morley.Michelson.Runtime" emulator,
-- inside an @hedgehog@ property.
clevelandTestProp :: (MonadIO m, MonadTest m) => ClevelandScenario PureM -> m ()
clevelandTestProp = evalIO . scenarioToIO (Alias "moneybag")

-- | Run an 'EmulatedScenario' via "Test.Cleveland.Michelson.Integrational".
runEmulated :: EmulatedScenario PureM -> ClevelandImpl PureM -> Address -> PureM ()
runEmulated scenario = scenario emulatedImpl

data PureState = PureState
  { psAliases :: Aliases
  , psDefaultAliasesCounter :: DefaultAliasCounter
  , psInternalState :: InternalState
  , psRefillableAddresses :: Set Address
  }

newtype PureM a = PureM
  { unPureM :: ReaderT (IORef PureState) IO a
  }
  deriving newtype (Functor, Applicative, Monad, MonadIO,
                    MonadThrow, MonadCatch, MonadReader (IORef PureState))

type Aliases = Map Alias AliasData

-- | Datatype to store alias data, we store optional 'SecretKey' in addition
-- to 'Address' in order to support bytes signing.
data AliasData = AliasData
  { adAddress :: Address
  , adMbSecretKey :: Maybe SecretKey
  }

emulatedImpl :: EmulatedImpl PureM
emulatedImpl =
  EmulatedImpl
    { eiBranchout = supportBranchout
    , eiOffshoot = \name scenario -> supportBranchout [(name, scenario)]
    , eiGetStorage = addCallStack . exceptionHandler . getStorageImpl
    , eiGetMorleyLogs = liftIntegrational getScenarioLogs
    , eiGetAddressMorleyLogs = liftIntegrational . getAddressScenarioLogs . toAddress
    , eiSetVotingPowers = liftIntegrational . setVotingPowers
    }
  where
    supportBranchout :: [(Text, PureM ())] -> PureM ()
    supportBranchout = \(scenarios :: [(Text, PureM ())]) ->
      forM_ scenarios $ \(name, scenario) -> do
        aliasesState <- askState
        newRef <- newIORef aliasesState
        local (\_ -> newRef) scenario `catchWithCallStack` \originalCallStackMb err ->
          maybe throwM throwWithCallStack originalCallStackMb $ failedInsideBranch name err


clevelandImpl :: ClevelandImpl PureM
clevelandImpl = mapClevelandImplExceptions (addCallStack . exceptionHandler)
  ClevelandImpl
  { ciOpsImpl = \(Sender sender) -> ClevelandOpsImpl
    { coiRunOperationBatch = mapM \case
        OriginateOp UntypedOriginateData{..} -> do
          ref <-
            liftIntegrational $ originate uodContract (pretty uodName) uodStorage uodBalance
          OriginateResult <$> saveAlias uodName (toAddress ref) Nothing
        TransferOp TransferData{..} -> do
          let fromAddr = #from .! sender
          let toAddr = #to .! toAddress tdTo
          refillable <- isAddressRefillable sender
          when refillable $ do
            balance <- cmiGetBalance sender
            when (balance < tdAmount) $ do
              let moneybag = #from .! genesisAddress
                  toSender = #to .! sender
              liftIntegrational $ lTransfer @() moneybag toSender (unsafeSubMutez tdAmount balance)
                (TrustEpName DefEpName) ()
          -- Here @toAddr@ is 'Address', so we can not check anything
          -- about it and assume that entrypoint is correct. We pass
          -- unit as contract parameter because it won't be checked
          -- anyway.
          liftIntegrational $ lTransfer @() fromAddr toAddr tdAmount
            (TrustEpName tdEntrypoint) tdParameter

          return TransferResult
    }

  , ciMiscImpl = ClevelandMiscImpl
    { cmiRunIO = \action -> liftIO action
    , cmiResolveAddress = resolve

    , cmiSignBytes = \bs addr -> do
        -- TODO [#248]: make sure this performs fast
        alias <- getAlias addr
        aliases <- askAliases
        let mbMbSk = Map.lookup alias aliases
        mbSk <- maybe (unknownAlias alias) (pure . adMbSecretKey) mbMbSk
        case mbSk of
          Nothing ->
            failWith . CustomTestError .
            mappend "Given address doesn't have known associated secret key: " . show $ alias
          Just sk -> liftIO $ sign sk bs

    , cmiGenKey = \alias -> do
      aliasHint <- resolveSpecificOrDefaultAliasHint alias
      smartGenKey Nothing aliasHint

    , cmiGenFreshKey =
        \alias -> do
          aliasHint <- resolveSpecificOrDefaultAliasHint alias
          aliases <- askAliases
          let mbSk = Map.lookup (hintToAlias aliasHint) aliases
          smartGenKey (adAddress <$> mbSk) aliasHint

    , cmiOriginateLargeUntyped = originateUntyped

    -- Comments are not supported by integrational testing engine (yet).
    , cmiComment = const pass
    , cmiGetPublicKey = \addr -> do
        aliases <- askAliases
        let mbAliasInfo = fmap snd $ find (\(_, AliasData addr' _) -> addr == addr') (Map.toList aliases)
        aliasInfo <- maybe (unknownAddress addr) pure mbAliasInfo
        case adMbSecretKey aliasInfo of
          Nothing ->
            failWith . CustomTestError .
            mappend "Given address doesn't have known associated public key: " . show $ addr
          Just sk -> pure $ toPublic sk
    , cmiGetChainId = do
        is <- askInternalState
        pure $ is^.isGState.gsChainIdL
    , cmiAdvanceTime = \time -> do
        liftIntegrational $
          rewindTime $
            toNum @Second @Integer $ ceilingUnit $ toUnit @Second time

    , cmiAdvanceToLevel = \fn -> liftIntegrational $
        -- do not go back in levels
        modifyLevel (\cl -> max (fn cl) cl)

    , cmiGetNow = liftIntegrational $ use isNow
    , cmiGetLevel = liftIntegrational $ use isLevel
    , cmiGetApproximateBlockInterval = pure $ sec 1
    , cmiFailure = \msg -> failWith $ CustomTestError (pretty msg)
    , cmiAttempt = try
    , cmiMarkAddressRefillable = setAddressRefillable
    , ..
    }
  }
  where
    isAddressRefillable addr = do
      stRef <- ask
      Set.member addr . psRefillableAddresses <$> readIORef stRef

    setAddressRefillable addr = do
      stRef <- ask
      modifyIORef stRef $ \st@PureState{..} ->
        st{psRefillableAddresses=Set.insert addr psRefillableAddresses}

    cmiGetBalance addr = do
      GState{..} <- liftIntegrational $ use isGState
      return $ maybe zeroMutez asBalance $ Map.lookup addr gsAddresses

    originateUntyped :: Sender -> UntypedOriginateData -> PureM Address
    originateUntyped _ UntypedOriginateData {..} = do
      ref <- liftIntegrational $
        originate uodContract (pretty uodName) uodStorage uodBalance
      saveAlias uodName (toAddress ref) Nothing

    cmiGetBigMapValueMaybe
      :: forall k v.
         (NiceComparable k, IsoValue v)
      => BigMapId k v
      -> k
      -> PureM (Maybe v)
    cmiGetBigMapValueMaybe bmId k = do
      mbBigMap <- findBigMapById bmId
      case mbBigMap of
        Nothing      -> pure Nothing
        Just bigMap  -> pure $ fromVal @v <$> Map.lookup (toVal k) bigMap

    cmiGetAllBigMapValuesMaybe
      :: forall k v.
         (NiceComparable k, IsoValue v)
      => BigMapId k v
      -> PureM (Maybe [v])
    cmiGetAllBigMapValuesMaybe bmId = do
      mbBigMap <- findBigMapById bmId
      case mbBigMap of
        Nothing     -> pure Nothing
        Just bigMap -> pure $ Just $ fromVal @v <$> Map.elems bigMap

    -- | Traverse storage values of all contracts and looks for a big_map with the given ID.
    -- If multiple big_maps with the given ID are found, it fails with error.
    findBigMapById
      :: forall k v.
         (NiceComparable k, IsoValue v)
      => BigMapId k v
      -> PureM (Maybe (Map (Value (ToT k)) (Value (ToT v))))
    findBigMapById (BigMapId bigMapId) = do
      addresses <- liftIntegrational $ use $ isGState . gsAddressesL . to Map.elems

      let Ap result =
            flip foldMap addresses \case
              ASContract ContractState{csStorage} ->
                findBigMapInStorage csStorage bigMapId
              ASSimple {} -> Ap $ Right []

      case result of
        -- The RPC does not distinguish between "the bigmap does not exist"
        -- and "the bigmap exists, but the key doesn't", so we mimic the RPC's behaviour here.
        -- We simply return `Nothing` in both cases.
        Right [] -> pure Nothing
        Right [bigMap] -> pure $ Just bigMap
        Right bigMaps ->
          error $ pretty $ unlinesF @_ @Builder
            [ "Expected all big_maps to have unique IDs, but found " +| length bigMaps |+ " big_maps with the ID " +| bigMapId |+ "."
            , "This is most likely a bug."
            ]
        Left err -> failWith err

    -- | Traverse a storage value and looks for a big_map with the given ID.
    -- If multiple big_maps with the given ID are found, they'll all be returned.
    findBigMapInStorage
      :: forall k v st. (SingI k, SingI v)
      => Value st -> Natural -> Ap (Either TestError) [Map (Value k) (Value v)]
    findBigMapInStorage storage bigMapId =
      dfsFoldMapValue
        (\case
            VBigMap (Just bigMapId') (bigMap :: Map (Value k') (Value v'))
              | bigMapId == bigMapId' -> do
                  Refl <- requireEq @k' @k (Ap . Left ... UnexpectedBigMapKeyType)
                  Refl <- requireEq @v' @v (Ap . Left ... UnexpectedBigMapValueType)
                  pure [bigMap]
            _ -> Ap $ Right []
        )
        storage

    cmiGetStorage
      :: forall st. (KnownValue st)
      => Address -> PureM st
    cmiGetStorage addr = do
      getStorageAsRPC addr \(storage :: Value actualT) ->
        fromVal <$> castM @actualT @(ToT st) storage (failWith ... UnexpectedStorageType)

    cmiGetStorageExpr :: Address -> PureM Expression
    cmiGetStorageExpr addr =
      getStorageAsRPC addr (pure . toExpression)

    -- | In a real chain, when we retrieve a contract's storage via the Tezos RPC,
    -- the storage expression will have all the big_maps replaced with their respective big_map IDs.
    --
    -- Here, we mimic the RPC's behaviour.
    --
    -- We expect all big_maps in the storage to already have an ID.
    -- IDs are assigned to big_maps by the interpreter/runtime when:
    --   * A contract with big_maps in its storage is originated
    --   * A transfer is made and the parameter contains big_maps
    --   * A contract's code is run and it calls `EMPTY_BIG_MAP`, `DUP` or `DUP n`.
    getStorageAsRPC :: Address -> (forall t. StorageScope t => Value t -> PureM r) -> PureM r
    getStorageAsRPC addr cont = do
      ContractState _ _ (storage :: Value t) <- contractStorage addr
      cont (valueAsRPC storage) \\ rpcStorageScopeEvi @t

    getAlias :: Address -> PureM Alias
    getAlias addr = do
      aliases <- askAliases
      let maybeAlias = (fmap fst . find (\(_, AliasData addr' _) -> addr == addr') . Map.toList) aliases
      maybe (unknownAddress addr) pure maybeAlias

    -- Generate a fresh address which was never generated for given alias.
    -- If the address is not saved, we use the alias as its seed.
    -- Otherwise we concatenate the alias with the saved address.
    smartGenKey :: Maybe Address -> AliasHint -> PureM Address
    smartGenKey existingAddr aliasHint@(AliasHint aliasTxt) =
      let
        seed = maybe aliasTxt (mappend aliasTxt . pretty) existingAddr
        sk = detSecretKey (encodeUtf8 seed)
        addr = detGenKeyAddress (encodeUtf8 seed)
       in saveAlias aliasHint addr $ Just sk

    saveAlias :: AliasHint -> Address -> Maybe SecretKey -> PureM Address
    saveAlias name addr mbSk = do
      ref <- ask
      modifyIORef ref $ \st ->
        st { psAliases = Map.insert (hintToAlias name) (AliasData addr mbSk) $ psAliases st }
      pure addr

    resolveSpecificOrDefaultAliasHint (SpecificAliasHint aliasHint) =
      return aliasHint
    resolveSpecificOrDefaultAliasHint (DefaultAliasHint) = do
      stRef <- ask
      st <- readIORef stRef
      let (DefaultAliasCounter counter) = psDefaultAliasesCounter st
      writeIORef stRef $
        st { psDefaultAliasesCounter = DefaultAliasCounter $ counter + 1 }
      return $ mkDefaultAlias counter

exceptionHandler :: PureM a -> PureM a
exceptionHandler action = try action >>= \case
  Left err -> exceptionToTransferFailure err >>= throwM
  Right res -> return res
  where
    exceptionToTransferFailure :: ScenarioError -> PureM TransferFailure
    exceptionToTransferFailure (ScenarioError _ e) = case e of
      InterpreterError err -> case err of
        EEZeroTransaction addr -> return $ EmptyTransaction (addrNameToAddr addr)
        EEIllTypedParameter addr _ -> return $ BadParameter (addrNameToAddr addr)
        EEUnexpectedParameterType addr _ _ -> return $ BadParameter (addrNameToAddr addr)
        EEInterpreterFailed addr (InterpretError (mfwsFailed -> michelsonFailed, _)) ->
          case michelsonFailed of
            MichelsonFailedWith val -> return $ FailedWith (addrNameToAddr addr) (toExpression val)
            MichelsonArithError (T.ShiftArithError errType _ _) | isOverflow errType -> return $ ShiftOverflow (addrNameToAddr addr)
            _ -> throwM err
        otherErr -> throwM otherErr
      otherErr -> throwM otherErr

    isOverflow :: T.ShiftArithErrorType -> Bool
    isOverflow = \case
      T.LslOverflow -> True
      T.LsrUnderflow -> False

getStorageImpl
  :: forall st addr. (ToStorageType st addr)
  => addr -> PureM st
getStorageImpl addr = do
  withDict (pickNiceStorage @st addr) $ do
    ContractState _ _ (storage :: Value actualT) <- contractStorage (toAddress addr)
    val <- castM @actualT @(ToT st) storage (failWith ... UnexpectedStorageType)
    pure $ T.fromVal val

-- Attempt to retrieve a ContractState given for the given address. Fails if the
-- address is unknown or the address is a simple address (contract without
-- code and storage).
contractStorage :: Address -> PureM ContractState
contractStorage addr = do
  GState{..} <- liftIntegrational $ use isGState
  case Map.lookup addr gsAddresses of
    Just (ASContract contractState) -> pure contractState
    Just (ASSimple {}) -> failWith . CustomTestError $
      "Expected address to be contract with storage, but it's a simple address: " <> show addr
    Nothing -> unknownAddress addr

resolve :: Alias -> PureM Address
resolve name = do
  aliases <- askAliases
  let maybeAddress = Map.lookup name aliases
  maybe (unknownAlias name) (pure . adAddress) maybeAddress

unknownAddress :: Address -> PureM whatever
unknownAddress =
  failWith . CustomTestError .
  mappend "Unknown address provided: " . pretty

unknownAlias :: Alias -> PureM whatever
unknownAlias =
  failWith . CustomTestError .
  mappend "Unknown address alias: " . pretty

failWith :: TestError -> PureM a
failWith = liftIntegrational . integrationalFail

liftIntegrational :: IntegrationalScenarioM a -> PureM a
liftIntegrational scenario = do
  st <- askInternalState
  updateAndReturn $ runState (runExceptT (unIntegrationalScenarioM scenario)) st
  where
    updateAndReturn (res, curState) = do
      ref <- ask
      modifyIORef ref (\st -> st { psInternalState = curState })
      either throwM return res


----------------------------------------------------------------------------
-- Support functions
----------------------------------------------------------------------------

initAliases :: Alias -> Aliases
initAliases alias = one ( alias
                        , AliasData genesisAddress $
                          Just $ genesisSecretKey
                        )

initEnv :: Alias -> PureState
initEnv alias = PureState (initAliases alias) (DefaultAliasCounter 0) initIS Set.empty

askState :: PureM PureState
askState = ask >>= readIORef

askAliases :: PureM Aliases
askAliases = psAliases <$> askState

askInternalState :: PureM InternalState
askInternalState = psInternalState <$> askState

failedInsideBranch :: Text -> SomeException -> FailedInBranch
failedInsideBranch name err = case fromException @FailedInBranch err of
  Just (FailedInBranch branch failure) ->
    FailedInBranch (appendScenarioBranch name branch) failure
  Nothing ->
    FailedInBranch (appendScenarioBranch name emptyScenarioBranch) err
