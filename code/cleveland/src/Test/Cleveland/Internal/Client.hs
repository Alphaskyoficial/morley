-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Implementation that works with real Tezos network, it
-- talks to a Tezos node and uses @tezos-client@.

module Test.Cleveland.Internal.Client
  ( runNetworkScenario
  , networkImpl
  , revealKeyUnlessRevealed
  , ClientM (..)
  , TestError(..)
  , MoneybagConfigurationException (..)

  -- * Environment
  , NetworkEnv (..)

  -- * Lens for 'NetworkEnv'
  , neMorleyClientEnvL
  , neSecretKeyL
  , neMoneybagAliasL

  -- * Error types
  , InternalNetworkScenarioError(..)
  ) where

import Data.Constraint ((\\))
import Data.Default (def)
import qualified Data.Set as Set
import Data.Time (NominalDiffTime, UTCTime, diffUTCTime, secondsToNominalDiffTime)
import Fmt (Buildable(build), Builder, pretty, unlinesF, (+|), (|+))
import Named (arg)
import System.IO (hFlush)
import Time (KnownDivRat, Second, Time, sec, threadDelay, toNum, toUnit)

import Lorentz (NicePackedValue, toAddress)
import Lorentz.Constraints.Scopes (NiceUnpackedValue, niceParameterEvi, niceUnpackedValueEvi)
import Morley.Client
  (AddressOrAlias(..), Alias, MorleyClientEnv, disableAlphanetWarning, runMorleyClientM)
import qualified Morley.Client as Client
import Morley.Client.Logging (logInfo, logWarning)
import qualified Morley.Client.RPC.Error as RPC (ClientRpcError(..))
import Morley.Client.RPC.Types
  (AppliedResult(..), BlockConstants(bcHeader), BlockHeaderNoHash(bhnhLevel, bhnhTimestamp),
  BlockId(..), OperationHash,
  ProtocolParameters(ProtocolParameters, ppCostPerByte, ppMinimalBlockDelay, ppOriginationSize))
import Morley.Micheline
  (Expression, FromExpression(fromExpression), StringEncode(..), TezosMutez(unTezosMutez))
import Morley.Michelson.TypeCheck (typeCheckContractAndStorage, typeCheckingWith)
import Morley.Michelson.Typed (BigMapId, SomeContractAndStorage(..), toVal)
import qualified Morley.Michelson.Typed as T
import qualified Morley.Michelson.Untyped as U
import Morley.Tezos.Address (Address, mkKeyAddress)
import Morley.Tezos.Core as Tezos
  (Mutez, Timestamp(..), addMutez, subMutez, timestampFromUTCTime, unsafeAddMutez, unsafeMkMutez,
  unsafeMulMutez, unsafeSubMutez)
import Morley.Tezos.Crypto
import qualified Morley.Tezos.Crypto as Crypto
import Morley.Util.Exception
import Morley.Util.Lens (makeLensesWith, postfixLFields)
import Test.Cleveland.Internal.Abstract
import Test.Cleveland.Internal.Exceptions (addCallStack, throwWithCallStack)
import Test.Cleveland.Util (ceilingUnit)

data NetworkEnv = NetworkEnv
  { neMorleyClientEnv :: MorleyClientEnv
  , neSecretKey :: Maybe Crypto.SecretKey
  , neMoneybagAlias :: Alias
  }

makeLensesWith postfixLFields ''NetworkEnv


-- | This error designates that necessary preparations for running tests
-- are not made.
data MoneybagConfigurationException
  = NoMoneybagAddress Alias
  | TwoMoneybagKeys Alias SecretKey Address
  deriving stock (Generic, Show, Eq)

instance Buildable MoneybagConfigurationException where
  build = \case
    NoMoneybagAddress alias -> unlinesF @_ @Builder
      [ "Moneybag alias is not registered in the tezos node: " <> build alias
      , ""
      , "Cleveland's network tests require a special address with plenty of XTZ for"
      , "originating contracts and performing transfers."
      , ""
      , "By default, Cleveland expects an account with the alias 'moneybag' to already exist."
      , "If no such alias exists, you can choose to either:"
      , "  * Use a different alias, supplied via '--cleveland-moneybag-alias'."
      , "  * Import a moneybag account, by supplying its secret key via '--cleveland-moneybag-secret-key'."
      ]
    TwoMoneybagKeys alias envKey existingAddress -> unlinesF @_ @Builder
      [ "Tried to import the secret key supplied via '--cleveland-moneybag-secret-key' and"
      , "associate it with the alias '" +| alias |+ "', but the alias already exists."
      , ""
      , "  --cleveland-moneybag-secret-key: " <> build envKey
      , "  Existing address             : " <> build existingAddress
      , ""
      , "Possible fix:"
      , "  * If you wish to use the existing address, please remove the '--cleveland-moneybag-secret-key' option."
      , "  * Otherwise, please supply a different alias via '--cleveland-moneybag-alias'."
      ]


instance Exception MoneybagConfigurationException where
  displayException = pretty

data ClientState = ClientState
  { csDefaultAliasCounter :: DefaultAliasCounter
  , csRefillableAddresses :: Set Address
  , csMoneybagAddress :: Moneybag
  }

newtype ClientM a = ClientM
  { unClientM :: ReaderT (IORef ClientState) IO a
  }
  deriving newtype (Functor, Applicative, Monad, MonadIO,
                    MonadThrow, MonadCatch, MonadReader (IORef ClientState))

data InternalNetworkScenarioError = TooManyRefillIterations Word Address
  deriving stock (Show)

instance Buildable InternalNetworkScenarioError where
  build (TooManyRefillIterations iter addr) =
    "Too many (" +| iter |+ ") refill iteratons of " +| addr |+ ""

instance Exception InternalNetworkScenarioError where
  displayException = pretty

runNetworkScenario :: NetworkEnv -> ClevelandScenario ClientM -> IO ()
runNetworkScenario (NetworkEnv env envKey envAlias@(Client.Alias alias)) scenario = do
  disableAlphanetWarning
  storageAddress <- runMorleyClientM env $
    Client.resolveAddressMaybe (AddressAlias envAlias)
  moneyAddr <- case (envKey, storageAddress) of
    (Nothing, Just addr) -> pure addr
    (Nothing, Nothing) -> throwM $ NoMoneybagAddress envAlias
    (Just ek, Just sa)
      | mkKeyAddress (toPublic ek) == sa -> pure sa
      | otherwise -> throwM $ TwoMoneybagKeys envAlias ek sa
    (Just ek, Nothing) -> do
      runMorleyClientM env (Client.importKey False (Client.AliasHint alias) ek)
      return $ mkKeyAddress (toPublic ek)
  ist <- newIORef ClientState
    { csDefaultAliasCounter = DefaultAliasCounter 0
    , csRefillableAddresses = Set.empty
    , csMoneybagAddress = Moneybag moneyAddr
    }
  runReaderT (unClientM $ scenario (networkImpl env) moneyAddr) ist

-- | Implementation that works with real network and uses `tezos-node`
-- RPC and `tezos-client`.
networkImpl :: MorleyClientEnv -> ClevelandImpl ClientM
networkImpl env = mapClevelandImplExceptions (addCallStack . exceptionHandler)
  ClevelandImpl
  { ciOpsImpl = \(Sender sender) -> ClevelandOpsImpl
    { coiRunOperationBatch = runOperationBatch sender
    }

  , ciMiscImpl = ClevelandMiscImpl
    { cmiRunIO = liftIO

    , cmiOriginateLargeUntyped = \sender untypedOriginateData -> do
        (_, res) <- runClientOrigination
          sender Client.originateLargeUntypedContract untypedOriginateData
        cmiComment $
          "Originated large smart contract " +| uodName untypedOriginateData |+
          " with address " <> pretty res
        pure res

    , cmiSignBytes = \hash signer -> liftIO $ runMorleyClientM env $
        -- We don't use password protected accounts in cleveland tests
        Client.signBytes (AddressResolved signer) Nothing hash

    , cmiGenKey = \alias -> do
        aliasHint <- resolveSpecificOrDefaultAliasHint alias
        liftIO $ runMorleyClientM env . Client.genKey $ aliasHint

    , cmiGenFreshKey = \alias -> do
        aliasHint <- resolveSpecificOrDefaultAliasHint alias
        liftIO $ runMorleyClientM env . Client.genFreshKey $ aliasHint

    , cmiGetBalance = getBalanceHelper
    , cmiGetChainId = liftIO $ runMorleyClientM env Client.getChainId
    , cmiFailure = \msg ->
        throwWithCallStack callStack $ CustomTestError (pretty msg)
    , cmiAttempt = try
    , cmiMarkAddressRefillable = setAddressRefillable
    , ..
    }
  }
  where
    cmiGetStorage
      :: forall st. (NiceUnpackedValue st)
      => Address -> ClientM st
    cmiGetStorage addr = do
      expr <- cmiGetStorageExpr addr
      either throwM (pure . T.fromVal) (fromExpression expr)
        \\ niceUnpackedValueEvi @st

    cmiGetBigMapValueMaybe :: (NicePackedValue k, NiceUnpackedValue v) => BigMapId k v -> k -> ClientM (Maybe v)
    cmiGetBigMapValueMaybe bigMapId k =
      liftIO . runMorleyClientM env $ Client.readBigMapValueMaybe bigMapId k

    cmiGetAllBigMapValuesMaybe :: (NiceUnpackedValue v) => BigMapId k v -> ClientM (Maybe [v])
    cmiGetAllBigMapValuesMaybe bigMapId =
      liftIO . runMorleyClientM env $ Client.readAllBigMapValuesMaybe bigMapId

    cmiGetStorageExpr :: Address -> ClientM Expression
    cmiGetStorageExpr = liftIO . runMorleyClientM env . Client.getContractStorage

    cmiComment :: Text -> ClientM ()
    cmiComment msg = liftIO $ putTextLn msg >> hFlush stdout

    cmiResolveAddress :: Alias -> ClientM Address
    cmiResolveAddress = liftIO . runMorleyClientM env . Client.resolveAddress . AddressAlias

    getAlias :: Address -> ClientM Alias
    getAlias = liftIO . runMorleyClientM env . Client.getAlias . AddressResolved

    cmiGetPublicKey :: Address -> ClientM PublicKey
    cmiGetPublicKey = liftIO . runMorleyClientM env . Client.getPublicKey . AddressResolved

    getBalanceHelper :: Address -> ClientM Mutez
    getBalanceHelper = liftIO . runMorleyClientM env . Client.getBalance

    cmiGetNow :: ClientM Tezos.Timestamp
    cmiGetNow = timestampFromUTCTime <$> getLastBlockTimestamp

    cmiGetLevel :: ClientM Natural
    cmiGetLevel = getLastBlockLevel

    cmiGetApproximateBlockInterval :: ClientM (Time Second)
    cmiGetApproximateBlockInterval = liftIO $ do
      pp <- runMorleyClientM env $ Client.getProtocolParameters
      return . sec . fromIntegral . unStringEncode $ ppMinimalBlockDelay pp

    cmiAdvanceTime :: (KnownDivRat unit Second) => Time unit -> ClientM ()
    cmiAdvanceTime delta = do
      let
        -- Round 'delta' to the nearest second, not smaller than 'delta'.
        -- A chain's time resolution is never smaller than a second,
        -- so if 'delta' is 0.1s, we actually need to wait at least 1s.
        deltaSec :: Time Second
        deltaSec = ceilingUnit $ toUnit @Second delta

        deltaSec' :: NominalDiffTime
        deltaSec' = secondsToNominalDiffTime $ toNum @Second deltaSec
      t0 <- getLastBlockTimestamp
      threadDelay deltaSec
      let
        go :: ClientM ()
        go = do
          now <- getLastBlockTimestamp
          if (now `diffUTCTime` t0) >= deltaSec'
            then pass
            else threadDelay (sec 1) >> go
      go

    cmiAdvanceToLevel :: (Natural -> Natural) -> ClientM ()
    cmiAdvanceToLevel targetLevelFn = do
      lastLevel <- getLastBlockLevel
      let targetLevel = max (targetLevelFn lastLevel) lastLevel
      let skippedLevels = targetLevel - lastLevel
      -- In case we need to skip more than one level we'll jump ahead for
      -- 'cmiGetApproximateBlockInterval' for 'skippedLevels - 1' times.
      -- This way we are sure we won't end up in the middle (or towards the end)
      -- of the target level.
      when (skippedLevels > 0) $ do
        when (skippedLevels > 1) $ do
          minBlockInterval <- cmiGetApproximateBlockInterval
          let waitTime = (skippedLevels - 1) * toNum @Second minBlockInterval
          threadDelay . sec $ fromIntegral waitTime
        -- A chain's time resolution is never smaller than a second, so with (less
        -- than) a level to go we can wait for 1s in loop until we reach the target.
        let go :: ClientM ()
            go = do
              curLevel <- cmiGetLevel
              when (targetLevel > curLevel) $ threadDelay (sec 1) >> go
        go


    getLastBlockTimestamp :: ClientM UTCTime
    getLastBlockTimestamp = liftIO $
      bhnhTimestamp . bcHeader <$> runMorleyClientM env (Client.getBlockConstants HeadId)

    getLastBlockLevel :: ClientM Natural
    getLastBlockLevel = liftIO $
      fromIntegral . bhnhLevel . bcHeader <$> runMorleyClientM env (Client.getBlockConstants HeadId)

    runOperationBatch sender ops = do
      istRef <- ask
      ClientState{csMoneybagAddress=Moneybag moneybag} <- readIORef istRef
      -- Note that tezos key reveal operation cost an additional fee
      -- so that's why we reveal keys in origination and transaction
      -- rather than doing it before scenario execution
      liftIO $ revealKeyUnlessRevealed env sender

      ops' <- forM ops \case
        OriginateOp uod ->
          Right <$> convertOriginateUntypedData uod
        TransferOp td ->
          pure . Left $ convertTransferData td

      let refill :: Word -> Client.MorleyClientM Word
          refill iter = do
            void $ dryRunOperations (AddressResolved sender) ops'
            pure iter
            `catch` \errs -> do
              when (iter > 3) $ throwM $ TooManyRefillIterations iter sender
              realBalance <- Client.getBalance sender
              amount <- max minimalMutez . addSafetyMutez <$> case errs of
                Client.UnexpectedRunErrors (findBalanceTooLow -> Just (required, balance))
                  -> do
                    logInfo $ sender |+ " balance of " +| realBalance |+ " \n\
                              \is too low, need " +| required |+ ", but got " +| balance |+ ""
                    let reportedDifference = unsafeSubMutez required balance
                        -- required >= balance should always be true if we got 'BalanceTooLow'
                    if iter == 0
                    -- on first iteration, we dry-run the transaction as moneybag
                    -- and esitmate the required balance that way;
                    then approximateRequired realBalance
                           `catch` \(_ :: Client.UnexpectedErrors) -> pure reportedDifference
                    -- on subsequent iterations (which run only if the first
                    -- wasn't enough), we rely on the reported `required` and `balance`,
                    -- NOTE: BalanceTooLow can be thrown either before fees are subtracted, or after.
                    -- In the former case, (required - balance == transfer_amount - real_balance)
                    -- In the latter case, (required - balance == transfer_amount - real_balance - fees)
                    -- Notice that fees are only included if real_balance >= transfer_amount.
                    -- Consequently, if transfer_amount > real_balance AND
                    -- transfer_amount + fees > real_balance + minimalMutez, the amount we transfer here
                    -- will be insufficient. TL;DR, it doesn't work for large transfer_amounts.
                    -- For batched transfers, this gets a bit more complicated, but the same principle
                    -- applies; unless total fees can exceed minimalMutez, or total transfer_amount
                    -- is large, it should work without looping.
                    else pure reportedDifference
                Client.UnexpectedRunErrors (findCantPayStorageFee -> Just ())
                  -> do
                    logInfo $ sender |+ " balance of " +| realBalance |+ "\n\
                              \ is too low to pay storage fee"
                    -- since no required balance is reported, there is no choice
                    approximateRequired realBalance
                      -- if running as moneybag failed for some reason, just throw in some tez
                      -- and hope for the best
                      `catch` \(_ :: Client.UnexpectedErrors) -> pure minimalMutez
                err -> throwM err
              logInfo $ "Will transfer " +| amount |+ " from " +| moneybag |+ ""
              void $ Client.lTransfer moneybag sender amount U.DefEpName () Nothing
              refill (iter + 1) -- loop
          addSafetyMutez x = fromMaybe x $ addMutez x safetyMutez
          minimalMutez = unsafeMkMutez 5e5
          safetyMutez = unsafeMkMutez 100
          safetyStorage = 20
          approximateRequired balance = do
            -- dry-run as moneybag and estimate cost+burn+fees
            (appliedResults, fees) <- unzip <$> dryRunOperations (AddressResolved moneybag) ops'
            ProtocolParameters{..} <- Client.getProtocolParameters
            -- uses quite a bit of unsafe mutez arithmetic, but arguably
            -- if we end up running into overflow while computing the
            -- required balance, then we couldn't run these operations
            -- anyway.
            let totalFees = unsafeSumMutez fees
                unsafeSumMutez = foldr unsafeAddMutez zeroMutez
                zeroMutez = unsafeMkMutez 0
                originatonSz = fromIntegral ppOriginationSize :: Natural
                (opsSum, originationSize) = bimap unsafeSumMutez sum . unzip
                  $ map opcostAndOriginationCount ops
                costPerByte = unTezosMutez ppCostPerByte
                opcostAndOriginationCount = \case
                  OriginateOp uod -> (uodBalance uod, originatonSz)
                  TransferOp td -> (tdAmount td, 0)
                storageDiff AppliedResult{..} = safetyStorage + fromIntegral arPaidStorageDiff
                storageBurnInBytes = originationSize + sum (map storageDiff appliedResults)
                storageBurnInMutez = unsafeMulMutez costPerByte storageBurnInBytes
                required = opsSum `unsafeAddMutez` totalFees `unsafeAddMutez` storageBurnInMutez
            logInfo $ "estimated amount needed is " +| required |+ ", but got " +| balance |+ "\n\
                      \Storage size: " +| storageBurnInBytes |+ "; Operations cost: " +| opsSum |+ "\n\
                      \Fees: " +| totalFees |+ "; Storage burn cost: " +| storageBurnInMutez |+ ""
            pure $ fromMaybe zeroMutez $ subMutez required balance

      refillable <- isAddressRefillable sender
      results <- liftIO $ runMorleyClientM env $ do
        when refillable $ do
          tookIters <- refill 0
          when (tookIters > 1) $ logWarning $
            "Refill of " +| sender |+ " took " +| tookIters |+ " iterations."
        snd <$> Client.runOperations (AddressResolved sender) ops'

      let results' = results <&> \case
            Left () ->
              TransferResult
            Right addr ->
              OriginateResult addr

      forM_ results' $ \case
        OriginateResult addr -> do
          alias <- getAlias addr
          cmiComment $ "Originated smart contract '" +| alias |+
            "' with address " <> pretty addr
        _ -> pass

      return results'

    dryRunOperations :: AddressOrAlias
                     -> [Either Client.TransactionData Client.OriginationData]
                     -> Client.MorleyClientM [(AppliedResult, Mutez)]
    dryRunOperations s = \case
      [] -> return []
      (x:xs) -> toList . map (second unTezosMutez) <$> Client.dryRunOperationsNonEmpty s (x :| xs)

    findBalanceTooLow :: [Client.RunError] -> Maybe (Mutez, Mutez)
    -- we really shouldn't get several errors of the same type here, so find only the first one
    findBalanceTooLow
      (Client.BalanceTooLow (arg #balance -> balance) (arg #required -> required):_)
      = Just (required, balance)
    findBalanceTooLow (_:xs) = findBalanceTooLow xs
    findBalanceTooLow [] = Nothing

    findCantPayStorageFee :: [Client.RunError] -> Maybe ()
    -- we really shouldn't get several errors of the same type here, so find only the first one
    findCantPayStorageFee
      (Client.CantPayStorageFee:_)
      = Just ()
    findCantPayStorageFee (_:xs) = findCantPayStorageFee xs
    findCantPayStorageFee [] = Nothing

    exceptionToTransferFailure :: RPC.ClientRpcError -> ClientM TransferFailure
    exceptionToTransferFailure = \case
      RPC.ContractFailed addr expr -> return $ FailedWith addr expr
      RPC.BadParameter addr _ -> return $ BadParameter addr
      RPC.EmptyTransaction addr -> return $ EmptyTransaction addr
      RPC.ShiftOverflow addr -> return $ ShiftOverflow addr
      internalError -> throwM internalError

    exceptionHandler :: ClientM a -> ClientM a
    exceptionHandler action = try action >>= \case
      Left err -> exceptionToTransferFailure err >>= throwM
      Right res -> return res

    runClientOrigination
      :: Sender
      -> (  Bool
         -> AliasHint
         -> AddressOrAlias
         -> Mutez
         -> U.Contract
         -> U.Value
         -> Maybe Mutez
         -> Client.MorleyClientM (OperationHash, Address)
         )
      -> UntypedOriginateData
      -> ClientM (OperationHash, Address)
    runClientOrigination (Sender sender) mkScenario (UntypedOriginateData{..}) = do
      let originationScenario =
            mkScenario True uodName (AddressResolved sender)
              uodBalance uodContract uodStorage Nothing
      -- Note that tezos key reveal operation cost an additional fee
      -- so that's why we reveal keys in origination and transaction
      -- rather than doing it before scenario execution
      liftIO $ do
        revealKeyUnlessRevealed env sender
        runMorleyClientM env originationScenario

    resolveSpecificOrDefaultAliasHint (SpecificAliasHint aliasHint) =
      return aliasHint
    resolveSpecificOrDefaultAliasHint (DefaultAliasHint) = do
      stateRef <- ask
      ist@ClientState{csDefaultAliasCounter=DefaultAliasCounter counter} <- readIORef stateRef
      writeIORef stateRef ist{ csDefaultAliasCounter = DefaultAliasCounter $ counter + 1 }
      return $ mkDefaultAlias counter

    setAddressRefillable addr = do
      stRef <- ask
      modifyIORef stRef $ \st@ClientState{..} ->
        st{csRefillableAddresses=Set.insert addr csRefillableAddresses}

    isAddressRefillable addr = do
      stRef <- ask
      Set.member addr . csRefillableAddresses <$> readIORef stRef


----------------------------------------------------------------------------
-- Helpers
----------------------------------------------------------------------------

convertOriginateUntypedData
  :: (MonadThrow m)
  => UntypedOriginateData -> m Client.OriginationData
convertOriginateUntypedData UntypedOriginateData{..} = do
  SomeContractAndStorage contract storage <-
    throwLeft . pure $ typeCheckingWith def $
      typeCheckContractAndStorage uodContract uodStorage
  return Client.OriginationData
    { odReplaceExisting = True
    , odName = uodName
    , odBalance = uodBalance
    , odContract = contract
    , odStorage = storage
    , odMbFee = Nothing
    }

convertTransferData
  :: TransferData -> Client.TransactionData
convertTransferData TransferData{ tdParameter = param :: p, ..} =
  Client.TransactionData Client.TD
    { tdReceiver = toAddress tdTo
    , tdAmount = tdAmount
    , tdEpName = tdEntrypoint
    , tdParam = toVal param
    , tdMbFee = Nothing
    } \\ niceParameterEvi @p

-- | Runs 'Client.revealKeyUnlessRevealed' with given client environment.
revealKeyUnlessRevealed :: MorleyClientEnv -> Address -> IO ()
revealKeyUnlessRevealed env addr = runMorleyClientM env $
  -- We don't use password protected accounts in cleveland.
  Client.revealKeyUnlessRevealed addr Nothing

----------------------------------------------------------------------------
-- Validation
----------------------------------------------------------------------------

-- | Signals an assertion failure during the execution of a 'ClevelandScenario' action.
data TestError
  = CustomTestError Text
  deriving stock Show

instance Exception TestError where
  displayException = pretty

instance Buildable TestError where
  build = \case
    CustomTestError msg -> build msg
