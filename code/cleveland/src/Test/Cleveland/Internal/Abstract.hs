-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Abstract cleveland interface not bound to a particular
-- implementation.
--
-- The interface may look a bit untyped and unsafe in some places.
-- For example, in order to call a contract one should supply a
-- simple address rather than a contract ref, so it is easy to pass
-- a value of wrong type. Also it is easy to call a non-existing entrypoint.
--
-- Subjectively, it makes writing test scenarios easier because you
-- have to prove less to the compiler. It also makes implementation of
-- cleveland engine a bit easier. Of course, it also makes it easier
-- to make certain mistakes. However, we expect users of this interface
-- to also use the functionality of the "Test.Cleveland.Internal.Pure" module
-- and convert cleveland scenarios to purely testable scenarios for
-- integrational testing engine. In this case errors should be detected
-- almost as quickly as they would reported by the compiler, at least
-- before trying to run scenario on a live network.
--
-- Also this interface uses 'Address' rather than 'EpAddress'.
-- I (\@gromak) concluded that 'EpAddress' can not be passed to @tezos-client@.
-- For key addresses it just does not make sense and for contract addresses
-- I get such errors:
--
-- @
--   bad contract notation
--   Invalid contract notation "KT1VrFpBPwBTm3hsK7DB7SPmY8fTHJ3vY6sJ%mint"
-- @
module Test.Cleveland.Internal.Abstract
  ( ContractHandler (..)
  , OriginateData (..)
  , TransferData (..)
  , Sender (..)
  , Moneybag (..)
  , ClevelandScenario
  , EmulatedScenario
  , UntypedOriginateData (..)
  , BaseOperationData (..)
  , BaseOperationResult (..)

  , DefaultAliasCounter (..)
  , SpecificOrDefaultAliasHint (..)

  -- * Actions
  , ClevelandOpsImpl (..)
  , ClevelandOpsFromImpl
  , ClevelandMiscImpl (..)
  , ClevelandImpl (..)

  , ciNewAddress
  , coiTransfer
  , coiTransferMoney
  , coiOriginateUntyped
  , coiOriginateUntypedSimple
  , coiOriginate
  , coiOriginateSimple
  , coiOriginateTypedSimple
  , cmiOriginateLargeUntypedSimple
  , cmiOriginateLarge
  , cmiOriginateLargeSimple
  , coiCall
  , cmiImportUntypedContract
  , cmiImportContract
  , EmulatedImpl(..)
  , mapClevelandOpsImplExceptions
  , mapClevelandMiscImplExceptions
  , mapClevelandImplExceptions

  -- * Batching
  , ClevelandOpsBatch
  , runBatched
  , batchedOpsImpl

  -- * Validation
  , TransferFailure (..)
  , FailedInBranch (..)
  , GenericTestError (..)

  -- * Helpers
  , auto
  , ep
  , mkDefaultAlias
  , (?-)

  -- * Morley client re-exports
  , AliasHint
  ) where

import Data.Constraint ((\\))
import Data.Default (Default(..))
import Data.Typeable (cast)
import Fmt (Buildable(..), Builder, pretty, (+|), (|+))
import Time (KnownDivRat, Second, Time)

import Lorentz
  (HasEntrypointArg, TAddress(..), ToAddress(..), ToTAddress(toTAddress), useHasEntrypointArg)
import Lorentz.Constraints
import Lorentz.Run (Contract, toMichelsonContract)
import Morley.Client (Alias(..), AliasHint)
import Morley.Micheline (Expression)
import Morley.Michelson.Interpret (MorleyLogs)
import Morley.Michelson.Runtime (VotingPowers)
import Morley.Michelson.Runtime.Import (importUntypedContract)
import Morley.Michelson.Typed (BigMapId, Dict(Dict), IsoValue(..))
import qualified Morley.Michelson.Typed as T
import Morley.Michelson.Typed.Convert
import Morley.Michelson.Typed.Entrypoints
import qualified Morley.Michelson.Untyped as U
import Morley.Tezos.Address
import Morley.Tezos.Core (ChainId, Mutez, Timestamp, toMutez, zeroMutez)
import qualified Morley.Tezos.Crypto as Crypto
import Morley.Util.Batching
import Morley.Util.TypeLits
import Test.Cleveland.Internal.Exceptions (WithCallStack(..))
import Test.Cleveland.Lorentz (importContract)
import Test.Cleveland.Lorentz.Types
import Test.Cleveland.Michelson.Integrational (ScenarioBranchName, (?-))

data OriginateData param st =
  (NiceParameterFull param, NiceStorage st) => OriginateData
  { odName :: AliasHint
  -- ^ Alias for the originated contract.
  , odBalance :: Mutez
  -- ^ Initial balance.
  , odStorage :: st
  -- ^ Initial storage.
  , odContract :: Contract param st
  -- ^ The contract itself.
  --
  -- We are using Lorentz version here which is convenient. However, keep in
  -- mind that if someone wants to test a contract from @.tz@ file, they should use
  -- 'UntypedOriginateData'.
  }

-- | Untyped version of OriginateData. It can be used for interaction with raw
-- Michelson contracts
data UntypedOriginateData = UntypedOriginateData
  { uodName :: AliasHint
  -- ^ Alias for the originated contract.
  , uodBalance :: Mutez
  -- ^ Initial balance.
  , uodStorage :: U.Value
  -- ^ Initial storage.
  , uodContract :: U.Contract
  -- ^ The contract itself.
  }

-- | Information about transfer operation.
data TransferData =
  forall v addr. (NiceParameter v, ToAddress addr) => TransferData
  { tdTo :: addr
  -- ^ Receiver address for this transaction.
  , tdAmount :: Mutez
  -- ^ Amount to be transferred.
  , tdEntrypoint :: EpName
  -- ^ An entrypoint to be called. Consider using 'ep' in testing
  -- scenarios.
  , tdParameter :: v
  -- ^ Parameter that will be used for a contract call. Set to @()@
  -- for transfers to key addresses.
  }

-- | Datatype specifying creation of arbitrary operation.
data BaseOperationData
  = OriginateOp UntypedOriginateData
  | TransferOp TransferData

-- | Datatype specifying result of operation run.
data BaseOperationResult
  = OriginateResult Address
  | TransferResult

-- | A batch returned invalid output, e.g. origination address when transaction
-- was supplied.
data BatchResultMismatch
  = BatchResultMismatch Text  -- ^ Carries expected operation type in lowercase

instance Buildable BatchResultMismatch where
  build = \case
    BatchResultMismatch expected ->
      "For " +| expected |+ " operation received inappropriate result"

-- | Designates the special sender address.
--
-- Transfers and some other operations will occur on behalf of this address.
-- This is initialized to @moneybag@ address and then can be locally modified.
-- Operations to be affected with this address accept 'Sender' argument
-- explicitly in 'ClevelandImpl'.
newtype Sender = Sender { unSender :: Address }

-- | Designates the address that gifts money to new addresses.
--
-- Once a new address is allocated in a test scenario, we have to transfer some
-- money to it so that it is able to serve as transactions sender. 'Moneybag'
-- serves as a source of that money.
--
-- We do not use 'Sender' for this purpose because in most situations changing
-- moneybag is not necessary. If a user wraps a large piece of their script with
-- 'Test.Cleveland.withSender' call and that changes the moneybag - this behaviour may be
-- undesired and unexpected to the user.
newtype Moneybag = Moneybag { unMoneybag :: Address }

-- | An alias hint with default value that can be used to define unique alias
-- automatically.
data SpecificOrDefaultAliasHint
  = SpecificAliasHint AliasHint
  | DefaultAliasHint
  deriving stock (Show)

instance IsString SpecificOrDefaultAliasHint where
  fromString = SpecificAliasHint . fromString

instance Default SpecificOrDefaultAliasHint where
  def = DefaultAliasHint

mkDefaultAlias :: Natural -> AliasHint
mkDefaultAlias counter =
  fromString $ ("default_cleveland_alias" <> show counter)

-- | Helper to use automatically determined unique alias.
auto :: SpecificOrDefaultAliasHint
auto = def

-- | Counter which is used to provide different default aliases.
newtype DefaultAliasCounter = DefaultAliasCounter {unDefaultAliasCounter :: Natural}

-- | A record data type with operations creating primitives.
data ClevelandOpsImpl m = ClevelandOpsImpl
  { coiRunOperationBatch
      :: HasCallStack => [BaseOperationData] -> m [BaseOperationResult]
  -- ^ Perform a batch of operations.
  }

-- | Operations creating methods that additionally accept operation sender.
type ClevelandOpsFromImpl m = Sender -> ClevelandOpsImpl m

-- | A record data type with all base methods one can use during a cleveland test.
data ClevelandMiscImpl m = ClevelandMiscImpl
  { cmiRunIO :: forall res. HasCallStack => IO res -> m res
  -- ^ Runs 'IO' Action
  , cmiResolveAddress :: HasCallStack => Alias -> m Address
  -- ^ Unwrap an 'Address' or get an 'Address' corresponding to an alias.
  , cmiGenKey :: HasCallStack => SpecificOrDefaultAliasHint -> m Address
  -- ^ Generate a secret key and store it with given alias.
  -- If a key with this alias already exists, the corresponding address
  -- will be returned and no state will be changed.
  , cmiGenFreshKey :: HasCallStack => SpecificOrDefaultAliasHint -> m Address
  -- ^ Generate a secret key and store it with given alias.
  -- Unlike 'cmiGenKey' this function overwrites the existing key when
  -- given alias is already stored.
  , cmiSignBytes :: HasCallStack => ByteString -> Address -> m Crypto.Signature
  -- ^ Get signature for preapplied operation.
  , cmiOriginateLargeUntyped :: HasCallStack => Sender -> UntypedOriginateData -> m Address
  -- ^ Originate a new raw Michelson contract that doesn't fit into the
  -- origination size limit, by executing multiple operation steps.
  --
  -- Note that this is not part of 'ClevelandOpsImpl' because large origination is
  -- _not_ a primitive operation. Also, it cannot appear in a batch (it simply
  -- may not fit).
  , cmiComment :: HasCallStack => Text -> m ()
  -- ^ Print given string verbatim as a comment.
  , cmiGetBalance :: HasCallStack => Address -> m Mutez
  -- ^ Get balance for given address or alias.
  , cmiGetStorage
      :: forall st. (HasCallStack, NiceUnpackedValue st)
      => Address -> m st
  -- ^ Get the storage for given address or alias.
  -- Note: the 'NiceUnpackedValue' constraint is used here because some implementations
  -- need it to parse a Micheline 'Expression's to the resulting value.
  --
  -- The 'NiceUnpackedValue' constraint also forbids storages with @big_map@s from being
  -- retrieved (due to a limitation in the tezos RPC).
  , cmiGetStorageExpr :: HasCallStack => Address -> m Expression
  -- ^ The same as 'cmiGetStorage' but doesn't require knowing the type in advance.
  -- Returns a Micheline 'Expression' instead of a @Value@.
  , cmiGetBigMapValueMaybe
      :: forall k v. (HasCallStack, NiceComparable k, NicePackedValue k, NiceUnpackedValue v)
      => BigMapId k v -> k -> m (Maybe v)
  -- ^ Retrieve a big_map value, given a big_map ID and a key.
  -- Returns 'Nothing' when the big_map ID does not exist, or it exists but
  -- does not contain the given key.
  , cmiGetAllBigMapValuesMaybe
      :: forall k v. (HasCallStack, NiceComparable k, NicePackedValue k, NiceUnpackedValue v)
      => BigMapId k v -> m (Maybe [v])
  -- ^ Retrieve all big_map values, given a big_map ID.
  -- Returns 'Nothing' when the big_map ID does not exist.
  , cmiGetPublicKey :: HasCallStack => Address -> m Crypto.PublicKey
  -- ^ Get public key assosiated with given address or alias.
  -- Fail if given address or alias is not an implicit account.
  , cmiGetChainId :: HasCallStack => m ChainId
  -- ^ Get current @ChainId@.
  , cmiAdvanceTime :: forall unit. (HasCallStack, KnownDivRat unit Second) => Time unit -> m ()
  -- ^ Advance at least the given amount of time, or until a new block is baked,
  -- whichever happens last.
  --
  -- On a real network, this is implemented using @threadDelay@, so it's advisable
  -- to use small amounts of time only.
  , cmiAdvanceToLevel :: HasCallStack => (Natural -> Natural) -> m ()
  -- ^ Advance at least to the level returned by the callback, accepting current level.
  , cmiGetNow :: HasCallStack => m Timestamp
  -- ^ Get the timestamp observed by the last block to be baked.
  , cmiGetLevel :: HasCallStack => m Natural
  -- ^ Get the current level observed by the last block to be baked.
  , cmiFailure :: forall a. HasCallStack => Builder -> m a
  -- ^ Fails the test with the given error message.
  , cmiGetApproximateBlockInterval :: HasCallStack => m (Time Second)
  -- ^ Get approximate block interval in seconds. Note, that this value
  -- is minimal bound and real intervals can be larger.
  , cmiAttempt :: forall a e. (Exception e, HasCallStack) => m a -> m (Either e a)
  -- ^ Attempts to perform an action, returning either the result of the action or an exception.
  , cmiMarkAddressRefillable :: Address -> m ()
  -- ^ Marks a given address as "refillable", i.e. if the address lacks funds for the next operation,
  -- some funds will automatically be transferred to it.
  }

-- | A record data type with all base methods one can use during a cleveland test.
data ClevelandImpl m = ClevelandImpl
  { ciOpsImpl :: ClevelandOpsFromImpl m
  -- ^ The group of methods related to operations.
  , ciMiscImpl :: ClevelandMiscImpl m
  -- ^ All the remaining methods.
  }

-- | Any monadic computation that can only access cleveland methods.
--
-- Scenario is also provided with the @moneybag@ address which is guaranteed to
-- exist and have plenty of XTZ.
type ClevelandScenario m = (Monad m) => ClevelandImpl m -> Address -> m ()

-- | A record data type with all base methods one can use during cleveland, but which are available
-- only when running on an emulated environment (e.g. "Morley.Michelson.Runtime") and not on a real network.
data EmulatedImpl m = EmulatedImpl
  { eiBranchout :: [(Text, m ())] -> m ()
  -- ^ Execute multiple testing scenarios independently, basing
  -- them on scenario built till this point.
  --
  -- The following property holds for this function:
  --
  -- @ pre >> branchout [a, b, c] = branchout [pre >> a, pre >> b, pre >> c] @.
  --
  -- In case of property failure in one of the branches no following branch is
  -- executed.
  --
  -- Providing empty list of scenarios to this function causes error;
  -- we do not require 'NonEmpty' here though for convenience.
  , eiOffshoot :: Text -> m () -> m ()
  -- ^ Test given scenario with the state gathered till this moment;
  -- if this scenario passes, go on as if it never happened.
  , eiGetStorage
      :: forall st addr. (HasCallStack, ToStorageType st addr)
      => addr -> m st
  -- ^ Retrieve a contract's full storage, including the contents of its big_maps.
  -- This function can only be used in emulator-only tests.
  , eiGetMorleyLogs :: m [MorleyLogs]
  -- ^ Returns the logs of an entire scenario
  , eiGetAddressMorleyLogs :: forall addr. ToAddress addr => addr -> m [MorleyLogs]
  -- ^ Returns the logs produced by the given contract.
  -- If this contract called other contracts, _their_ logs won't be included here.
  , eiSetVotingPowers :: VotingPowers -> m ()
  -- ^ Change voting power distribution.
  }

-- | Any monadic computation that can only access cleveland methods
-- and will be run in an emulated environment.
type EmulatedScenario m =
  Monad m => EmulatedImpl m -> ClevelandImpl m -> Address -> m ()

--------------------------------------------------------------------------
-- Higher-level actions
--
-- These functions are built upon the functions provided by 'ClevelandImpl' and
-- offer a more convenient experience.
--
-- They take 'ClevelandImpl' as an explicit argument
-- and do not have extra constraints. It makes it easier to adapt them for
-- any reasonable interface.
--
-- Adapted to @caps@ versions are provided in 'Test.Cleveland.Internal.Caps'.
--------------------------------------------------------------------------

-- | Generate a new secret key and record it with given alias. Also
-- transfer small amount of XTZ to it from the \"moneybag\" address
-- (which is @moneybag@ alias  by default). Does not override an existing
-- address.
--
-- Note, that when we generate a new address we must be aware of
-- prefix appended to it, so that when you create address alias @foo@,
-- you cannot call it directly by `AddressAlias "foo"`, but with the
-- corresponding prefix `AddressAlias "prefix.foo"`.
ciNewAddress
  :: (HasCallStack, Monad m)
  => ClevelandOpsFromImpl m -> ClevelandMiscImpl m -> Moneybag -> SpecificOrDefaultAliasHint -> m Address
ciNewAddress opsImpl miscImpl moneybag name = do
  let sender = Sender (unMoneybag moneybag)
  addr <- cmiGenKey miscImpl name
  -- The address may exist from previous scenarios runs and have sufficient
  -- balance for the sake of testing; if so, we can save some time
  balance <- cmiGetBalance miscImpl addr
  when (balance < toMutez 0.5_e6) $  -- < 0.5 XTZ
    coiTransfer (opsImpl sender) TransferData
      { tdTo = addr
      , tdAmount = toMutez $ 0.9_e6 -- 0.9 XTZ
      , tdEntrypoint = DefEpName
      , tdParameter = ()
      }
  pure addr

-- | Helpers that runs a single operation using 'ClevelandOpsImpl'.
coiRunSingleOperation
  :: (HasCallStack, Functor m)
  => ClevelandOpsImpl m
  -> Text
  -> BaseOperationData
  -> (BaseOperationResult -> Maybe a)
  -> m a
coiRunSingleOperation impl desc opData parseRes =
  runOperationBatchM (Proxy @BatchResultMismatch) impl $
    opData `submitThenParse` maybeToRight (BatchResultMismatch desc) . parseRes

-- | A simplified version of the originateUntyped command.
-- The contract will have 0 balance.
coiOriginateUntypedSimple
  :: (HasCallStack, Functor m)
  => ClevelandOpsImpl m -> AliasHint -> U.Value -> U.Contract -> m Address
coiOriginateUntypedSimple impl uodName uodStorage uodContract =
  let uodBalance = zeroMutez
  in coiOriginateUntyped impl UntypedOriginateData{..}

-- | Originate a new raw Michelson contract with given data.
coiOriginateUntyped
  :: (HasCallStack, Functor m)
  => ClevelandOpsImpl m -> UntypedOriginateData -> m Address
coiOriginateUntyped impl uod =
  coiRunSingleOperation impl "origination" (OriginateOp uod)
    \case{ OriginateResult addr -> Just addr; _ -> Nothing }

-- | Lorentz version for origination.
coiOriginate
  :: (HasCallStack, Functor m)
  => ClevelandOpsImpl m -> OriginateData param st -> m (ContractHandler param st)
coiOriginate impl dat@OriginateData{..} =
  ContractHandler (pretty odName) <$> coiOriginateUntyped impl
    (originateDataToUntyped dat)

-- | Like 'coiOriginateUntypedSimple', but accepts typed contract and initial storage
-- as a Haskell value.
coiOriginateTypedSimple
  :: (HasCallStack, Functor m, NiceParameterFull  cp, NiceStorage st)
  => ClevelandOpsImpl m -> AliasHint -> st -> T.Contract (ToT cp) (ToT st) -> m (ContractHandler cp st)
coiOriginateTypedSimple impl name storage contract@T.Contract{} =
  ContractHandler (pretty name) <$> coiOriginateUntypedSimple impl name
    (untypeHelper storage) (convertContract contract)

-- | Base method for making a transfer.
coiTransfer
  :: (HasCallStack, Functor m)
  => ClevelandOpsImpl m -> TransferData -> m ()
coiTransfer impl td =
  coiRunSingleOperation impl "transfer" (TransferOp td)
    \case{ TransferResult -> Just (); _ -> Nothing }

-- | Simply transfer money to an address.
--
-- This assumes that target address is either an implicit address or has
-- a default entrypoint with a unit argument; otherwise the call fails.
coiTransferMoney
  :: (HasCallStack, Functor m, ToAddress addr)
  => ClevelandOpsImpl m -> addr -> Mutez -> m ()
coiTransferMoney impl (toAddress -> to) amount =
  coiTransfer impl TransferData
    { tdTo = to
    , tdAmount = amount
    , tdEntrypoint = DefEpName
    , tdParameter = ()
    }

-- | A simplified version of the originate command.
-- The contract will have 0 balance.
coiOriginateSimple ::
     (HasCallStack, NiceParameterFull param, NiceStorage st, Functor m)
  => ClevelandOpsImpl m
  -> AliasHint
  -> st
  -> Contract param st
  -> m (ContractHandler param st)
coiOriginateSimple impl odName odStorage odContract =
  let odBalance = zeroMutez
  in coiOriginate impl OriginateData{..}

-- | Call a certain entrypoint of a contract referred to by the second
-- typed address from the first address.
coiCall ::
     forall v addr m epRef epArg.
     (HasCallStack, ToTAddress v addr, HasEntrypointArg v epRef epArg, IsoValue epArg, Typeable epArg, Functor m)
  => ClevelandOpsImpl m
  -> addr
  -> epRef
  -> epArg
  -> m ()
coiCall impl (toTAddress @v @addr -> TAddress to) epRef param =
  case useHasEntrypointArg @v @epRef @epArg epRef of
    (Dict, epName) ->
      coiTransfer impl $
      TransferData
        { tdTo = to
        , tdAmount = zeroMutez
        , tdEntrypoint = epName
        , tdParameter = param
        }

-- | A simplified version of the originateLarge command.
-- The contract will have 0 balance.
cmiOriginateLargeSimple ::
     (HasCallStack, NiceParameterFull param, NiceStorage st, Functor m)
  => ClevelandMiscImpl m
  -> Sender
  -> AliasHint
  -> st
  -> Contract param st
  -> m (ContractHandler param st)
cmiOriginateLargeSimple impl sender odName odStorage odContract =
  let odBalance = zeroMutez
  in cmiOriginateLarge impl sender OriginateData{..}

-- | A simplified version of the originateLargeUntyped command.
-- The contract will have 0 balance.
cmiOriginateLargeUntypedSimple
  :: HasCallStack
  => ClevelandMiscImpl m -> Sender -> AliasHint -> U.Value -> U.Contract -> m Address
cmiOriginateLargeUntypedSimple impl sender uodName uodStorage uodContract =
  let uodBalance = zeroMutez
  in cmiOriginateLargeUntyped impl sender UntypedOriginateData{..}

-- | Lorentz version for large origination.
cmiOriginateLarge
  :: (HasCallStack, Functor m)
  => ClevelandMiscImpl m -> Sender -> OriginateData param st -> m (ContractHandler param st)
cmiOriginateLarge impl sender dat@OriginateData{..} =
  ContractHandler (pretty odName) <$> cmiOriginateLargeUntyped impl sender
    (originateDataToUntyped dat)

cmiImportUntypedContract :: HasCallStack => ClevelandMiscImpl m -> FilePath -> m U.Contract
cmiImportUntypedContract impl path = cmiRunIO impl (importUntypedContract path)

cmiImportContract
  :: (HasCallStack, NiceParameterFull param, NiceStorage st)
  => ClevelandMiscImpl m -> FilePath -> m (Contract param st)
cmiImportContract impl path = cmiRunIO impl (importContract path)

----------------------------------------------------------------------------
-- Batched operations
----------------------------------------------------------------------------

-- | Where the batched operations occur.
--
-- Note that this is not a 'Monad', rather an 'Applicative' - use
-- @-XApplicativeDo@ extension for nicer experience.
newtype ClevelandOpsBatch a = ClevelandOpsBatch
  { unClevelandOpsBatch
      :: BatchingM BaseOperationData BaseOperationResult Void a
  } deriving newtype (Functor, Applicative)

instance
  TypeError
  ( 'Text "Attempt to use monad capabilities within a batch" ':$$:
    'Text "In case you are using a do-block, make sure that" ':$$:
    'Text "• `ApplicativeDo` extension is enabled" ':$$:
    'Text "• there is a return statement in the end" ':$$:
    'Text "• returned value picks variables in the order in which they are defined"
  ) =>
  Monad ClevelandOpsBatch where
    (>>=) = error "impossible"

{- | Run a series of operations within a batch.

Example:

@
contract <- runBatched impl $ do
  -- this block is executed within 'ClevelandOpsBatch'
  contract <- coiOriginate batchedOpsImpl ...
  for_ [1..3] \i ->
    coiTransfer batchedOpsImpl ...
  return contract
@

See 'ClevelandOpsBatch' for some precautions.
-}
runBatched
  :: (HasCallStack, Functor m)
  => ClevelandOpsImpl m
  -> ClevelandOpsBatch a
  -> m a
runBatched impl =
  runOperationBatchM (Proxy @Void) impl . unClevelandOpsBatch

-- | 'ClevelandOpsImpl' suitable for methods executed within a batch.
batchedOpsImpl :: ClevelandOpsImpl ClevelandOpsBatch
batchedOpsImpl = ClevelandOpsImpl
  { coiRunOperationBatch = ClevelandOpsBatch . traverse (`submitThenParse` pure)
  }

-- | Version of 'coiRunOperationBatch' that uses 'BatchingM'.
--
-- This is an internal function.
--
-- Invariant: all errors described by @e@ must be internal and should not occur
-- in practice (we require @e@ type to be specified explicitly to hinder
-- incorrect usage).
runOperationBatchM
  :: (HasCallStack, Buildable e, Functor m)
  => Proxy e
  -> ClevelandOpsImpl m
  -> BatchingM BaseOperationData BaseOperationResult e a
  -> m a
runOperationBatchM _ impl =
  fmap snd . unsafeRunBatching (fmap ((), ) . coiRunOperationBatch impl)

----------------------------------------------------------------------------
-- Validation
----------------------------------------------------------------------------

-- | Failures that could be expected in the execution of a 'ClevelandScenario' action.
-- These can be caught and handled with 'Test.Cleveland.attempt'.
data TransferFailure
  = FailedWith Address Expression
  -- ^ Expect that interpretation of contract with the given address ended
  -- with @FAILWITH@.
  | ShiftOverflow Address
  -- ^ Expect that interpretation of contract with the given address ended
  -- with a shift overflow error.
  | EmptyTransaction Address
  -- ^ Expect failure due to an attempt to transfer 0tz towards a simple address.
  | BadParameter Address
  -- ^ Expect failure due to an attempt to call a contract with an invalid parameter.
  deriving stock (Show, Eq)
-- TODO: [#284] add more errors here!
--  | ClevelandMutezArithError ArithErrorType
--  | ClevelandGasExhaustion

-- | When an exception is thrown in a 'Test.Cleveland.branchout' branch, we wrap it in this
-- constructor to remember in _which_ branch it was thrown.
-- We use this information to provide better error messages when a test fails.
data FailedInBranch = FailedInBranch ScenarioBranchName SomeException
  deriving stock (Show)

instance Buildable TransferFailure where
  build = \case
    FailedWith addr expr -> "Contract: " +| addr |+ " failed with: " +| expr |+ ""
    ShiftOverflow addr -> "Contract: " +| addr |+ " failed due to a shift overflow"
    EmptyTransaction addr -> "Attempted to transfer 0tz to a simple address: " +| build addr
    BadParameter addr ->
      "Attempted to call contract " +| addr |+ " with a parameter of the wrong type"

instance Buildable FailedInBranch where
  build (FailedInBranch branchName (SomeException err)) = "In '" +| branchName |+
    "' branch:\n" +| (build $ displayException err)

data GenericTestError
  = UnexpectedSuccess
  deriving stock Show

instance Buildable GenericTestError where
  build = \case
    UnexpectedSuccess ->
      "Expected an exception to be thrown, but it wasn't"

instance Exception TransferFailure where
  displayException = pretty
  fromException someEx@(SomeException ex) =
    cast @_ @TransferFailure ex
    <|>
    ( do
        WithCallStack _ exInner <- fromException @WithCallStack someEx
        fromException exInner
    )
    <|>
    ( do
        FailedInBranch _ exInner <- fromException @FailedInBranch someEx
        fromException exInner
    )


instance Exception FailedInBranch where
  displayException = pretty

instance Exception GenericTestError where
  displayException = pretty

----------------------------------------------------------------------------
-- Other helpers
----------------------------------------------------------------------------

-- | A short partial constructor for 'EpName'. It is supposed to be
-- applied to string constants, so programmer is responsible for
-- validity. And this code is for tests anyway, so each failure is a
-- programmer mistake.
--
-- It is intentionally here and not in some deeper module because the
-- name is really short and more suitable for writing scenarios.
ep :: HasCallStack => Text -> EpName
ep = U.unsafeBuildEpName

-- | Common comversion function from 'OriginateData' to 'UntypedOriginateData'
originateDataToUntyped :: OriginateData param st -> UntypedOriginateData
originateDataToUntyped OriginateData{..} = UntypedOriginateData
  { uodName = odName
  , uodBalance = odBalance
  , uodStorage = untypeHelper odStorage
  , uodContract = convertContract $ toMichelsonContract odContract
  }

untypeHelper :: forall st. NiceStorage st => st -> U.Value
untypeHelper = untypeValue . toVal \\ niceStorageEvi @st

mapClevelandOpsImplExceptions
  :: (forall a. HasCallStack => m a -> m a)
  -> ClevelandOpsImpl m -> ClevelandOpsImpl m
mapClevelandOpsImplExceptions f ClevelandOpsImpl{..} = ClevelandOpsImpl
    { coiRunOperationBatch = \op -> f $ coiRunOperationBatch op
    }

-- | Note that this does *not* map over 'cmiAttempt'.
mapClevelandMiscImplExceptions
  :: (forall a. HasCallStack => m a -> m a)
  -> ClevelandMiscImpl m -> ClevelandMiscImpl m
mapClevelandMiscImplExceptions f ClevelandMiscImpl{..} = ClevelandMiscImpl
    { cmiRunIO = \action -> f $ cmiRunIO action
    , cmiResolveAddress = \address -> f $ cmiResolveAddress address
    , cmiSignBytes = \bs alias -> f $ cmiSignBytes bs alias
    , cmiGenKey = \aliasHint -> f $ cmiGenKey aliasHint
    , cmiGenFreshKey = \aliasHint -> f $ cmiGenFreshKey aliasHint
    , cmiOriginateLargeUntyped = \sender uodata -> f $ cmiOriginateLargeUntyped sender uodata
    , cmiComment = \t -> f $ cmiComment t
    , cmiGetBalance = \addr -> f $ cmiGetBalance addr
    , cmiGetStorage = \addr -> f $ cmiGetStorage addr
    , cmiGetStorageExpr = \addr -> f $ cmiGetStorageExpr addr
    , cmiGetBigMapValueMaybe = \bmId k -> f $ cmiGetBigMapValueMaybe bmId k
    , cmiGetAllBigMapValuesMaybe = \bmId -> f $ cmiGetAllBigMapValuesMaybe bmId
    , cmiGetPublicKey = \addr -> f $ cmiGetPublicKey addr
    , cmiGetChainId = f $ cmiGetChainId
    , cmiAdvanceTime = \time -> f $ cmiAdvanceTime time
    , cmiAdvanceToLevel = \level -> f $ cmiAdvanceToLevel level
    , cmiGetNow = f $ cmiGetNow
    , cmiGetLevel = f $ cmiGetLevel
    , cmiFailure = \builder -> f $ cmiFailure builder
    , cmiGetApproximateBlockInterval = f $ cmiGetApproximateBlockInterval
    , cmiAttempt = \action -> cmiAttempt action
    , cmiMarkAddressRefillable = f . cmiMarkAddressRefillable
    }

-- | Runs a handler over every action (except 'cmiAttempt'),
-- possibly transforming exceptions thrown by those actions.
mapClevelandImplExceptions
  :: (forall a. HasCallStack => m a -> m a)
  -> ClevelandImpl m -> ClevelandImpl m
mapClevelandImplExceptions f ClevelandImpl{..} = ClevelandImpl
  { ciOpsImpl = mapClevelandOpsImplExceptions f . ciOpsImpl
  , ciMiscImpl = mapClevelandMiscImplExceptions f ciMiscImpl
  }
