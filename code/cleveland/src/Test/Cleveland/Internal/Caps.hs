-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE InstanceSigs #-}

-- | @caps@-based interface for cleveland.
module Test.Cleveland.Internal.Caps
  ( MonadCleveland
  , MonadEmulated
  , MonadOps (..)
  , ClevelandT
  , EmulatedT
  , withSender
  , withMoneybag
  , runIO
  , resolveAddress
  , newAddress
  , newFreshAddress
  , newRefillableAddress
  , signBytes
  , signBinary
  , originate
  , originateSimple
  , originateUntyped
  , originateUntypedSimple
  , originateTypedSimple
  , originateLarge
  , originateLargeSimple
  , originateLargeUntyped
  , originateLargeUntypedSimple
  , transfer
  , transferMoney
  , call
  , inBatch
  , importUntypedContract
  , importContract
  , comment
  , getBalance
  , getStorage
  , getFullStorage
  , getStorageExpr
  , getAllBigMapValues
  , getAllBigMapValuesMaybe
  , getBigMapSize
  , getBigMapSizeMaybe
  , getBigMapValueMaybe
  , getBigMapValue
  , getMorleyLogs
  , getAddressMorleyLogs
  , getPublicKey
  , getChainId
  , advanceTime
  , advanceLevel
  , advanceToLevel
  , getNow
  , getLevel
  , getApproximateBlockInterval
  , branchout
  , offshoot
  , setVotingPowers

  -- * Assertions
  , failure
  , assert
  , (@==)
  , (@/=)
  , (@@==)
  , (@@/=)
  , checkCompares
  , checkComparesWith

  -- * Exception handling
  , attempt
  , catchTransferFailure
  , checkTransferFailure
  , expectTransferFailure
  , expectFailedWith
  , expectError
  , expectCustomError
  , expectCustomError_
  , expectCustomErrorNoArg
  , expectNumericError
  -- ** TransferFailure predicates
  , TransferFailurePredicate(..)
  , shiftOverflow
  , emptyTransaction
  , badParameter
  , failedWith
  , addressIs
  -- ** @FAILWITH@ errors
  , constant
  , lerror
  , customError
  , customError_
  , customErrorNoArg
  , numericError

  -- * Capabilities
  , SenderCap
  , MoneybagCap
  , ClevelandOpsCap
  , uncapsCleveland
  , uncapsEmulated
  , clevelandOpsCapImpl
  , clevelandMiscCapImpl
  , emulatedCapImpl
  , senderCapImpl
  , moneybagCapImpl

  -- * Internal helpers
  , ActionToCaps(..)
  , implActionToCaps
  , miscImplAndSenderActionToCaps
  , ActionToMonadOps (..)
  ) where

import Data.Either.Validation (Validation(..))
import qualified Data.List.NonEmpty as NE
import Fmt (Buildable, Builder, build, indentF, pretty, unlinesF, (+|), (|+))
import qualified Monad.Capabilities as Caps
import Time (KnownDivRat, Second, Time)

import Lorentz
  (BigMapId, CustomError(..), ErrorTagMap, HasEntrypointArg, IsError, IsoValue, Label, MText,
  MustHaveErrorArg, ToAddress, ToTAddress, errorTagToMText, errorToVal, errorToValNumeric,
  toAddress, toVal)
import Lorentz.Bytes
import Lorentz.Constraints
import Lorentz.Run (Contract)
import Morley.Client (Alias, AsRPC)
import Morley.Micheline (Expression, FromExpression(..))
import Morley.Michelson.Interpret (MorleyLogs)
import Morley.Michelson.Printer.Util (buildRenderDoc)
import Morley.Michelson.Runtime (VotingPowers)
import Morley.Michelson.Typed (SomeConstant(..))
import qualified Morley.Michelson.Typed as T
import Morley.Michelson.Typed.Convert ()
import qualified Morley.Michelson.Untyped as U
import Morley.Tezos.Address (Address)
import Morley.Tezos.Core (ChainId, Mutez, Timestamp)
import Morley.Tezos.Crypto (PublicKey, Signature)
import Test.Cleveland.Internal.Abstract
import Test.Cleveland.Lorentz.Types

-- | A capability that provides sender address.
newtype SenderCap m = SenderCap Sender

-- | A capability that provides moneybag address.
newtype MoneybagCap m = MoneybagCap Moneybag

-- | A capability that provides operations-related primitve actions.
newtype ClevelandOpsCap m = ClevelandOpsCap (ClevelandOpsFromImpl m)

-- | Constraint for a monad in which we can do cleveland actions.
type MonadCleveland caps base m =
  ( Monad base, m ~ Caps.CapsT caps base
  , Caps.HasCaps '[ClevelandOpsCap, ClevelandMiscImpl, SenderCap, MoneybagCap] caps
  )

-- | Constraint for a monad in which we can do cleveland actions that can't be run on a real network.
-- It requires the 'EmulatedImpl' capability.
type MonadEmulated caps base m =
  (MonadCleveland caps base m, Caps.HasCap EmulatedImpl caps)

-- | Monad transformer that adds only 'ClevelandImpl' capability.
type ClevelandT m =
  Caps.CapsT '[ ClevelandOpsCap, ClevelandMiscImpl, SenderCap, MoneybagCap ] m

-- | Monad transformer that adds both 'ClevelandImpl' and 'EmulatedImpl' capabilities.
type EmulatedT m =
  Caps.CapsT '[ ClevelandOpsCap, ClevelandMiscImpl, EmulatedImpl, SenderCap, MoneybagCap ] m

-- | Typeclass for monads where operations-related actions can occur.
--
-- This is implemented for 'MonadCleveland' and batch context.
--
-- Has 'Functor' as a superclass constraint for convenience, all the related methods
-- require it.
class Functor m => MonadOps m where
  -- | Obtain 'ClevelandOpsImpl' suitable for the current \"monad\".
  --
  -- In CPS style, because the \"monad\" can be actually not a monad, so
  -- it can't work like 'ask' for 'ReaderT'.
  withOpsImpl :: (ClevelandOpsImpl m -> m a) -> m a

instance MonadOps ClevelandOpsBatch where
  withOpsImpl action = action batchedOpsImpl

-- | Update the current sender on whose behalf transfer and other operations are
-- invoked.
withSender :: MonadCleveland caps base m => Address -> m a -> m a
withSender addr = local $ Caps.overrideCap (senderCapImpl addr)

-- | Update the current moneybag that transfers money on the newly created
-- addresses. For the rare occasions when this is necessary.
withMoneybag :: MonadCleveland caps base m => Address -> m a -> m a
withMoneybag addr = local $ Caps.overrideCap (moneybagCapImpl addr)

-- | 'cmiRunIO' adapted to @caps@.
runIO :: (HasCallStack, MonadCleveland caps base m) => IO res -> m res
runIO = implActionToCaps cmiRunIO

-- | 'cmiResolveAddress' adapted to @caps@.
resolveAddress :: (HasCallStack, MonadCleveland caps base m) => Alias -> m Address
resolveAddress = implActionToCaps cmiResolveAddress

-- | 'ciNewAddress' adapted to @caps@.
newAddress :: (HasCallStack, MonadCleveland caps base m) => SpecificOrDefaultAliasHint -> m Address
newAddress = actionToCaps \caps ->
  let ClevelandOpsCap opsFromImpl = Caps.getCap caps
      miscImpl = Caps.getCap caps
      MoneybagCap moneybag = Caps.getCap caps
  in ciNewAddress opsFromImpl miscImpl moneybag

-- | Like 'newAddress', but marks address as "refillable"
-- with 'cmiMarkAddressRefillable'
--
-- If a refillable address lacks funds for the next operation,
-- some funds will automatically be transferred to it.
newRefillableAddress :: (HasCallStack, MonadCleveland caps base m) => SpecificOrDefaultAliasHint -> m Address
newRefillableAddress hint = do
  addr <- newAddress hint
  implActionToCaps cmiMarkAddressRefillable addr
  return addr

-- | Generate a new secret key and record it with given alias. If the
-- alias is already known, the key will be overwritten. The address is
-- guaranteed to be fresh, i. e. no operations on it have been made.
newFreshAddress :: (HasCallStack, MonadCleveland caps base m) => SpecificOrDefaultAliasHint -> m Address
newFreshAddress = implActionToCaps cmiGenFreshKey

-- | 'cmiSignBytes' adapted to @caps@.
signBytes :: (HasCallStack, MonadCleveland caps base m) => ByteString -> Address -> m Signature
signBytes = implActionToCaps cmiSignBytes

-- | Type-safer version of 'signBytes'.
signBinary :: (HasCallStack, BytesLike bs, MonadCleveland caps base m) => bs -> Address -> m (TSignature bs)
signBinary bs addr = TSignature <$> signBytes (toBytes bs) addr

-- | 'coiOriginateUntyped' adapted to @caps@
originateUntyped :: (HasCallStack, MonadOps m) => UntypedOriginateData -> m Address
originateUntyped = actionToMonadOps coiOriginateUntyped

-- | 'coiOriginateUntypedSimple' adapted to @caps@
originateUntypedSimple
  :: (HasCallStack, MonadOps m) => AliasHint -> U.Value -> U.Contract -> m Address
originateUntypedSimple = actionToMonadOps coiOriginateUntypedSimple

-- | 'coiOriginate' adapted to @caps@.
originate
  :: forall cp st m.
     (HasCallStack, MonadOps m)
  => OriginateData cp st -> m (ContractHandler cp st)
originate = actionToMonadOps coiOriginate

-- | 'coiOriginateSimple' adapted to @caps@.
originateSimple
  :: forall cp st m.
     ( HasCallStack
     , MonadOps m
     , NiceParameterFull cp, NiceStorage st
     )
  => AliasHint
  -> st
  -> Contract cp st
  -> m (ContractHandler cp st)
originateSimple = actionToMonadOps coiOriginateSimple

-- | 'coiOriginateTypedSimple' adapter to @caps@.
originateTypedSimple
  :: forall cp st m.
     ( HasCallStack
     , MonadOps m
     , NiceParameterFull cp, NiceStorage st
     )
  => AliasHint -> st -> T.Contract (T.ToT cp) (T.ToT st) -> m (ContractHandler cp st)
originateTypedSimple = actionToMonadOps coiOriginateTypedSimple

-- | 'cmiOriginateLargeUntyped' adapted to @caps@
originateLargeUntyped
  :: (HasCallStack, MonadCleveland caps base m) => UntypedOriginateData -> m Address
originateLargeUntyped = miscImplAndSenderActionToCaps cmiOriginateLargeUntyped

-- | 'cmiOriginateLargeUntypedSimple' adapted to @caps@
originateLargeUntypedSimple
  :: (HasCallStack, MonadCleveland caps base m)
  => AliasHint -> U.Value -> U.Contract -> m Address
originateLargeUntypedSimple = miscImplAndSenderActionToCaps cmiOriginateLargeUntypedSimple

-- | 'cmiOriginateLarge' adapted to @caps@.
originateLarge
  :: forall param st m caps base.
     (HasCallStack, MonadCleveland caps base m)
  => OriginateData param st -> m (ContractHandler param st)
originateLarge = miscImplAndSenderActionToCaps cmiOriginateLarge

-- | 'cmiOriginateLargeSimple' adapted to @caps@.
originateLargeSimple
  :: forall param st m caps base.
     ( HasCallStack
     , MonadCleveland caps base m
     , NiceParameterFull param, NiceStorage st
     )
  => AliasHint
  -> st
  -> Contract param st
  -> m (ContractHandler param st)
originateLargeSimple = miscImplAndSenderActionToCaps cmiOriginateLargeSimple

-- | 'coiTransfer' adapted to @caps@.
--
-- Avoid using this method in favour of 'transferMoney' and 'call', unless
-- you need the semantics of both in one operation.
transfer :: (HasCallStack, MonadOps m) => TransferData -> m ()
transfer = actionToMonadOps coiTransfer

-- | 'coiTransferMoney' adapted to @caps@.
transferMoney :: (HasCallStack, MonadOps m, ToAddress addr) => addr -> Mutez -> m ()
transferMoney = actionToMonadOps coiTransferMoney

-- | Call a certain entrypoint of a contract referred to by some
-- typed address.
-- The sender is the address identified by the @moneybag@ alias, unless another alias is specified
-- via '--cleveland-moneybag-alias' or using 'withSender'.
-- This address should have sufficient XTZ to pay the tx fee.
call
  :: forall v addr m epRef epArg.
     (HasCallStack, MonadOps m, ToTAddress v addr, HasEntrypointArg v epRef epArg, IsoValue epArg, Typeable epArg)
  => addr
  -> epRef
  -> epArg
  -> m ()
call = actionToMonadOps (coiCall @v)

-- | Import an untyped contract from file.
importUntypedContract :: (HasCallStack, MonadCleveland caps base m) => FilePath -> m U.Contract
importUntypedContract = implActionToCaps cmiImportUntypedContract

-- | Import a contract from file.
importContract
  :: ( HasCallStack, NiceParameterFull param, NiceStorage st
     , MonadCleveland caps base m
     )
  => FilePath -> m (Contract param st)
importContract = implActionToCaps cmiImportContract

{- | Run operations in a batch.

Example:

@
contract <- inBatch $ do
  contract <- originate ...
  for_ [1..3] \i ->
    transfer ...
  return contract
@

Batched operations should be applied to chain faster, but note that batches have
their own limits. For instance, at the moment of writing, the gas limit on a
batch is 10x of gas limit applied to a single operation.

A context of a batch is only 'Applicative', not 'Monad'. This means that:

* Return values of one function cannot be passed to another function in the same
  batch, it can only be outputed outside of the batch;
* Sometimes compiler does not recognize that only 'Applicative' context is
  required, in case of any issues with that - follow the error messages.

-}
inBatch :: (HasCallStack, MonadCleveland caps base m) => ClevelandOpsBatch a -> m a
inBatch batch = do
  caps <- ask
  let SenderCap sender = Caps.getCap caps
      ClevelandOpsCap impl = Caps.getCap caps
  runBatched (impl sender) batch

-- | 'cmiComment' adapted to @caps@.
comment :: (HasCallStack, MonadCleveland caps base m) => Text -> m ()
comment = implActionToCaps cmiComment

-- | 'cmiGetBalance' adapted to @caps@.
getBalance :: (HasCallStack, MonadCleveland caps base m, ToAddress addr) => addr -> m Mutez
getBalance = implActionToCaps cmiGetBalance . toAddress

-- | Retrieve a contract's storage in its "RPC representation"
-- (i.e., all its big_maps will be replaced by big_map IDs).
--
-- If the storage is of a user-defined type, then 'Test.Cleveland.deriveRPC' / 'Test.Cleveland.deriveManyRPC'
-- should be used to create an RPC representation of the storage type.
--
-- > data MyStorage = MyStorage { field1 :: Natural, field2 :: BigMap Integer MText }
-- > deriveRPC "MyStorage"
getStorage
  :: forall st addr caps base m.
    (HasCallStack, MonadCleveland caps base m, ToStorageType st addr, NiceUnpackedValue (AsRPC st))
  => addr
  -> m (AsRPC st)
getStorage addr = implActionToCaps cmiGetStorage (toAddress addr)

-- | Retrieve a contract's full storage, including the contents of its big_maps.
--
-- This function can only be used in emulator-only tests.
getFullStorage
  :: forall st addr caps base m.
    (HasCallStack, MonadEmulated caps base m, ToStorageType st addr)
  => addr
  -> m st
getFullStorage = implActionToCaps eiGetStorage

-- | 'cmiGetStorageExpr' adapted to @caps@
getStorageExpr
  :: forall addr caps base m.
    (HasCallStack, MonadCleveland caps base m, ToAddress addr)
  => addr
  -> m Expression
getStorageExpr = implActionToCaps cmiGetStorageExpr . toAddress

-- | Retrieve a big_map value, given a big_map ID and a key.
-- Returns 'Nothing' when the big_map ID does not exist, or it exists but
-- does not contain the given key.
getBigMapValueMaybe
  :: forall k v caps base m.
   ( HasCallStack, MonadCleveland caps base m
   , NiceComparable k, NicePackedValue k, NiceUnpackedValue v
   )
  => BigMapId k v -> k -> m (Maybe v)
getBigMapValueMaybe x y =
  withFrozenCallStack $ implActionToCaps cmiGetBigMapValueMaybe x y

-- | Like 'getBigMapValueMaybe', but fails the tests instead of returning 'Nothing'.
getBigMapValue
  :: forall k v caps base m.
   ( HasCallStack, MonadCleveland caps base m
   , NiceComparable k, NicePackedValue k, NiceUnpackedValue v
   , Buildable k
   )
  => BigMapId k v -> k -> m v
getBigMapValue bmId k =
  withFrozenCallStack $ getBigMapValueMaybe bmId k >>= \case
    Just v -> pure v
    Nothing -> failure $ unlinesF @_ @Builder
      [ "Either:"
      , "  1. A big_map with ID '" +| bmId |+ "' does not exist, or"
      , "  2. It exists, but does not contain the key '" +| k |+ "'."
      ]

-- | Retrieve all big_map values, given a big_map ID.
-- Returns 'Nothing' when the big_map ID does not exist.
getAllBigMapValuesMaybe
  :: forall k v caps base m.
  ( HasCallStack, MonadCleveland caps base m
  , NiceComparable k, NicePackedValue k, NiceUnpackedValue v
  )
  => BigMapId k v -> m (Maybe [v])
getAllBigMapValuesMaybe bmId =
  withFrozenCallStack $ implActionToCaps cmiGetAllBigMapValuesMaybe bmId

-- | Like 'getAllBigMapValuesMaybe', but fails the tests instead of returning 'Nothing'.
getAllBigMapValues
  :: forall k v caps base m.
  ( HasCallStack, MonadCleveland caps base m
  , NiceComparable k, NicePackedValue k, NiceUnpackedValue v
  )
  => BigMapId k v -> m [v]
getAllBigMapValues bmId =
  withFrozenCallStack $ getAllBigMapValuesMaybe bmId >>= \case
    Just vs -> pure vs
    Nothing -> failure $ "A big map with ID '" +| bmId |+ "' does not exist"

-- | Retrieve a big_map size, given a big_map ID.
-- Returns 'Nothing' when the big_map ID does not exist.
--
-- /O(n)/, because it's implemented with 'Morley.Client.RPC.Getters.getBigMapValues'.
getBigMapSizeMaybe
  :: forall k v caps base m.
  ( HasCallStack, MonadCleveland caps base m
  , NiceComparable k, NicePackedValue k, NiceUnpackedValue v
  )
  => BigMapId k v -> m (Maybe Natural)
getBigMapSizeMaybe bmId = withFrozenCallStack $
    fmap (fmap (fromIntegral @Int @Natural . length)) (getAllBigMapValuesMaybe bmId)

-- | Like 'getBigMapSizeMaybe', but fails the tests instead of returning 'Nothing'.
getBigMapSize
  :: forall k v caps base m.
  ( HasCallStack, MonadCleveland caps base m
  , NiceComparable k, NicePackedValue k, NiceUnpackedValue v
  )
  => BigMapId k v -> m Natural
getBigMapSize bmId = withFrozenCallStack $
  fromIntegral @Int @Natural . length <$> getAllBigMapValues bmId

-- | 'cmiGetPublicKey' adapted to @caps@.
getPublicKey :: (HasCallStack, MonadCleveland caps base m) => Address -> m PublicKey
getPublicKey = implActionToCaps cmiGetPublicKey

-- | 'cmiGetChainId' adapted to @caps@.
getChainId :: (HasCallStack, MonadCleveland caps base m) => m ChainId
getChainId = implActionToCaps cmiGetChainId

-- | Advance at least the given amount of time, or until a new block is baked,
-- whichever happens last.
--
-- On a real network, this is implemented using @threadDelay@, so it's advisable
-- to use small amounts of time only.
advanceTime
  :: forall unit caps base m
  . (HasCallStack, MonadCleveland caps base m, KnownDivRat unit Second)
  => Time unit -> m ()
advanceTime = implActionToCaps cmiAdvanceTime

-- | Wait till the provided number of levels is past.
advanceLevel
  :: forall caps base m
  . (HasCallStack, MonadCleveland caps base m)
  => Natural -> m ()
advanceLevel l = implActionToCaps cmiAdvanceToLevel (+ l)

-- | Wait till the provided level is reached.
advanceToLevel
  :: forall caps base m
  . (HasCallStack, MonadCleveland caps base m)
  => Natural -> m ()
advanceToLevel target = implActionToCaps cmiAdvanceToLevel (const target)

-- | Get the timestamp observed by the last block to be baked.
getNow :: (HasCallStack, MonadCleveland caps base m) => m Timestamp
getNow = implActionToCaps cmiGetNow

getLevel :: (HasCallStack, MonadCleveland caps base m) => m Natural
getLevel = implActionToCaps cmiGetLevel

-- | Get approximate block interval in seconds. Note, that this value
-- is minimal bound and real intervals can be larger, see
-- http://tezos.gitlab.io/active/consensus.html#minimal-block-delay-function
-- for more information about block delays.
getApproximateBlockInterval :: (HasCallStack, MonadCleveland caps base m) => m (Time Second)
getApproximateBlockInterval = implActionToCaps cmiGetApproximateBlockInterval

-- | Execute multiple testing scenarios independently, basing
-- them on scenario built till this point.
--
-- The following property holds for this function:
--
-- @ pre >> branchout [a, b, c] = branchout [pre >> a, pre >> b, pre >> c] @.
--
-- In case of property failure in one of the branches no following branch is
-- executed.
--
-- Providing empty list of scenarios to this function causes error;
-- we do not require 'NonEmpty' here though for convenience.
branchout :: (MonadEmulated caps base m) => [(Text, m ())] -> m ()
branchout = implActionToCaps eiBranchout

-- | Test given scenario with the state gathered till this moment;
-- if this scenario passes, go on as if it never happened.
offshoot :: MonadEmulated caps base m => Text -> m () -> m ()
offshoot = implActionToCaps eiOffshoot

-- | Returns a list of logs of all operations performed so far. Logs are messages of morley instruction
-- @PRINT@ - https://gitlab.com/morley-framework/morley/-/blob/master/code/morley/docs/language/morleyInstructions.md#print
getMorleyLogs :: MonadEmulated caps base m => m [MorleyLogs]
getMorleyLogs = implActionToCaps eiGetMorleyLogs

-- | Returns the logs produced by the given contract.
-- If this contract called other contracts, _their_ logs won't be included here.
getAddressMorleyLogs :: (MonadEmulated caps base m, ToAddress addr) => addr -> m [MorleyLogs]
getAddressMorleyLogs = implActionToCaps eiGetAddressMorleyLogs

-- | Updates voting power accessible via @VOTING_POWER@ and similar
-- instructions.
setVotingPowers :: MonadEmulated caps base m => VotingPowers -> m ()
setVotingPowers = implActionToCaps eiSetVotingPowers

----------------------------------------------------------------------------
-- Assertions
----------------------------------------------------------------------------

-- | Fails the test with the given error message.
failure :: forall a caps base m. (HasCallStack, MonadCleveland caps base m) => Builder -> m a
failure = implActionToCaps cmiFailure

-- | Fails the test with the given error message if the given condition is false.
assert :: (HasCallStack, MonadCleveland caps base m) => Bool -> Builder -> m ()
assert b errMsg =
  unless b $ failure errMsg

-- | @x \@== expected@ fails the test if @x@ is not equal to @expected@.
(@==)
  :: (HasCallStack, MonadCleveland caps base m, Eq a, Buildable a)
  => a -- ^ The actual value.
  -> a -- ^ The expected value.
  -> m ()
actual @== expected =
  assert (actual == expected) $
    unlinesF
      [ "Failed comparison"
      , "━━ Expected (rhs) ━━"
      , build expected
      , "━━ Got (lhs) ━━"
      , build actual
      ]
infix 1 @==

-- | Fails the test if the two given values are equal.
(@/=)
  :: (HasCallStack, MonadCleveland caps base m, Eq a, Buildable a)
  => a -> a -> m ()
a @/= b =
  assert (a /= b) $
    unlinesF
      [ "The two values are equal:"
      , build a
      ]
infix 1 @/=

-- | Monadic version of '@=='.
--
-- > getBalance addr @@== 10
(@@==)
  :: (HasCallStack, MonadCleveland caps base m, Eq a, Buildable a)
  => m a -- ^ The actual value.
  -> a -- ^ The expected value.
  -> m ()
getActual @@== expected = do
  actual <- getActual
  actual @== expected
infix 1 @@==

-- | Monadic version of '@/='.
--
-- > getBalance addr @@/= 10
(@@/=)
  :: (HasCallStack, MonadCleveland caps base m, Eq a, Buildable a)
  => m a -> a -> m ()
getA @@/= b =  do
  a <- getA
  a @/= b
infix 1 @@/=

-- | Fails the test if the comparison operator fails when applied to the given arguments.
-- Prints an error message with both arguments.
--
-- Example:
--
-- > checkCompares 2 (>) 1
checkCompares
  :: forall a b caps base m
   . (HasCallStack, MonadCleveland caps base m, Buildable a, Buildable b)
  => a
  -> (a -> b -> Bool)
  -> b
  -> m ()
checkCompares a f b = checkComparesWith pretty a f pretty b

-- | Like 'checkCompares', but with an explicit show function.
-- This function does not have any constraint on the type parameters @a@ and @b@.
--
-- For example, to print with 'Fmt.pretty':
--
-- > checkComparesWith pretty a (<) pretty b
checkComparesWith
  :: forall a b caps base m
   . (HasCallStack, MonadCleveland caps base m)
  => (a -> Text)
  -> a
  -> (a -> b -> Bool)
  -> (b -> Text)
  -> b
  -> m ()
checkComparesWith showA a f showB b =
  assert (f a b) $
    unlinesF
      [ "Failed"
      , "━━ lhs ━━"
      , showA a
      , "━━ rhs ━━"
      , showB b
      ]

----------------------------------------------------------------------------
-- Exception Handling
----------------------------------------------------------------------------

-- | Attempt to run an action and return its result or, if interpretation fails, an error.
attempt
  :: forall e caps base m a. (HasCallStack, MonadCleveland caps base m, Exception e)
  => m a -> m (Either e a)
attempt = implActionToCaps cmiAttempt

-- | Asserts that a transfer should fail, and returns the exception.
catchTransferFailure :: (HasCallStack, MonadCleveland caps base m) => m a -> m TransferFailure
catchTransferFailure action =
  attempt action >>= \case
    Left err -> return err
    Right _ -> runIO $ throwM UnexpectedSuccess

-- | Asserts that a transfer should fail, and runs some 'TransferFailurePredicate's over the exception.
--
-- > expectTransferFailure (failedWith (constant @MText "NOT_ADMIN")) $
-- >   call contractAddr (Call @"Ep") arg
--
-- > call contractAddr (Call @"Ep") arg & expectTransferFailure
-- >   ( failedWith (customError #tag 3) &&
-- >     addressIs contractAddr
-- >   )
expectTransferFailure :: (HasCallStack, MonadCleveland caps base m) => TransferFailurePredicate -> m a -> m ()
expectTransferFailure predicate act = do
  err <- catchTransferFailure act
  checkTransferFailure err predicate

-- | Check whether a given predicate holds for a given 'TransferFailure'.
checkTransferFailure :: (HasCallStack, MonadCleveland caps base m) => TransferFailure -> TransferFailurePredicate -> m ()
checkTransferFailure err predicate =
  case go predicate of
    Success () -> pass
    Failure expectedOutcome -> failure $ unlinesF
      [ "Expected transfer to fail with an error such that:"
      , ""
      , indentF 2 $ unlinesF expectedOutcome
      , ""
      , "But these conditions were not met."
      , "Actual transfer error:"
      , indentF 2 $ build err
      ]
  where
    go :: TransferFailurePredicate -> Validation (NonEmpty Builder) ()
    go = \case
      AndPredicate ps ->
        first (fmtExpectedOutcomes "AND") (traverse_ go ps)
      OrPredicate ps ->
        case traverse_ go ps of
          Success () -> Success ()
          Failure expectedOutcomes ->
            if length expectedOutcomes == length ps
              -- If all sub-predicates failed, then this predicate failed.
              then Failure $ fmtExpectedOutcomes "OR" expectedOutcomes
              -- If at least 1 sub-predicate succeeded, then this predicate succeeded.
              else Success ()
      TransferFailurePredicate p -> first one $ p err

    fmtExpectedOutcomes :: Builder -> NonEmpty Builder -> NonEmpty Builder
    fmtExpectedOutcomes delimiter = \case
      expectedOutcome :| [] -> one expectedOutcome
      expectedOutcomes ->
        one $ unlinesF
          [ "("
          , indentF 2 $ unlinesF $ NE.intersperse delimiter expectedOutcomes
          , ")"
          ]

-- | Asserts that interpretation of a contract ended with @FAILWITH@, returning the given constant value.
expectFailedWith
  :: forall err a caps base m
   . (HasCallStack, MonadCleveland caps base m, NiceConstant err)
  => err -> m a -> m ()
expectFailedWith err = expectTransferFailure $ failedWith (constant err)

-- | Asserts that interpretation of a contract ended with @FAILWITH@, returning the given lorentz error.
expectError
  :: forall err a caps base m
   . (HasCallStack, MonadCleveland caps base m, IsError err)
  => err -> m a -> m ()
expectError err = expectTransferFailure $ failedWith (lerror err)

-- | Asserts that interpretation of a contract ended with @FAILWITH@, returning the given custom lorentz error.
expectCustomError
  :: forall arg a tag caps base m
   . ( HasCallStack, MonadCleveland caps base m
     , IsError (CustomError tag)
     , MustHaveErrorArg tag (MText, arg)
     )
  => Label tag -> arg -> m a -> m ()
expectCustomError tag arg = expectTransferFailure $ failedWith (customError tag arg)

-- | Version of 'expectCustomError' for error with @unit@ argument.
expectCustomError_
  :: ( HasCallStack, MonadCleveland caps base m
     , IsError (CustomError tag)
     , MustHaveErrorArg tag (MText, ())
     )
  => Label tag -> m a -> m ()
expectCustomError_ tag = expectCustomError tag ()

-- | Version of 'expectCustomError' specialized for expecting @NoErrorArg@s.
expectCustomErrorNoArg
  :: ( HasCallStack, MonadCleveland caps base m
     , IsError (CustomError tag)
     , MustHaveErrorArg tag MText
     )
  => Label tag -> m a -> m ()
expectCustomErrorNoArg tag = expectTransferFailure $ failedWith (customErrorNoArg tag)

-- | Asserts that interpretation of a contract ended with @FAILWITH@, returning the given lorentz numeric error.
expectNumericError
  :: forall err a caps base m
   . (HasCallStack, MonadCleveland caps base m, IsError err)
  => ErrorTagMap -> err -> m a -> m ()
expectNumericError tagMap err = expectTransferFailure $ failedWith (numericError tagMap err)

----------------------------------------------------------------------------
-- TransferFailure Predicates
----------------------------------------------------------------------------

-- | A predicate that checks whether a transfer operation failed for the expected reason.
--
-- Predicates can be combined using the '&&' and '||' operators.
data TransferFailurePredicate
  = TransferFailurePredicate
      (TransferFailure -> Validation Builder ())
      -- ^ A predicate that either returns () or, if it fails,
      -- a message explaining what the expected outcome was.
  | AndPredicate (NonEmpty TransferFailurePredicate)
  | OrPredicate (NonEmpty TransferFailurePredicate)

instance Boolean TransferFailurePredicate where
  AndPredicate l && AndPredicate r = AndPredicate $ l <> r
  AndPredicate l && r = AndPredicate $ l <> one r
  l && AndPredicate r = AndPredicate $ one l <> r
  l && r = AndPredicate $ one l <> one r

  OrPredicate l || OrPredicate r = OrPredicate $ l <> r
  OrPredicate l || r = OrPredicate $ l <> one r
  l || OrPredicate r = OrPredicate $ one l <> r
  l || r = OrPredicate $ one l <> one r

-- | Asserts that interpretation of a contract failed due to a shift overflow error.
shiftOverflow :: TransferFailurePredicate
shiftOverflow = TransferFailurePredicate \case
  ShiftOverflow _ -> pass
  _ -> Failure "Contract failed due to a shift overflow"

-- | Asserts that an action failed due to an attempt to transfer 0tz towards a simple address.
emptyTransaction :: TransferFailurePredicate
emptyTransaction = TransferFailurePredicate \case
  EmptyTransaction _ -> pass
  _ -> Failure "Attempted to transfer 0tz to a simple address"

-- | Asserts that an action failed due to an attempt to call a contract with an invalid parameter.
badParameter :: TransferFailurePredicate
badParameter = TransferFailurePredicate \case
  BadParameter _ -> pass
  _ -> Failure "Attempted to call a contract with a parameter of the wrong type"

-- | Asserts that interpretation of a contract ended with @FAILWITH@, throwing the given error.
--
-- This function should be used together with one of the "@FAILWITH@ constructors" (e.g. 'constant', 'customError').
failedWith :: SomeConstant -> TransferFailurePredicate
failedWith expectedFailWithVal = TransferFailurePredicate \case
  FailedWith _ actualFailWithExpr
    | actualFailWithExpr `isEq` expectedFailWithVal -> pass
  _ -> Failure $ "Contract failed with: " <> buildRenderDoc expectedFailWithVal
  where
    isEq :: Expression -> SomeConstant -> Bool
    isEq expr (SomeConstant (v :: T.Value t)) =
      either (const False) (== v) (fromExpression @(T.Value t) expr)

-- | Asserts that the error occurred while interpreting the contract with the given address.
addressIs
  :: ToAddress addr
  => addr -- ^ The expected address.
  -> TransferFailurePredicate
addressIs (toAddress -> expectedAddr) = TransferFailurePredicate \err -> do
  let actualAddr =
        case err of
          FailedWith addr _ -> addr
          ShiftOverflow addr -> addr
          EmptyTransaction addr -> addr
          BadParameter addr -> addr
  when (actualAddr /= expectedAddr) $
    Failure $ "Failure occurred in contract with address: " <> build expectedAddr

----------------------------------------------------------------------------
-- 'FAILWITH' errors
----------------------------------------------------------------------------

-- | A constant michelson value that a contract threw with @FAILWITH@.
constant :: forall err. NiceConstant err => err -> SomeConstant
constant err =
  withDict (niceConstantEvi @err) $
    SomeConstant $ toVal err

-- | A lorentz error.
lerror :: forall err. IsError err => err -> SomeConstant
lerror err = errorToVal err SomeConstant

-- | A custom lorentz error.
customError
  :: forall arg tag. (IsError (CustomError tag), MustHaveErrorArg tag (MText, arg))
  => Label tag -> arg -> SomeConstant
customError tag arg =
  lerror $ CustomError tag (errorTagToMText tag, arg)

-- | A custom lorentz error with a @unit@ argument.
customError_
  :: (IsError (CustomError tag), MustHaveErrorArg tag (MText, ()))
  => Label tag -> SomeConstant
customError_ tag = customError tag ()

-- | A custom lorentz error with no argument.
customErrorNoArg
  :: (IsError (CustomError tag), MustHaveErrorArg tag MText)
  => Label tag -> SomeConstant
customErrorNoArg tag =
  lerror $ CustomError tag (errorTagToMText tag)

-- | A lorentz numeric error.
numericError :: forall err. IsError err => ErrorTagMap -> err -> SomeConstant
numericError tagMap err = errorToValNumeric tagMap err SomeConstant

----------------------------------------------------------------------------
-- Capabilities
----------------------------------------------------------------------------

-- | Adapt @caps@-based interface back to argument passing style.
uncapsCleveland
  :: forall m a. Monad m
  => ClevelandT m a
  -> ClevelandImpl m
  -> Address
  -> m a
uncapsCleveland action ClevelandImpl{..} moneybagAddr = do
  runReaderT action $
    Caps.buildCaps $
      Caps.AddCap (clevelandOpsCapImpl ciOpsImpl) $
      Caps.AddCap (clevelandMiscCapImpl ciMiscImpl) $
      Caps.AddCap (senderCapImpl moneybagAddr) $
      Caps.AddCap (moneybagCapImpl moneybagAddr) $
      Caps.BaseCaps Caps.emptyCaps

clevelandOpsCapImpl
  :: forall m. Monad m
  => ClevelandOpsFromImpl m -> Caps.CapImpl ClevelandOpsCap '[] m
clevelandOpsCapImpl implFrom = Caps.CapImpl $ ClevelandOpsCap \sender ->
  let impl = implFrom sender
  in ClevelandOpsImpl
  { coiRunOperationBatch = lift ... coiRunOperationBatch impl
  }

clevelandMiscCapImpl
  :: forall m. Monad m
  => ClevelandMiscImpl m -> Caps.CapImpl ClevelandMiscImpl '[] m
clevelandMiscCapImpl impl = Caps.CapImpl $ ClevelandMiscImpl
  { cmiRunIO = lift . cmiRunIO impl
  , cmiResolveAddress = lift . cmiResolveAddress impl
  , cmiSignBytes = lift ... cmiSignBytes impl
  , cmiGenKey = lift . cmiGenKey impl
  , cmiGenFreshKey = lift . cmiGenFreshKey impl
  , cmiOriginateLargeUntyped = lift ... cmiOriginateLargeUntyped impl
  , cmiComment = lift . cmiComment impl
  , cmiGetBalance = lift . cmiGetBalance impl
  , cmiGetStorage = lift . cmiGetStorage impl
  , cmiGetStorageExpr = lift . cmiGetStorageExpr impl
  , cmiGetBigMapValueMaybe = lift ... cmiGetBigMapValueMaybe impl
  , cmiGetAllBigMapValuesMaybe = lift ... cmiGetAllBigMapValuesMaybe impl
  , cmiGetPublicKey = lift . cmiGetPublicKey impl
  , cmiGetChainId = lift $ cmiGetChainId impl
  , cmiAdvanceTime = lift ... cmiAdvanceTime impl
  , cmiAdvanceToLevel = lift ... cmiAdvanceToLevel impl
  , cmiGetNow = lift ... cmiGetNow impl
  , cmiGetLevel = lift ... cmiGetLevel impl
  , cmiFailure = lift ... cmiFailure impl
  , cmiGetApproximateBlockInterval = lift $ cmiGetApproximateBlockInterval impl
  , cmiAttempt = \action -> ReaderT $ \r -> cmiAttempt impl (runReaderT action r)
  , cmiMarkAddressRefillable = lift ... cmiMarkAddressRefillable impl
  }

senderCapImpl :: Address -> Caps.CapImpl SenderCap '[] m
senderCapImpl sender = Caps.CapImpl $ SenderCap (Sender sender)

moneybagCapImpl :: Address -> Caps.CapImpl MoneybagCap '[] m
moneybagCapImpl moneybag = Caps.CapImpl $ MoneybagCap (Moneybag moneybag)

instance (Monad m, Caps.HasCaps '[ClevelandOpsCap, SenderCap] caps) =>
         MonadOps (Caps.CapsT caps m) where
  withOpsImpl action = do
    caps <- ask
    let SenderCap sender = Caps.getCap caps
        ClevelandOpsCap impl = Caps.getCap caps
    action (impl sender)

uncapsEmulated
  :: forall m a. Monad m
  => EmulatedT m a
  -> EmulatedImpl m
  -> ClevelandImpl m
  -> Address
  -> m a
uncapsEmulated action implEmulated ClevelandImpl{..} moneybagAddr = do
  runReaderT action $
    Caps.buildCaps $
      Caps.AddCap (clevelandOpsCapImpl ciOpsImpl) $
      Caps.AddCap (clevelandMiscCapImpl ciMiscImpl) $
      Caps.AddCap (emulatedCapImpl implEmulated) $
      Caps.AddCap (senderCapImpl moneybagAddr) $
      Caps.AddCap (moneybagCapImpl moneybagAddr) $
      Caps.BaseCaps Caps.emptyCaps

emulatedCapImpl :: Monad m => EmulatedImpl m -> Caps.CapImpl EmulatedImpl '[] m
emulatedCapImpl impl = Caps.CapImpl $ EmulatedImpl
  { eiBranchout =
      \(scenarios :: [(Text, Caps.CapsT caps m ())]) ->
        ReaderT $ \caps ->
          let scenarios' :: [(Text, m ())] = second (flip runReaderT caps) <$> scenarios
          in  eiBranchout impl scenarios'
  , eiOffshoot =
      \name (scenario :: Caps.CapsT caps m ()) ->
        ReaderT $ \caps ->
          let scenario' :: m () = runReaderT scenario caps
          in  eiOffshoot impl name scenario'
  , eiGetStorage = lift ... eiGetStorage impl
  , eiGetMorleyLogs = lift ... eiGetMorleyLogs impl
  , eiGetAddressMorleyLogs = lift ... eiGetAddressMorleyLogs impl
  , eiSetVotingPowers = lift ... eiSetVotingPowers impl
  }

----------------------------------------------------------------------------
-- Internal helpers
----------------------------------------------------------------------------

class ActionToCaps caps m x where
  actionToCaps :: (Caps.Capabilities caps m -> x) -> x

instance (Monad base, m ~ Caps.CapsT caps base) =>
         ActionToCaps caps base (Caps.CapsT caps base a) where
  actionToCaps :: (Caps.Capabilities caps base -> m a) -> m a
  actionToCaps action = do
    impl <- ask
    action impl

instance ActionToCaps caps m r => ActionToCaps caps m (x -> r) where
  actionToCaps :: (Caps.Capabilities caps m -> (x -> r)) -> (x -> r)
  actionToCaps action x = actionToCaps (flip action x)

implActionToCaps
  :: ( ActionToCaps caps m r, Typeable impl
     , Caps.HasCap impl caps
     )
  => (impl (Caps.CapsT caps m) -> r) -> r
implActionToCaps action =
  actionToCaps $ \caps -> action (Caps.getCap caps)

miscImplAndSenderActionToCaps
  :: ( ActionToCaps caps m r
     , Caps.HasCaps [ClevelandMiscImpl, SenderCap] caps
     )
  => (ClevelandMiscImpl (Caps.CapsT caps m) -> Sender -> r) -> r
miscImplAndSenderActionToCaps action =
  actionToCaps $ \caps ->
    let SenderCap sender = Caps.getCap caps
        impl = Caps.getCap caps
    in action impl sender

class ActionToMonadOps m x where
  actionToMonadOps :: (ClevelandOpsImpl m -> x) -> x

-- Using IncoherentInstances is evil, but this time if anything goes wrong
-- we should get a compilation error.
instance {-# INCOHERENT #-} MonadOps m => ActionToMonadOps m (m a) where
  actionToMonadOps :: (ClevelandOpsImpl m -> m a) -> m a
  actionToMonadOps = withOpsImpl

instance {-# INCOHERENT #-} ActionToMonadOps m r => ActionToMonadOps m (x -> r) where
  actionToMonadOps :: (ClevelandOpsImpl m -> (x -> r)) -> (x -> r)
  actionToMonadOps action x = actionToMonadOps (flip action x)
