-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- Utility functions used in the @cleveland:test:cleveland-test@ test suite.
module TestSuite.Util
  ( runViaTastyOnEmulator
  , runViaTastyOnNetwork
  , outcomeIsFailure
  , shouldFailWithMessage

  -- * Contracts
  , idContract
  , saveInStorageContract
  ) where

import Lorentz

import Data.List (isInfixOf)
import Fmt (build, indentF, unlinesF)
import Test.Tasty.HUnit (Assertion, assertFailure, testCase)
import Test.Tasty.Options (OptionSet, singleOption)
import Test.Tasty.Providers (IsTest(run), TestName)
import Test.Tasty.Runners
  (FailureReason(TestFailed), Outcome(Failure, Success), Result(resultOutcome),
  TestTree(AskOptions))

import Test.Cleveland
  (ClevelandT, EmulatedT, MonadCleveland, attempt, failure, uncapsCleveland, uncapsEmulated)
import Test.Cleveland.Internal.Client (ClientM)
import Test.Cleveland.Internal.Pure (PureM)
import Test.Cleveland.Tasty.Internal
  (RunOnEmulator(RunOnEmulator), RunOnNetwork(RunOnNetwork), whenNetworkEnabled)
import Test.Cleveland.Tasty.Internal.Options (ContextLinesOpt(ContextLinesOpt))

-- | Runs a cleveland action via Tasty, and performs some checks on the Tasty result.
--
-- It uses the Tasty options taken from the environment (CLI and environment variables).
-- It also sets 'ContextLinesOpt' to 0, otherwise the test report may capture
-- source code lines not actually related to the test.
--
-- These options can be overriden by passing in an additional 'OptionSet'.
runViaTastyOnEmulator :: TestName -> OptionSet -> EmulatedT PureM () -> (Result -> Assertion) -> TestTree
runViaTastyOnEmulator testName options clevelandTest checkResult =
  AskOptions $ \envOptions ->
    testCase testName $ do
      let allOptions = envOptions <> singleOption (ContextLinesOpt 0) <> options
      let tastyTest = RunOnEmulator $ uncapsEmulated (void clevelandTest)
      tastyResult <- run allOptions tastyTest (\_ -> pure ())
      checkResult tastyResult

-- | Runs a cleveland action via Tasty, and performs some checks on the Tasty result.
--
-- It uses the Tasty options taken from the environment (CLI and environment variables).
-- It also sets 'ContextLinesOpt' to 0, otherwise the test report may capture
-- source code lines not actually related to the test.
--
-- These options can be overriden by passing in an additional 'OptionSet'.
runViaTastyOnNetwork :: TestName -> OptionSet -> ClevelandT ClientM () -> (Result -> Assertion) -> TestTree
runViaTastyOnNetwork testName options clevelandTest checkResult =
  whenNetworkEnabled $ \_ ->
    AskOptions $ \envOptions ->
      testCase testName $ do
        let allOptions = envOptions <> singleOption (ContextLinesOpt 0) <> options
        let tastyTest = RunOnNetwork $ uncapsCleveland (void clevelandTest)
        tastyResult <- run allOptions tastyTest (\_ -> pure ())
        checkResult tastyResult

-- | Checks that a Tasty test failed because the nested cleveland test failed
-- (and not because, for example, the test framework threw an exception).
outcomeIsFailure :: HasCallStack => Result -> Assertion
outcomeIsFailure tastyResult =
  case resultOutcome tastyResult of
    Failure TestFailed -> pass
    Failure reason ->
      assertFailure $ toString $ unlines
        [ "Expected Tasty test to fail because the cleveland test failed, but failed because:"
        , show reason
        ]
    Success ->
      assertFailure "Expected Tasty test to fail, but it succeeded"

-- | Check that a Cleveland test fails and that the error message contains
-- the given string.
shouldFailWithMessage
  :: forall any caps base m.
     (HasCallStack, MonadCleveland caps base m, Show any)
  => String -> m any -> m ()
shouldFailWithMessage expectedMessage action  = do
  attempt @SomeException action >>= \case
    Right x ->
      failure $ "Expected action to fail, but it succeeded with: " <> show x
    Left ex ->
      unless (expectedMessage `isInfixOf` displayException ex) $
        failure $ unlinesF
          [ "━━━━━ Expected error message to contain:"
          , indentF 2 $ build expectedMessage
          , ""
          , "━━━━━ Actual error message was:"
          , indentF 2 $ build $ displayException ex
          ]

----------------------------------------------------------------------------
-- Test contracts
----------------------------------------------------------------------------

-- | Simple contract that does nothing when called.
idContract :: (NiceParameterFull cp, NiceStorage st) => Contract cp st
idContract = defaultContract $
  cdr #
  nil @Operation #
  pair

-- | Simple contract that takes a parameter and saves it in its storage.
saveInStorageContract :: (NiceParameterFull st, NiceStorage st) => Contract st st
saveInStorageContract = defaultContract $
  car #
  nil @Operation #
  pair
