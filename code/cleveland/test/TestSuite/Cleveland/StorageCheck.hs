-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE OverloadedLists #-}

module TestSuite.Cleveland.StorageCheck
  ( test_GetStorage
  , test_GetFullStorage
  , test_GetStorageExpr
  ) where

import Lorentz hiding (comment)

import Fmt (Buildable(..), genericF)
import Test.Tasty (TestTree)

import Morley.Micheline (fromExpression, toExpression)
import qualified Morley.Michelson.Typed as T
import Test.Cleveland
import Test.Cleveland.Tasty

import TestSuite.Util (idContract, saveInStorageContract)

data Storage = Storage
  { _stField1 :: Natural
  , _stField2 :: BigMap Natural MText
  }
  deriving stock (Generic, Show, Eq)
  deriving anyclass (IsoValue, HasAnnotation)
instance Buildable Storage where build = genericF

deriveRPC "Storage"

test_GetStorage :: TestTree
test_GetStorage =
  clevelandScenarioCaps "getStorage returns the contract's storage" $ do
    addr <- originateSimple
      "save parameter in storage"
      (1 :: Natural)
      saveInStorageContract

    comment "checking initial storage"
    getStorage addr @@== 1

    transfer TransferData
      { tdTo = addr
      , tdAmount = 100
      , tdEntrypoint = DefEpName
      , tdParameter = 2 :: Natural
      }

    comment "storage is updated after transfer"
    getStorage addr @@== 2

test_GetFullStorage :: TestTree
test_GetFullStorage =
  emulatedScenarioCaps "getFullStorage returns storage with big_map's contents" $ do
    let initialStorage =
          Storage
            { _stField1 = 23
            , _stField2 =
                [ (1, "a")
                , (2, "b")
                ]
            }
    addr <- originateSimple @() @Storage "test" initialStorage idContract
    getFullStorage addr @@== initialStorage

test_GetStorageExpr :: TestTree
test_GetStorageExpr = clevelandScenarioCaps "getStorageExpr is consistent with getStorage" $ do
  addr <- originateSimple @() @Storage "test" (Storage 1 [(2, "a")]) idContract
  storage <- getStorage addr
  let expectedExpr = toExpression (toVal storage)
  let expectedVal = either (error . show) id $ fromExpression @(T.Value $ T.ToT (Integer, Integer)) expectedExpr
  actualExpr <- getStorageExpr addr
  let actualVal = either (error . show) id $ fromExpression @(T.Value $ T.ToT (Integer, Integer)) actualExpr
  actualVal @== expectedVal
