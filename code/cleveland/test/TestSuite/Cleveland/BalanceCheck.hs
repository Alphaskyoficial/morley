-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.BalanceCheck
  ( test_BalanceCheck
  , test_EmptyBalanceCheck
  ) where

import Test.Tasty

import Morley.Tezos.Address (Address)
import Test.Cleveland
import Test.Cleveland.Tasty

test_BalanceCheck :: TestTree
test_BalanceCheck =
  clevelandScenarioCaps "An address's balance can be checked" $ do
    test :: Address <- newFreshAddress auto

    comment "balance is updated after transfer"
    transferMoney test 100
    getBalance test @@== 100

test_EmptyBalanceCheck :: TestTree
test_EmptyBalanceCheck =
  clevelandScenarioCaps "An empty address' balance can be checked" $ do
    test <- newFreshAddress auto
    dummy <- newFreshAddress auto

    -- Doing something in order not to get "validating empty scenario" error
    transferMoney dummy 1
    getBalance test @@== 0
