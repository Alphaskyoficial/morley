-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.TransferCheck
  ( test_TransferFromContract
  , test_nonUnitParamToImplicitAccount_fails
  ) where

import Named ((!))
import Test.Tasty

import Lorentz ((#))
import qualified Lorentz as L
import Lorentz.Value
import Test.Cleveland
import Test.Cleveland.Tasty

import TestSuite.Util (idContract, shouldFailWithMessage)

test_TransferFromContract :: [TestTree]
test_TransferFromContract =
  [ emulatedScenarioCaps "Disallow transferring ꜩ when revealing a contract (#440)" do
      testRevealContract morleyMessage
  , networkScenarioCaps "Disallow transferring ꜩ when revealing a contract (#440)" do
      testRevealContract rpcMessage

  , emulatedScenarioCaps "Disallow transferring ꜩ from an empty implicit account (#440)" do
      testEmptyImplicitAccount morleyMessage
  , networkScenarioCaps  "Disallow transferring ꜩ from an empty implicit account (#440)" do
      testEmptyImplicitAccount rpcMessage
  , clevelandScenarioCaps "Fails transfering 0tz to plain account"
      $ testZeroTransactionFails
  , clevelandScenarioCaps "Success transfering 0tz to a contract"
      $ testZeroTransactionSuccess
  , clevelandScenarioCaps "Allow transferring 0ꜩ from TRANSFER_TOKENS (#440)" testEmptyTransfer
  ]
  where
    morleyMessage :: String
    morleyMessage = "Global transaction of funds (200 μꜩ) from an originated contract"

    rpcMessage :: String
    rpcMessage = "Contracts (rx) cannot be revealed"

    testRevealContract :: MonadCleveland caps base m => String -> m ()
    testRevealContract expectedErrorMsg = do
      addr <- newAddress auto
      contractAddr <- originateSimple @() @() "rx" () idContract

      comment "give some funds to the originated contract"
      transferMoney contractAddr 200

      comment "fail when transferring from contract"
      transferMoney addr 200
        & withSender (toAddress contractAddr)
        & shouldFailWithMessage expectedErrorMsg

    testEmptyImplicitAccount :: MonadCleveland caps base m => String -> m ()
    testEmptyImplicitAccount expectedErrorMsg = do
      testAddr <- newFreshAddress auto
      contractAddr <- originate OriginateData
        { odName = "rx"
        , odStorage = ()
        , odBalance = 500
        , odContract = idContract @() @()
        }

      comment "fail when transferring from contract"
      transferMoney testAddr 200
        & withSender (toAddress contractAddr)
        & shouldFailWithMessage expectedErrorMsg

    testEmptyTransfer :: MonadCleveland caps base m => m ()
    testEmptyTransfer = do
      addr1 <- originateSimple "test1" () zeroTransferContract
      addr2 <- originateSimple "test2" () idContract
      call addr1 L.CallDefault (toTAddress addr2)

zeroTransferContract :: L.Contract (L.TAddress ()) ()
zeroTransferContract = L.defaultContract $
  L.car #
  L.pairE
    ( L.transferTokensE
        ! #contract do
            L.contract # L.assertSome [L.mt|Invalid contract address|]
        ! #amount do L.push zeroMutez
        ! #arg L.unit
      L.|:| L.nil
    , L.unit
    )

test_nonUnitParamToImplicitAccount_fails :: TestTree
test_nonUnitParamToImplicitAccount_fails =
  clevelandScenarioCaps "`transfer` fails when param for an implicit account is not Unit" do
    addr <- newFreshAddress "alias"
    transfer TransferData
      { tdTo = addr
      , tdAmount = 2
      , tdEntrypoint = DefEpName
      , tdParameter = (2 :: Natural)
      }
      & shouldFailWithMessage "Bad contract parameter for: "

testZeroTransactionFails :: MonadCleveland caps base m => m ()
testZeroTransactionFails = do
  wallet <- newAddress "wallet"
  let
    transferData = TransferData
      { tdTo = wallet
      , tdAmount = 0
      , tdEntrypoint = DefEpName
      , tdParameter = ()
      }

  expectTransferFailure emptyTransaction (transfer transferData)

testZeroTransactionSuccess :: MonadCleveland caps base m => m ()
testZeroTransactionSuccess = do
  address <- originateSimple @MText "test0tzContract" True idContract
  let
    transferData = TransferData
      { tdTo = address
      , tdAmount = 0
      , tdEntrypoint = DefEpName
      , tdParameter = "aaa" :: MText
      }

  transfer transferData
