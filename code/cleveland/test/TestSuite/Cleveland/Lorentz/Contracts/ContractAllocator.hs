-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.Lorentz.Contracts.ContractAllocator
  ( allocatorContract
  ) where

import Lorentz as L

import Test.Cleveland.Instances ()

allocatorContract :: Contract [Address] Storage
allocatorContract = defaultContract allocatorContractLorentz

type Storage = ("storage" :! [Address])

allocatorContractLorentz :: ContractCode [Address] Storage
allocatorContractLorentz =
  unpair #
  dip (nil @Operation) #
  iter (
    unsafeContractCalling @() DefEpName #
    whenSome
      (push 1 # unit # transferTokens # cons)
    ) #
  unit # push 0 # none @KeyHash #
  createContract @() @() (defaultContract failWith) #
  dip L.drop #
  cons #
  pair
