-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.WithSender
  ( test_Money_are_spent_from_sender
  , test_Sender_in_contract_call_is_updated
  ) where


import Test.Tasty

import Lorentz ((#))
import qualified Lorentz as L
import Test.Cleveland
import Test.Cleveland.Lorentz.Consumer
import Test.Cleveland.Tasty

returnSenderContract :: L.Contract (L.View () L.Address) ()
returnSenderContract = L.defaultContract $
  L.unpair # (L.view_ $ L.drop @() # L.drop @() # L.sender)

test_Money_are_spent_from_sender :: TestTree
test_Money_are_spent_from_sender =
  -- Running this on emulator won't work because there
  -- originations and transfers cost nothing
  networkScenarioCaps "Money is spent from sender address" $ do
    user <- newAddress auto

    balance1 <- getBalance user
    originateSimple "contract" () returnSenderContract
      & withSender user

    balance2 <- getBalance user

    checkCompares balance2 (<) balance1

test_Sender_in_contract_call_is_updated :: TestTree
test_Sender_in_contract_call_is_updated =
  clevelandScenarioCaps "Contract's sender is updated" $ do
    user <- newAddress auto

    consumer <- originateSimple "consumer" [] contractConsumer
    contract <- originateSimple "contract" () returnSenderContract

    call contract CallDefault (L.mkView () consumer)
      & withSender user

    getStorage consumer @@== [user]
