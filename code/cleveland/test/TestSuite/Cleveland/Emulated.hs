-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.Emulated
  ( test_Emulated
  ) where

import Test.Tasty

import Morley.Tezos.Address
import Test.Cleveland
import Test.Cleveland.Tasty

import TestSuite.Util (shouldFailWithMessage)

test_Emulated :: [TestTree]
test_Emulated =
  [ testGroup "branchout"
    [ emulatedScenarioCaps "passes if all branches pass" $
        branchout
          [ "a" ?- pass
          , "b" ?- pass
          ]

    , emulatedScenarioCaps "fails if any branch fails" $ do
        addr <- newAddress auto
        let branchA = "a" ?- transferMoney addr 0
        let branchB = "b" ?- pass
        branchout [branchA, branchB] & expectTransferFailure emptyTransaction
        branchout [branchB, branchA] & expectTransferFailure emptyTransaction

    , emulatedScenarioCaps "a branch's effects do not leak into another branch" $ do
        test <- newAddress auto
        initialBalance <- getBalance test
        branchout
          [ "a" ?- do
              transferMoney test 2
              getBalance test @@== initialBalance + 2
          , "b" ?- do
              transferMoney test 3
              getBalance test @@== initialBalance + 3
          ]

    , emulatedScenarioCaps "branchout's effects are discarded" $ do
        test <- newAddress auto
        initialBalance <- getBalance test
        branchout
          [ "a" ?- transferMoney test 3 ]
        getBalance test @@== initialBalance

    , emulatedScenarioCaps "adds branch name to error" do
        branchout
          [ "<branch name>" ?- void $
              signBytes "" (unsafeParseAddress "tz1Zpj6cNhkVkvksGtRTwTrgfCG3WP9eA5BM")
          ]
          & shouldFailWithMessage "<branch name>"
    ]
  , testGroup "offshoot"
    [ emulatedScenarioCaps "fails if inner scenario fails" $ do
        addr <- newAddress auto
        offshoot "a" (transferMoney addr 0)
          & expectTransferFailure emptyTransaction

    , emulatedScenarioCaps "offshoot's effects are discarded" $ do
        test <- newAddress auto
        initialBalance <- getBalance test
        offshoot "a" $ transferMoney test 1
        getBalance test @@== initialBalance
    ]
  ]
