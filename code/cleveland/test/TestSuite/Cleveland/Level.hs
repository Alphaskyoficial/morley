-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.Level
  ( test_Level
  ) where

import Test.Tasty

import Test.Cleveland
import Test.Cleveland.Michelson.Dummy (dummyLevel)
import Test.Cleveland.Tasty

test_Level :: TestTree
test_Level =
  testGroup "functions for level advancing" $
    [ testGroup "advanceLevel" $
      [ testGroup "advances levels by the exact number" $
          testDeltas <&> \delta ->
            clevelandScenarioCaps (show delta) $ do
              l0 <- getLevel
              advanceLevel delta
              l1 <- getLevel

              assert (l1 == l0 + delta) $
                mconcat
                  [ "Expected exactly "
                  , show delta
                  , " levels to be skipped, but "
                  , show (l1 - l0)
                  , " were actually skipped."
                  ]
      ]
    , testGroup "advanceToLevel" $
        [ testGroup "advances levels to the exact level" $
            testDeltas <&> \delta ->
              clevelandScenarioCaps (show delta) $ do
                l0 <- getLevel
                advanceToLevel (l0 + delta)
                l1 <- getLevel

                assert (l1 == l0 + delta) $
                  mconcat
                    [ "Expected to be at level "
                    , show (l0 + delta)
                    , " but was at "
                    , show l1
                    , "."
                    ]
        , clevelandScenarioCaps "is no-op if target level is lower than current level"  $ do
              l0 <- getLevel
              advanceToLevel (fromInteger $ (fromIntegral @_ @Integer l0) - 4)
              l1 <- getLevel

              assert (l1 == l0) $
                mconcat
                  [ "Expected to be at level "
                  , show l0
                  , " but was at "
                  , show l1
                  , "."
                  ]
        ]
    , emulatedScenarioCaps "initial level is 'dummyLevel' in the emulator" $ do
        l0 <- getLevel
        l0 @== dummyLevel
    ]
  where
    testDeltas :: [Natural]
    testDeltas =
      [ 0, 1, 2, 3, 4]
