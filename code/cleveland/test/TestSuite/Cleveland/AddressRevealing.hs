-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.AddressRevealing
  ( test_AddressRevealing
  ) where

import Test.Tasty

import Test.Cleveland
import Test.Cleveland.Tasty

test_AddressRevealing :: TestTree
test_AddressRevealing =
  clevelandScenarioCaps "New address key is revealed" $ do
    test <- newAddress auto
    test2 <- newFreshAddress auto

    transferMoney test 1000
    comment "new key address is revealed"
    withSender test $ transferMoney test2 1
