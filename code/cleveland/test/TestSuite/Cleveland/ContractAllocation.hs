-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.ContractAllocation
  ( test_ContractAllocation
  ) where

import Test.Tasty

import Morley.Tezos.Address (Address)
import Morley.Util.Named
import Test.Cleveland
import Test.Cleveland.Tasty

import TestSuite.Cleveland.Lorentz.Contracts.ContractAllocator

test_ContractAllocation :: TestTree
test_ContractAllocation =
  clevelandScenarioCaps "Contracts can be allocated and called" $ do
    addresses :: [Address] <- replicateM 10 $ newFreshAddress auto
    let
      od = OriginateData
        { odName = "allocator"
        , odStorage = #storage .! (take 570 $ cycle addresses)
        , odBalance = 25
        , odContract = allocatorContract
        }

    allocatorAddr <- originate od

    call allocatorAddr CallDefault (take 25 $ cycle addresses)
