-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.Assertions
  ( test_failure_fails
  , test_assert_succeeds
  , test_assert_fails
  , test_eq_succeeds
  , test_eq_fails
  , test_neq_succeeds
  , test_neq_fails
  , test_checkCompares_succeeds
  , test_checkCompares_fails
  , test_checkComparesWith_succeeds
  , test_checkComparesWith_fails
  ) where

import Fmt (pretty)
import Test.Tasty (TestTree)
import Text.RawString.QQ (r)

import Test.Cleveland
import Test.Cleveland.Tasty (clevelandScenarioCaps)

import TestSuite.Util (shouldFailWithMessage)

{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

test_failure_fails :: TestTree
test_failure_fails =
  clevelandScenarioCaps "`failure` fails unconditionally" do
    failure @() "<error msg>"
      & shouldFailWithMessage "<error msg>"

test_assert_succeeds :: TestTree
test_assert_succeeds =
  clevelandScenarioCaps "`assert` succeeds when condition is true" $
    assert True "<error msg>"

test_assert_fails :: TestTree
test_assert_fails =
  clevelandScenarioCaps "`assert` fails when condition is false" do
    assert False "<error msg>"
      & shouldFailWithMessage "<error msg>"

test_eq_succeeds :: TestTree
test_eq_succeeds =
  clevelandScenarioCaps "`@==` succeeds when the values are equal" $
    True @== True

test_eq_fails :: TestTree
test_eq_fails =
  clevelandScenarioCaps "`@==` fails when the values are not equal" do
    (True @== False)
      & shouldFailWithMessage [r|
━━ Expected (rhs) ━━
False
━━ Got (lhs) ━━
True|]

test_neq_succeeds :: TestTree
test_neq_succeeds =
  clevelandScenarioCaps "`@/=` succeeds when the values are not equal" $
    True @/= False

test_neq_fails :: TestTree
test_neq_fails =
  clevelandScenarioCaps "`@/=` fails when the values are equal" do
    (True @/= True)
      & shouldFailWithMessage "The two values are equal:"

test_checkCompares_succeeds :: TestTree
test_checkCompares_succeeds =
  clevelandScenarioCaps "`checkCompares` succeeds when the comparison succeeds" $
    checkCompares @Int 2 elem [1, 2, 3]

test_checkCompares_fails :: TestTree
test_checkCompares_fails =
  clevelandScenarioCaps "`checkCompares` fails when the comparison fails" do
    checkCompares @Int 1 elem []
      & shouldFailWithMessage [r|
━━ lhs ━━
1
━━ rhs ━━
[]|]

test_checkComparesWith_succeeds :: TestTree
test_checkComparesWith_succeeds =
  clevelandScenarioCaps "`checkComparesWith` succeeds when the comparison succeeds" $
    checkComparesWith @Int pretty 2 elem pretty [1, 2, 3]

test_checkComparesWith_fails :: TestTree
test_checkComparesWith_fails =
  clevelandScenarioCaps "`checkComparesWith` fails when the comparison fails and prints values" do
    checkComparesWith @Int toCardinal 1 elem (show . fmap toOrdinal) [2, 3]
      & shouldFailWithMessage [r|
━━ lhs ━━
One
━━ rhs ━━
["Second","Third"]|]
  where
    toCardinal = \case
      1 -> "One"
      2 -> "Two"
      3 -> "Three"
      n -> show n
    toOrdinal = \case
      1 -> "First"
      2 -> "Second"
      3 -> "Third"
      n -> show @String n
