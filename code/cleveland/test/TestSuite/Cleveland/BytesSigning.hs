-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.BytesSigning
  ( test_BytesSigning
  ) where

import Test.Tasty

import Lorentz ((#))
import qualified Lorentz as L
import Morley.Tezos.Crypto
import Test.Cleveland
import Test.Cleveland.Tasty

checkSignatureContract :: L.Contract (PublicKey, (L.TSignature ByteString, ByteString)) ()
checkSignatureContract = L.defaultContract $
  L.car #
  L.unpair # L.dip L.unpair #
  L.checkSignature # L.assert [L.mt|Invalid signature|] #
  L.unit # L.nil @L.Operation # L.pair

test_BytesSigning :: TestTree
test_BytesSigning =
  clevelandScenarioCaps "Bytestrings can be signed" $ do
    dummy <- newFreshAddress "user"
    signer <- newFreshAddress "signer"

    helper <- originateSimple "helper" () checkSignatureContract

    let bytes = "some bytestring"

    sig <- signBinary bytes signer
    signerPK <- getPublicKey signer
    dummyPK <- getPublicKey dummy

    expectFailedWith [L.mt|Invalid signature|] $
      call helper CallDefault (dummyPK, (sig, bytes))
    call helper CallDefault (signerPK, (sig, bytes))
