-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.OperationReplay
  ( test_replayOrigination_fails
  , test_replayTransfer_fails
  ) where

import Test.Tasty

import Morley.Tezos.Address (Address)
import Test.Cleveland
import Test.Cleveland.Tasty
import Test.Util.Contracts
import TestSuite.Util (idContract, shouldFailWithMessage)

test_replayTransfer_fails :: TestTree
test_replayTransfer_fails =
  emulatedScenarioCaps "Transfer operation replay is prohibited" $ do
    dummyAddress <- chAddress <$> originateSimple @() @() "dummy" () idContract
    contract <- importContract @Address @() (contractsDir </> "replay_transfer.tz")
    replayTransfer <- originateSimple "replayTransfer" () contract
    call replayTransfer CallDefault dummyAddress
      & shouldFailWithMessage "Operation replay attempt"

test_replayOrigination_fails :: TestTree
test_replayOrigination_fails =
  emulatedScenarioCaps "Origination operation replay is prohibited" $ do
    contract <- importContract @() @() (contractsDir </> "replay_origination.tz")
    replayOrigination <- originateSimple "replayOrigination" () contract
    call replayOrigination CallDefault ()
      & shouldFailWithMessage "Operation replay attempt"
