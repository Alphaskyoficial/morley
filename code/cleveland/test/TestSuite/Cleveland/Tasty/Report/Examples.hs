-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.Tasty.Report.Examples
  ( reportExamples
  ) where

import Data.String.Interpolate
import System.FilePath ((</>))
import Text.RawString.QQ (r)

import Morley.Tezos.Address
import Test.Cleveland
import Test.Cleveland.Internal.Pure



----------------------------------------------------------------------------
-- Examples
----------------------------------------------------------------------------

example1 :: EmulatedT PureM ()
example1 = void $ signBytes "" unknownAddr

example1ExpectedErr :: String
example1ExpectedErr =
  [i|
   ┏━━ #{reportExamplesPath} ━━━
23 ┃ example1 :: EmulatedT PureM ()
24 ┃ example1 = void $ signBytes "" unknownAddr
   ┃                   ^^^^^^^^^^^^^^^^^^^^^^^^
   ┃                   | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
25 ┃

CallStack (from HasCallStack):
  |]

----------------------------------------------------------------------------
-- Examples with helper functions
----------------------------------------------------------------------------

exampleWithHelperFunction1 :: EmulatedT PureM ()
exampleWithHelperFunction1 = exampleWithHelperFunction1Helper

exampleWithHelperFunction1Helper :: EmulatedT PureM ()
exampleWithHelperFunction1Helper = void $ signBytes "" unknownAddr

exampleWithHelperFunction1ExpectedErr :: String
exampleWithHelperFunction1ExpectedErr =
  [i|
   ┏━━ #{reportExamplesPath} ━━━
46 ┃ exampleWithHelperFunction1Helper :: EmulatedT PureM ()
47 ┃ exampleWithHelperFunction1Helper = void $ signBytes "" unknownAddr
   ┃                                           ^^^^^^^^^^^^^^^^^^^^^^^^
   ┃                                           | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
48 ┃

CallStack (from HasCallStack):
  |]

exampleWithHelperFunction2 :: EmulatedT PureM ()
exampleWithHelperFunction2 = exampleWithHelperFunction2Helper

exampleWithHelperFunction2Helper :: HasCallStack => EmulatedT PureM ()
exampleWithHelperFunction2Helper = withFrozenCallStack $ void $ signBytes "" unknownAddr

exampleWithHelperFunction2ExpectedErr :: String
exampleWithHelperFunction2ExpectedErr =
  [i|
   ┏━━ #{reportExamplesPath} ━━━
62 ┃ exampleWithHelperFunction2 :: EmulatedT PureM ()
63 ┃ exampleWithHelperFunction2 = exampleWithHelperFunction2Helper
   ┃                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   ┃                              | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
64 ┃

CallStack (from HasCallStack):
  |]

----------------------------------------------------------------------------
-- Example with no callstack frames
----------------------------------------------------------------------------

exampleNoCallStack1 :: EmulatedT PureM ()
exampleNoCallStack1 = withFrozenCallStack $ void $ signBytes "" unknownAddr

exampleNoCallStack1ExpectedErr :: String
exampleNoCallStack1ExpectedErr =
  [r|
Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
  |]

----------------------------------------------------------------------------
-- Example with errors spanning across multiple lines
----------------------------------------------------------------------------

exampleMultipleLines1 :: EmulatedT PureM ()
exampleMultipleLines1 =
  void ( signBytes
           ""
           unknownAddr )

exampleMultipleLines1ExpectedErr :: String
exampleMultipleLines1ExpectedErr =
  [i|
    ┏━━ #{reportExamplesPath} ━━━
 99 ┃ exampleMultipleLines1 =
100 ┃   void ( signBytes
101 ┃            ""
102 ┃            unknownAddr )
    ┃          ^^^^^^^^^^^^^
    ┃          | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
103 ┃

CallStack (from HasCallStack):
  |]

exampleMultipleLines2 :: EmulatedT PureM ()
exampleMultipleLines2 =
  void (   signBytes
         ""
           unknownAddr )

exampleMultipleLines2ExpectedErr :: String
exampleMultipleLines2ExpectedErr =
  [i|
    ┏━━ #{reportExamplesPath} ━━━
120 ┃ exampleMultipleLines2 =
121 ┃   void (   signBytes
122 ┃          ""
123 ┃            unknownAddr )
    ┃          ^^^^^^^^^^^^^
    ┃          | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
124 ┃

CallStack (from HasCallStack):
  |]

exampleMultipleLines3 :: EmulatedT PureM ()
exampleMultipleLines3 =
  void (   signBytes
           ""
         unknownAddr )

exampleMultipleLines3ExpectedErr :: String
exampleMultipleLines3ExpectedErr =
  [i|
    ┏━━ #{reportExamplesPath} ━━━
141 ┃ exampleMultipleLines3 =
142 ┃   void (   signBytes
143 ┃            ""
144 ┃          unknownAddr )
    ┃          ^^^^^^^^^^^
    ┃          | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
145 ┃

CallStack (from HasCallStack):
  |]

exampleMultipleLines4 :: EmulatedT PureM ()
exampleMultipleLines4 =
  void (      signBytes
                   ""
         unknownAddr   )

exampleMultipleLines4ExpectedErr :: String
exampleMultipleLines4ExpectedErr =
  [i|
    ┏━━ #{reportExamplesPath} ━━━
162 ┃ exampleMultipleLines4 =
163 ┃   void (      signBytes
164 ┃                    ""
165 ┃          unknownAddr   )
    ┃          ^^^^^^^^^^^^^^
    ┃          | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
166 ┃

CallStack (from HasCallStack):
  |]

exampleMultipleLines5 :: EmulatedT PureM ()
exampleMultipleLines5 =
  void (    signBytes
                     ""
         unknownAddr   )

exampleMultipleLines5ExpectedErr :: String
exampleMultipleLines5ExpectedErr =
  [i|
    ┏━━ #{reportExamplesPath} ━━━
183 ┃ exampleMultipleLines5 =
184 ┃   void (    signBytes
185 ┃                      ""
186 ┃          unknownAddr   )
    ┃          ^^^^^^^^^^^^^^
    ┃          | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
187 ┃

CallStack (from HasCallStack):
  |]

exampleMultipleLines6 :: EmulatedT PureM ()
exampleMultipleLines6 =
  void (    signBytes
                   ""
           unknownAddr   )

exampleMultipleLines6ExpectedErr :: String
exampleMultipleLines6ExpectedErr =
  [i|
    ┏━━ #{reportExamplesPath} ━━━
204 ┃ exampleMultipleLines6 =
205 ┃   void (    signBytes
206 ┃                    ""
207 ┃            unknownAddr   )
    ┃            ^^^^^^^^^^^
    ┃            | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
208 ┃

CallStack (from HasCallStack):
  |]

unknownAddr :: Address
unknownAddr = unsafeParseAddress "tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z"

reportExamples :: [(String, EmulatedT PureM (), String)]
reportExamples =
  [ ("example1", example1, example1ExpectedErr)
  , ("exampleWithHelperFunction1", exampleWithHelperFunction1, exampleWithHelperFunction1ExpectedErr)
  , ("exampleWithHelperFunction2", exampleWithHelperFunction2, exampleWithHelperFunction2ExpectedErr)
  , ("exampleNoCallStack1", exampleNoCallStack1, exampleNoCallStack1ExpectedErr)
  , ("exampleMultipleLines1", exampleMultipleLines1, exampleMultipleLines1ExpectedErr)
  , ("exampleMultipleLines2", exampleMultipleLines2, exampleMultipleLines2ExpectedErr)
  , ("exampleMultipleLines3", exampleMultipleLines3, exampleMultipleLines3ExpectedErr)
  , ("exampleMultipleLines4", exampleMultipleLines4, exampleMultipleLines4ExpectedErr)
  , ("exampleMultipleLines5", exampleMultipleLines5, exampleMultipleLines5ExpectedErr)
  , ("exampleMultipleLines6", exampleMultipleLines6, exampleMultipleLines6ExpectedErr)
  ]

reportExamplesPath :: FilePath
reportExamplesPath = "test" </> "TestSuite" </> "Cleveland" </> "Tasty" </> "Report" </> "Examples.hs"
