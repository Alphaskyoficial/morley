-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Module testing voting power instructions and their support from
-- cleveland.
module TestSuite.Cleveland.VotingPower
  ( test_VotingPowers
  ) where

import Test.Tasty

import Lorentz.Value
import Morley.Tezos.Crypto
import Test.Cleveland
import Test.Cleveland.Tasty

test_VotingPowers :: [TestTree]
test_VotingPowers =
  [ clevelandScenarioCaps "Can pick voting power of some address" $ do
      contract
        <- originateSimple "vp contract" 0
        =<< importContract @KeyHash @Natural ("../../contracts/voting_power.tz")

      let keyHash = unsafeParseKeyHash "tz1Yq4Hj9u2f9473wAKiFEm36Sp2GfB5aTGa"
      call contract CallDefault keyHash
      getStorage contract @@== 0

  , emulatedScenarioCaps "Can set voting powers in test scenario" $ do
      contract
        <- originateSimple "vp contract" (0, 0)
        =<< importContract @KeyHash @(Natural, Natural) ("../../contracts/voting_powers.tz")

      comment "Setting custom voting powers to access them later"
      let keyHash = unsafeParseKeyHash "tz1Yq4Hj9u2f9473wAKiFEm36Sp2GfB5aTGa"
      let keyHash2 = unsafeParseKeyHash "tz1hvYBbHRJhT3dQ8bEjc34xm3rjJJmTcuqs"
      setVotingPowers (mkVotingPowers [(keyHash, 123), (keyHash2, 57)])

      call contract CallDefault keyHash
      getStorage contract @@== (123, 180)
  ]
