-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE ApplicativeDo #-}

module TestSuite.Cleveland.BatchTransferCheck
  ( test_SimpleTransfer
  , test_BatchTransferLeapingCosts
  ) where

import Test.Tasty

import Lorentz ((#), (:->))
import qualified Lorentz as L
import Test.Cleveland
import Test.Cleveland.Tasty

test_SimpleTransfer :: TestTree
test_SimpleTransfer =
  clevelandScenarioCaps "Check the batch transaction correctness" $ do
    test1 <- newFreshAddress auto
    test2 <- newFreshAddress auto

    comment "balance is updated after batch transfer"
    inBatch $ do
      for_ [100, 200] $ transferMoney test1
      transferMoney test2 300
      return ()

    getBalance test1 @@== 300
    getBalance test2 @@== 300

-- | Add given element on stack once on first invocation, and 10k times on
-- subsequent invocations.
leapingContract :: forall a. (L.NiceParameterFull a, L.NiceStorage [a], L.Dupable a) => L.Contract a [a]
leapingContract = L.defaultContract $
  L.unpair #
  L.duupX @2 # L.size # L.int # L.ifEq0 (L.push 1) (L.push 5000) #
  lIterate (L.dup @a # L.dip L.cons) #
  L.drop @a #
  L.nil # L.pair
  where
    lIterate :: s :-> s -> Natural : s :-> s
    lIterate f =
      decrease #
      L.loop (L.dip f # decrease) #
      L.drop @Natural

    decrease :: Natural : s :-> Bool : Natural : s
    decrease =
      L.push @Natural 1 # L.rsub # L.isNat #
      L.ifSome (L.push True) (L.push 999 # L.push False)

-- | Even in case when transactions in batch have very different costs,
-- and costs of subsequent transactions is affected by previous transactions,
-- everything works as expected.
test_BatchTransferLeapingCosts :: TestTree
test_BatchTransferLeapingCosts =
  clevelandScenarioCaps "Check fees evaluation correctness for leaping transfer costs" $ do
    contract <- originateSimple "contract" [] (leapingContract @L.MText)

    comment "Perform batch transfer, second transaction should have much \
            \higher cost with respect to the first one"
    inBatch $
      for_ ["a", "b"] $ call contract CallDefault

    comment "Sanity check on storage"
    getStorage contract @@==
      replicate 5000 "b" ++ one "a"
