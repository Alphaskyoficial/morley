-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.BatchOperation
  ( test_SomeCases
  ) where

import Test.Tasty

import Lorentz.Value
import Test.Cleveland
import Test.Cleveland.Lorentz.Consumer
import Test.Cleveland.Tasty

test_SomeCases :: [TestTree]
test_SomeCases =
  [ clevelandScenarioCaps "Origination and transfer within a batch work" $ do
      test1 <- newFreshAddress auto

      contract1 <- inBatch $ do
        contract1 <- originateSimple "c" ([] :: [()]) contractConsumer
        transferMoney test1 100
        return contract1

      getBalance test1 @@== 100

      transferMoney contract1 200
      getBalance contract1 @@== 200


  , clevelandScenarioCaps "Loops within batch work" $ do
      test1 :: Address <- newFreshAddress auto
      test2 :: Address <- newFreshAddress auto

      inBatch $ do
        for_ [(test1, 100), (test2, 200)] $ uncurry transferMoney

      getBalance test1 @@== 100
      getBalance test2 @@== 200


  , clevelandScenarioCaps "Can return multiple values from a batch" $ do
      (_contract1, _contract2) <- inBatch $ do
        contract1 <- originateSimple "c1" ([] :: [Integer]) contractConsumer
        contract2 <- originateSimple "c2" ([] :: [Integer]) contractConsumer
        return (contract1, contract2)

      return ()
  ]
