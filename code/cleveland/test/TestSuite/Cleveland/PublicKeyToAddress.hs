-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Test for the calculation of an address from its public key. See [#354]
module TestSuite.Cleveland.PublicKeyToAddress
  ( test_publicKeyToAddress
  ) where

import Hedgehog (forAll, property, withTests)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)
import qualified Unsafe as Unsafe

import Lorentz as L hiding (comment)
import Test.Cleveland
import Test.Cleveland.Instances ()
import Test.Cleveland.Internal.Abstract (SpecificOrDefaultAliasHint(..))

publicKeyToAddress :: Contract PublicKey (Maybe Address)
publicKeyToAddress = defaultContract $
  L.car #
  L.hashKey #
  L.implicitAccount #
  L.address #
  L.some #
  L.nil #
  L.pair

test_publicKeyToAddress :: TestTree
test_publicKeyToAddress =
  testProperty "The address is calculated correctly from the given public key" $
    withTests 200 $ property $ do
      aliasHint <- fromString <$> (forAll $ Gen.string (Range.linear 0 100) Gen.unicode)
      clevelandTestProp $ uncapsCleveland $ scenario aliasHint

scenario :: (MonadCleveland caps base m) => AliasHint -> m ()
scenario aliasHint = do
  c <- originateSimple @PublicKey @(Maybe Address) "publicKeyToAddress" Nothing publicKeyToAddress

  addr1 <- newAddress $ SpecificAliasHint aliasHint
  addr1pk <- getPublicKey addr1

  call c CallDefault addr1pk
  addr2 <- Unsafe.fromJust <$> getStorage @(Maybe Address) c

  addr1 @== addr2
