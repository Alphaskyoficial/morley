-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests on importing functionality.
module TestSuite.Cleveland.Import
  ( test_Import
  , test_Embed
  , test_ImportInt
  , test_EmbedInt
  , test_ImportAddress
  , test_EmbedAddress
  , test_ImportMutez
  , test_EmbedMutez
  ) where

import Test.Tasty

import Lorentz.Value (Address, Mutez)
import Morley.Tezos.Address (ta)
import Test.Cleveland
import Test.Cleveland.Lorentz (embedContract, embedValue, importValue)
import Test.Cleveland.Tasty

test_Import :: [TestTree]
test_Import =
  [ clevelandScenarioCaps "Can embed a contract" $ do
      contract <- originateSimple "basic1" []
        $$(embedContract @() @[Integer] "../../contracts/basic1.tz")

      call contract CallDefault ()
  ]

test_Embed :: [TestTree]
test_Embed =
  [ clevelandScenarioCaps "Can embed a contract" $ do
      contractCode <- importContract @() @[Integer] "../../contracts/basic1.tz"
      contract <- originateSimple "basic1" [] contractCode

      call contract CallDefault ()
  ]

test_ImportInt :: [TestTree]
test_ImportInt =
  [ emulatedScenarioCaps "Can import an int value" $ do
      value <- runIO $ importValue @Integer "./test/fixtures/intValue.txt"

      value @== 42
  ]

test_EmbedInt :: [TestTree]
test_EmbedInt =
  [ emulatedScenarioCaps "Can embed an int value" $ do
      let value = $$(embedValue @Integer "./test/fixtures/intValue.txt")

      value @== 42
  ]

test_ImportAddress :: [TestTree]
test_ImportAddress =
  [ emulatedScenarioCaps "Can import an address value" $ do
      value <- runIO $ importValue @Address "./test/fixtures/addrValue.txt"

      value @== [ta|tz1Se5MhFNJdPrur2iNwC3nCLvYzr1y9TsZp|]
  ]

test_EmbedAddress :: [TestTree]
test_EmbedAddress =
  [ emulatedScenarioCaps "Can embed an address value" $ do
      let value = $$(embedValue @Address "./test/fixtures/addrValue.txt")

      value @== [ta|tz1Se5MhFNJdPrur2iNwC3nCLvYzr1y9TsZp|]
  ]

test_ImportMutez :: [TestTree]
test_ImportMutez =
  [ emulatedScenarioCaps "Can import a mutez value" $ do
      value <- runIO $ importValue @Mutez "./test/fixtures/intValue.txt"

      value @== 42
  ]

test_EmbedMutez :: [TestTree]
test_EmbedMutez =
  [ emulatedScenarioCaps "Can embed a mutez value" $ do
      let value = $$(embedValue @Mutez "./test/fixtures/intValue.txt")

      value @== 42
  ]
