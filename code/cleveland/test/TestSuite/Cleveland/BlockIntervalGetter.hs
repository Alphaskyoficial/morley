-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.BlockIntervalGetter
  ( test_BlockIntervalGetter
  ) where

import Test.Tasty

import Test.Cleveland
import Test.Cleveland.Tasty

test_BlockIntervalGetter :: TestTree
test_BlockIntervalGetter =
  clevelandScenarioCaps "Approximate block interval can be obtained" $ do
    comment "Getting approximate block interval"
    blockInterval <- getApproximateBlockInterval
    comment "Wait for obtained interval amount of time"
    advanceTime blockInterval
