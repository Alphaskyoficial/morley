-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module TestSuite.Cleveland.ChainIdGet
  ( test_ChainIdGet
  ) where

import Test.Tasty

import Lorentz ((#), (:!))
import qualified Lorentz as L
import Morley.Tezos.Core (ChainId, dummyChainId)
import Morley.Util.Named ((.!))
import Test.Cleveland
import Test.Cleveland.Tasty

type Storage = ("storage" :! ChainId)

checkChainIdContract :: L.Contract () Storage
checkChainIdContract = L.defaultContract $
  L.drop # L.chainId # L.toNamed #storage # L.nil @L.Operation # L.pair

test_ChainIdGet :: TestTree
test_ChainIdGet =
  clevelandScenarioCaps "ChainId is retrievable" $ do
    helperAddr <- originateSimple "helper" (#storage .! dummyChainId) checkChainIdContract

    chainId <- getChainId
    call helperAddr CallDefault ()
    getStorage helperAddr @@== #storage .! chainId
