-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE OverloadedLists #-}

module TestSuite.Cleveland.CallStack
  ( test_callStack
  ) where

import Lorentz hiding (assert, comment, not)

import Data.Char (isNumber, isSpace)
import qualified Data.List as List
import Servant.Client
  (BaseUrl(BaseUrl), ClientEnv(baseUrl), ClientError(ConnectionError), Scheme(Http))
import System.FilePath ((</>))
import Test.Hspec.Expectations
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Runners (Result(resultDescription))
import Text.RawString.QQ (r)
import Time (sec)
import qualified Unsafe

import Morley.Client (Alias(..), TezosClientError(EConnreset), mceClientEnvL, mceTezosClientL)
import Morley.Client.TezosClient (tceEndpointUrlL)
import Morley.Michelson.Typed (convertContract, untypeValue)
import Morley.Tezos.Address (unsafeParseAddress)
import Test.Cleveland
import Test.Cleveland.Internal.Client (neMorleyClientEnvL)
import Test.Cleveland.Michelson.Integrational
  (ScenarioBranchName(..), ScenarioError(ScenarioError), TestError(CustomTestError))
import Test.Cleveland.Tasty

import TestSuite.Util (idContract, outcomeIsFailure, runViaTastyOnEmulator, runViaTastyOnNetwork)

test_callStack :: TestTree
test_callStack =
  testGroup "Error messages include a helpful callstack" $
    [ testFailureIncludesCallStack "callstack points to runIO"
        [r|
          runIO (throwM DummyException)
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          runIO (throwM DummyException)

    , testFailureIncludesCallStack "callstack points to resolveAddress"
        [r|
          void $ resolveAddress invalidAlias
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by using an unknown alias
          void $ resolveAddress invalidAlias

    , testFailureIncludesCallStack "callstack points to newAddress"
        [r|
            void $ newAddress "b"
                   ^^^^^^^^^^^^^^
        |]
        do
          addr <- newFreshAddress "a"
          -- force a failure by using an address without money as the donator
          withMoneybag addr $
            void $ newAddress "b"

    , testFailureIncludesCallStack "callstack points to signBytes"
        [r|
          void $ signBytes "" invalidAddr
                 ^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ signBytes "" invalidAddr

    , testFailureIncludesCallStack "callstack points to signBinary"
        [r|
          void $ signBinary @ByteString "" invalidAddr
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ signBinary @ByteString "" invalidAddr

    , testFailureIncludesCallStack "callstack points to originateUntyped"
        [r|
          void $ originateUntyped UntypedOriginateData
            { uodName = ""
            , uodBalance = 0
            , uodStorage = untypeValue $ toVal @Natural 3
            , uodContract = convertContract $ toMichelsonContract @() @() idContract
            }
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by using a storage of the wrong type
          void $ originateUntyped UntypedOriginateData
            { uodName = ""
            , uodBalance = 0
            , uodStorage = untypeValue $ toVal @Natural 3
            , uodContract = convertContract $ toMichelsonContract @() @() idContract
            }

    , testFailureIncludesCallStack "callstack points to originateUntypedSimple"
        [r|
          void $ originateUntypedSimple ""
            (untypeValue $ toVal @Natural 3)
            (convertContract $ toMichelsonContract @() @() idContract)
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by using a storage of the wrong type
          void $ originateUntypedSimple ""
            (untypeValue $ toVal @Natural 3)
            (convertContract $ toMichelsonContract @() @() idContract)

    , testFailureIncludesCallStack "callstack points to originate"
        [r|
          void $ originate OriginateData
            { odName = ""
            , odBalance = maxBound
            , odStorage = ()
            , odContract = idContract @() @()
            }
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering `maxBound` mutez
          void $ originate OriginateData
            { odName = ""
            , odBalance = maxBound
            , odStorage = ()
            , odContract = idContract @() @()
            }

    , testFailureIncludesCallStack "callstack points to transfer"
        [r|
          transfer TransferData
            { tdTo = invalidAddr
            , tdAmount = 0
            , tdEntrypoint = DefEpName
            , tdParameter = ()
            }
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering from an unknown alias
          transfer TransferData
            { tdTo = invalidAddr
            , tdAmount = 0
            , tdEntrypoint = DefEpName
            , tdParameter = ()
            }

    , testFailureIncludesCallStack "callstack points to transferMoney"
        [r|
          transferMoney invalidAddr 1
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering from an unknown address
          transferMoney invalidAddr 1
            & withSender invalidAddr

    , testFailureIncludesCallStack "callstack points to call"
        [r|
          call invalidTAddr CallDefault ()
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering to an unknown address
          call invalidTAddr CallDefault ()

    , testFailureIncludesCallStack "callstack points to inBatch for batched transfers"
        [r|
          inBatch $ do
            call (TAddress @() invalidAddr) CallDefault ()
            call (TAddress @() invalidAddr) CallDefault ()
            return ()
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering to an unknown address
          inBatch $ do
            call (TAddress @() invalidAddr) CallDefault ()
            call (TAddress @() invalidAddr) CallDefault ()
            return ()

    , testFailureIncludesCallStack "callstack points to importUntypedContract"
        [r|
          void $ importUntypedContract "<invalid file path>"
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ importUntypedContract "<invalid file path>"

    , testGroup "attempt"
      [ testFailureIncludesCallStack "when action throws an unexpected exception, callstack points to action"
          [r|
                runIO $ throwM DummyException
                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          |]
          do
            void $
              attempt @TransferFailure $
                runIO $ throwM DummyException
      ]

    , testGroup "catchTransferFailure" $
      [ testFailureIncludesCallStack "when action does not throw, callstack points to catchTransferFailure"
          [r|
              catchTransferFailure
                pass
              ^^^^^^^^^^^^^^^^^^^^
          |]
          do
            void $
              catchTransferFailure
                pass
      , testGroup "when action throws an unexpected exception, callstack points to action" $
          let unexpectedExceptions =
                [ ( "DummyException"
                  , SomeException DummyException
                  )
                , ( "unexpected TestError constructor"
                  , SomeException $ ScenarioError (ScenarioBranchName []) (CustomTestError "err")
                  )
                , ( "Servant ClientError"
                  , SomeException $ ConnectionError (SomeException DummyException)
                  )
                , ( "TezosClientError"
                  , SomeException EConnreset
                  )
                ]
          in  flip fmap unexpectedExceptions $ \(testName, SomeException ex) ->
                testFailureIncludesCallStack
                  testName
                  [r|
                      runIO (throwM ex)
                      ^^^^^^^^^^^^^^^^^
                  |]
                  do
                    void $ catchTransferFailure $ do
                      pass
                      runIO (throwM ex)
                      pass
      ]

    , testFailureIncludesCallStack "when exception predicate fails, callstack points to checkTransferFailure"
        [r|
          checkTransferFailure err $ failedWith (constant @Natural 2)
          ^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          addr <- originateSimple "" () contractFailWith1
          err <- catchTransferFailure $ call addr CallDefault ()
          checkTransferFailure err $ failedWith (constant @Natural 2)

    , testGroup "expectTransferFailure" $
      [ testFailureIncludesCallStack "when action does not throw, callstack points to expectTransferFailure"
          [r|
              & expectTransferFailure emptyTransaction
                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          |]
          do
            pass
              & expectTransferFailure emptyTransaction

      , testFailureIncludesCallStack "when action throws an unexpected exception, callstack points to action"
          [r|
            runIO (throwM DummyException)
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          |]
          do
            runIO (throwM DummyException)
              & expectTransferFailure emptyTransaction

      , testFailureIncludesCallStack "when exception predicate fails, callstack points to expectTransferFailure"
          [r|
              & expectTransferFailure (failedWith (constant @Natural 2))
                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          |]
          do
            addr <- originateSimple "" () contractFailWith1
            call addr CallDefault ()
              & expectTransferFailure (failedWith (constant @Natural 2))
      ]

    , testFailureIncludesCallStack "callstack points to expectFailedWith"
        [r|
          expectFailedWith @MText "" pass
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          expectFailedWith @MText "" pass

    , testFailureIncludesCallStack "callstack points to expectError"
        [r|
          expectError @MText "" pass
          ^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          expectError @MText "" pass

    , testFailureIncludesCallStack "callstack points to expectCustomError"
        [r|
          expectCustomError #unitError () pass
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          expectCustomError #unitError () pass

    , testFailureIncludesCallStack "callstack points to expectCustomError_"
        [r|
          expectCustomError_ #unitError pass
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          expectCustomError_ #unitError pass

    , testFailureIncludesCallStack "callstack points to expectCustomErrorNoArg"
        [r|
          expectCustomErrorNoArg #noArgError pass
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          expectCustomErrorNoArg #noArgError pass

    , testFailureIncludesCallStack "callstack points to expectNumericError"
        [r|
          expectNumericError @MText [] "" pass
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          expectNumericError @MText [] "" pass

    , testFailureIncludesCallStack "callstack points to getStorage"
        [r|
          void $ getStorage @() invalidAddr
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ getStorage @() invalidAddr

    , testFailureIncludesCallStackOnEmulator "callstack points to getFullStorage on emulator"
        [r|
          void $ getFullStorage @() invalidAddr
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ getFullStorage @() invalidAddr

    , testFailureIncludesCallStack "callstack points to getStorageExpr"
        [r|
          void $ getStorageExpr invalidAddr
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ getStorageExpr invalidAddr

    , sabotageNetworkEnv $
        testFailureIncludesCallStackOnNetwork "callstack points to getBigMapValueMaybe"
          [r|
            void $ getBigMapValueMaybe @Integer @Integer 999999999999999999 0
                   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          |]
          do
            void $ getBigMapValueMaybe @Integer @Integer 999999999999999999 0

    , testFailureIncludesCallStack "callstack points to getBigMapValue"
        [r|
          void $ getBigMapValue @Integer @Integer 999999999999999999 0
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ getBigMapValue @Integer @Integer 999999999999999999 0

    , testFailureIncludesCallStack "callstack points to getPublicKey"
        [r|
          void $ getPublicKey invalidAddr
                 ^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ getPublicKey invalidAddr

    , sabotageNetworkEnv $
        testFailureIncludesCallStackOnNetwork "callstack points to getChainId on network"
          [r|
            void getChainId
                 ^^^^^^^^^^
          |]
          do
            void getChainId

    , sabotageNetworkEnv $
        testFailureIncludesCallStackOnNetwork "callstack points to advanceTime on network"
          [r|
            advanceTime (sec 1)
            ^^^^^^^^^^^^^^^^^^^
          |]
          do
            advanceTime (sec 1)
    , sabotageNetworkEnv $
        testFailureIncludesCallStackOnNetwork "callstack points to advanceLevel on network"
          [r|
            advanceLevel 1
            ^^^^^^^^^^^^^^
          |]
          do
            advanceLevel 1
    , sabotageNetworkEnv $
        testFailureIncludesCallStackOnNetwork "callstack points to getNow on network"
          [r|
            void getNow
                 ^^^^^^
          |]
          do
            void getNow
    , sabotageNetworkEnv $
        testFailureIncludesCallStackOnNetwork "callstack points to getLevel on network"
          [r|
            void getLevel
                 ^^^^^^^^
          |]
          do
            void getLevel
    , testFailureIncludesCallStackOnEmulator
        "when a branchout branch throws, the callstack points to the function inside the branch"
        [r|
                  getStorage @() invalidAddr
                  ^^^^^^^^^^^^^^^^^^^^^^^^^^
                  | In 'a' branch:
                  | Unknown address provided: tz1fsFpWk691ncq1xwS62dbotECB67B13gfC
        |]
        do
          branchout
            [ "a" ?-
                void $
                  getStorage @() invalidAddr
            ]
    , testFailureIncludesCallStackOnEmulator
        "when a branchout branch throws ANY exception, the exception raised is printed in a right way"
        [r|
                runIO $ throwM DummyException
                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                | In 'a' branch:
                | DummyException

        |]
        do
          branchout
            [ "a" ?-
                runIO $ throwM DummyException
            ]

    , testFailureIncludesCallStackOnEmulator
        "when offshoot throws, the callstack points to the function inside offshoot"
        [r|
              getStorage @() invalidAddr
              ^^^^^^^^^^^^^^^^^^^^^^^^^^
              | In 'a' branch:
              | Unknown address provided: tz1fsFpWk691ncq1xwS62dbotECB67B13gfC

        |]
        do
          offshoot "a" $
            void $
              getStorage @() invalidAddr

    , testFailureIncludesCallStack "callstack points to failure"
        [r|
          failure "a"
          ^^^^^^^^^^^
        |]
        do
          failure "a"

    , testFailureIncludesCallStack "callstack points to assert"
        [r|
          assert False "a"
          ^^^^^^^^^^^^^^^^
        |]
        do
          assert False "a"

    , testFailureIncludesCallStack "callstack points to @=="
        [r|
          1 @== (2 :: Int)
          ^^^^^^^^^^^^^^^^
        |]
        do
          1 @== (2 :: Int)

    , testFailureIncludesCallStack "callstack points to @/="
        [r|
          1 @/= (1 :: Int)
          ^^^^^^^^^^^^^^^^
        |]
        do
          1 @/= (1 :: Int)

    , testFailureIncludesCallStack "callstack points to @@=="
        [r|
          pure 1 @@== (2 :: Int)
          ^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          pure 1 @@== (2 :: Int)

    , testFailureIncludesCallStack "callstack points to @@/="
        [r|
          pure 1 @@/= (1 :: Int)
          ^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          pure 1 @@/= (1 :: Int)

    , testFailureIncludesCallStack "callstack points to checkCompares"
        [r|
          checkCompares @Int 1 (==) 2
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          checkCompares @Int 1 (==) 2

    , testFailureIncludesCallStack "callstack points to checkComparesWith"
        [r|
          checkComparesWith @Int show 1 (==) show 2
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          checkComparesWith @Int show 1 (==) show 2

    , testGroup "callstack points to a method at its top"
        [ testFailureIncludesCallStack' "callstack points to helper using @=="
            [r|
              bulkCheck [0, 1] [0, 2]
              ^^^^^^^^^^^^^^^^^^^^^^^
            |]
            do
              let
                bulkCheck
                  :: (MonadCleveland caps base m, HasCallStack)
                  => [Int] -> [Int] -> m ()
                bulkCheck = sequence_ ... zipWith (@==)

              bulkCheck [0, 1] [0, 2]
        ]
    ]

  where
    invalidAlias = Alias "UnknownAlias"
    invalidAddr = unsafeParseAddress "tz1fsFpWk691ncq1xwS62dbotECB67B13gfC"
    invalidTAddr = TAddress @() invalidAddr

testFailureIncludesCallStack
  :: HasCallStack
  => TestName
  -> String
  -> (forall caps base m. MonadCleveland caps base m => m ())
  -> TestTree
testFailureIncludesCallStack testName =
  testFailureIncludesCallStack'
    testName

-- | Check that exceptions thrown by the given function contain a callstack that points to that function.
--
-- The scenario is run on both the emulator and on a network.
testFailureIncludesCallStack'
  :: HasCallStack
  => TestName
  -> String
  -> (forall caps base m. MonadCleveland caps base m => m ())
  -> TestTree
testFailureIncludesCallStack' testName expectedErrorLines test =
  testGroup testName
    [ testFailureIncludesCallStackOnEmulator
        "On emulator"
        expectedErrorLines
        test
    , testFailureIncludesCallStackOnNetwork
        "On network"
        expectedErrorLines
        test
    ]

testFailureIncludesCallStackOnEmulator
  :: HasCallStack
  => TestName -> String -> (forall m. Monad m => EmulatedT m ()) -> TestTree
testFailureIncludesCallStackOnEmulator testName expectedErrorLines cleveland =
  runViaTastyOnEmulator testName mempty cleveland $ \tastyResult -> do
    outcomeIsFailure tastyResult
    checkErrorMessage (resultDescription tastyResult) expectedErrorLines

testFailureIncludesCallStackOnNetwork
  :: HasCallStack
  => TestName -> String -> (forall m. Monad m => ClevelandT m ()) -> TestTree
testFailureIncludesCallStackOnNetwork testName expectedErrorLines cleveland =
  runViaTastyOnNetwork testName mempty cleveland $ \tastyResult -> do
    outcomeIsFailure tastyResult
    checkErrorMessage (resultDescription tastyResult) expectedErrorLines

-- | If we can't force a function to fail on a network by, e.g.,
-- passing the wrong arguments or violating its pre-conditions,
-- we can use this function to mess with the 'NetworkEnv' config and force
-- the test to crash.
--
-- For example, 'newAddress' and 'getChainId' don't normally fail,
-- but if we mess with the config, they will.
sabotageNetworkEnv :: TestTree -> TestTree
sabotageNetworkEnv =
  modifyNetworkEnv f
  where
    faultyBaseUrl = BaseUrl Http "" 0 ""

    f :: NetworkEnv -> NetworkEnv
    f =
      (neMorleyClientEnvL.mceTezosClientL.tceEndpointUrlL .~ faultyBaseUrl) .
      (neMorleyClientEnvL.mceClientEnvL %~ \clientEnv -> clientEnv
        { baseUrl = faultyBaseUrl }
      )

-- | Checks that an error message includes a pretty-printed callstack,
-- and that it points to this file and contains the expected lines.
checkErrorMessage :: HasCallStack => String -> String -> Assertion
checkErrorMessage err expectedLines = do
  Unsafe.head (List.lines err) `shouldContain` ("━━ test" </> "TestSuite" </> "Cleveland" </> "CallStack.hs ━━━")

  if strippedExpectedLines `List.isInfixOf` strippedErrorLines
    then pass
    else
      assertFailure $
        List.unlines $
          [ "Expected the error message to contain: " ]
          <> strippedExpectedLines
          <> [ "But it didn't. Actual error message was: "]
          <> strippedErrorLines

  where
    stripLineNumber line =
      line
      & List.dropWhile isSpace
      & List.dropWhile isNumber
      & List.dropWhile isSpace
      & List.dropWhile (== '┃')
      & List.drop 1

    -- Strip 1) the header, 2) the callstack entries, 3) the line numbers and 4) the vertical border
    -- from the error message, to make writing these tests easier.
    strippedErrorLines =
      err
      & List.lines
      & List.takeWhile (/= "CallStack (from HasCallStack):")
      & Unsafe.tail
      <&> stripLineNumber

    -- The strings quoted with [r||] in this module contain a leading and trailing empty lines,
    -- so we need to remove them here.
    strippedExpectedLines = Unsafe.init $ Unsafe.tail $ List.lines expectedLines

----------------------------------------------------------------------------
-- Test data
----------------------------------------------------------------------------

type instance ErrorArg "unitError" = UnitErrorArg

instance CustomErrorHasDoc "unitError" where
  customErrClass = ErrClassActionException
  customErrDocMdCause = "Error for testing custom error handling in cleveland"

type instance ErrorArg "noArgError" = NoErrorArg

instance CustomErrorHasDoc "noArgError" where
  customErrClass = ErrClassActionException
  customErrDocMdCause = "Error for testing custom error handling in cleveland"

contractFailWith1 :: Contract () ()
contractFailWith1 = defaultContract $
  push @Natural 1 # failWith

data DummyException = DummyException
  deriving stock (Eq, Show)

instance Exception DummyException where
