module TestSuite.Cleveland.RefillableAddress
  ( test_AddressRefills
  , test_AddressDoesntRefill
  ) where

import Test.Tasty

import Test.Cleveland
import Test.Cleveland.Tasty

import Test.Util.Contracts
import TestSuite.Util (shouldFailWithMessage)

test_AddressRefills :: [TestTree]
test_AddressRefills =
  [ clevelandScenarioCaps "A refillable address refills when transfer amount > balance" $ do
      refillable <- newRefillableAddress "refillable"
      receiver <- newFreshAddress auto

      balanceSender <- getBalance refillable

      withSender refillable do
        transferMoney receiver (balanceSender + 1000) -- obviously more than sender has

      getBalance receiver @@== (balanceSender + 1000)
  , clevelandScenarioCaps "A refillable address refills when transfer amount == balance - 1 μtz" $ do
      refillable <- newRefillableAddress "refillable"
      receiver <- newFreshAddress auto

      balanceSender <- getBalance refillable

      withSender refillable do
        transferMoney receiver (balanceSender - 1) -- also would fail without auto-refill, due to fees
        -- NOTE: the test crashes currently when transfer amount = balance; it seems like a
        -- bug in the local chain code. Theoretically, this test should work either way.

      getBalance receiver @@== (balanceSender - 1)
  , clevelandScenarioCaps "A refillable address refills during contract origination" $ do
      refillable <- newRefillableAddress "refillable"
      soBigContract <- importContract @() @() $ contractsDir </> "so_big.tz"
      -- storage burn should be at least >= 1 XTZ due to the contract size
      -- however, since 'refillable' has at least 0.5 XTZ after 'newRefillableAddress'
      -- we also transfer 0.5 XTZ to the contract; this ensures we're overbudget.
      void $ withSender refillable $ originate $ OriginateData
        { odName = "so_big"
        , odBalance = 0.5e6
        , odContract = soBigContract
        , odStorage = ()
        }
  ]

test_AddressDoesntRefill :: [TestTree]
test_AddressDoesntRefill =
  [ emulatedScenarioCaps "A non-refillable address doesn't refill" (nonRefillableScenario emulatedError)
  , networkScenarioCaps "A non-refillable address doesn't refill" (nonRefillableScenario networkError)
  ]
  where
  emulatedError = "doesn't have enough funds"
  networkError = "too low"

nonRefillableScenario :: MonadCleveland caps base m => String -> m ()
nonRefillableScenario errorMsg = do
  nonRefillable <- newFreshAddress auto
  receiver <- newFreshAddress auto

  -- needed to reveal `nonRefillable`
  transferMoney nonRefillable 1

  shouldFailWithMessage errorMsg $
    withSender nonRefillable $ transferMoney receiver 100
