-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Morley.Michelson.Printer
  ( RenderDoc(..)
  , renderAnyBuildable
  , printDoc
  , printUntypedContract
  , printTypedContractCode
  , printTypedContract
  , printSomeContract
  , printTypedValue
  , printUntypedValue
  ) where

import Data.Constraint (withDict)
import Data.Singletons (SingI)
import qualified Data.Text.Lazy as TL

import Morley.Michelson.Printer.Util (RenderDoc(..), doesntNeedParens, printDoc, renderAnyBuildable)
import qualified Morley.Michelson.Typed as T
import qualified Morley.Michelson.Untyped as U

-- | Convert an untyped contract into a textual representation which
-- will be accepted by the OCaml reference client: tezos-client.
printUntypedContract :: (RenderDoc op) => Bool -> U.Contract' op -> TL.Text
printUntypedContract forceSingleLine = printDoc forceSingleLine . renderDoc doesntNeedParens

-- | Convert a typed contract into a textual representation which
-- will be accepted by the OCaml reference client: tezos-client.
printTypedContractCode :: (SingI p, SingI s) => Bool -> T.ContractCode p s -> TL.Text
printTypedContractCode forceSingleLine =
  printUntypedContract forceSingleLine . T.convertContractCode

-- | Convert typed contract into a textual representation which
-- will be accepted by the OCaml reference client: tezos-client.
printTypedContract :: Bool -> T.Contract p s -> TL.Text
printTypedContract forceSingleLine fc@T.Contract{} =
  printUntypedContract forceSingleLine $ T.convertContract fc

-- | Convert typed value into a textual representation which
-- will be accepted by the OCaml reference client: tezos-client.
printTypedValue
  :: forall t.
      (T.ProperUntypedValBetterErrors t)
  => Bool -> T.Value t -> TL.Text
printTypedValue forceSingleLine =
  withDict (T.properUntypedValEvi @t) $
  printUntypedValue forceSingleLine . T.untypeValue

-- | Convert untyped value into a textual representation which
-- will be accepted by the OCaml reference client: tezos-client.
printUntypedValue :: (RenderDoc op) => Bool -> U.Value' op -> TL.Text
printUntypedValue forceSingleLine =
  printDoc forceSingleLine . renderDoc doesntNeedParens

-- | Convert 'T.SomeContract' into a textual representation which
-- will be accepted by the OCaml reference client: tezos-client.
printSomeContract :: Bool -> T.SomeContract -> TL.Text
printSomeContract forceSingleLine (T.SomeContract fc) =
  printTypedContract forceSingleLine fc
