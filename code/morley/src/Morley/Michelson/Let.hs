-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Morley.Michelson.Let
  ( LetType (..)
  , LetValue (..)
  ) where

import Data.Aeson.TH (deriveJSON)
import qualified Data.Text as T

import Morley.Michelson.Macro (ParsedOp)
import Morley.Michelson.Untyped (Ty, Value')
import Morley.Util.Aeson

-- | A programmer-defined constant
data LetValue = LetValue
  { lvName :: T.Text
  , lvSig :: Ty
  , lvVal :: (Value' ParsedOp)
  } deriving stock (Eq, Show)

-- | A programmer-defined type-synonym
data LetType = LetType
  { ltName :: T.Text
  , ltSig :: Ty
  } deriving stock (Eq, Show)

deriveJSON morleyAesonOptions ''LetValue
deriveJSON morleyAesonOptions ''LetType
