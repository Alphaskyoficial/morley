-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Measuring operation size of typed stuff.
module Morley.Michelson.Typed.OpSize
  ( OpSize (..)
  , U.opSizeHardLimit
  , U.smallTransferOpSize

  , instrOpSize
  , contractOpSize
  , valueOpSize
  ) where

import Morley.Michelson.Typed.Aliases
import Morley.Michelson.Typed.Convert
import Morley.Michelson.Typed.Instr
import Morley.Michelson.Typed.Scope
import Morley.Michelson.Untyped (OpSize)
import qualified Morley.Michelson.Untyped as U

-- | Estimate instruction operation size.
instrOpSize :: Instr inp out -> OpSize
instrOpSize = U.expandedInstrsOpSize . instrToOps

-- | Estimate contract code operation size.
contractOpSize :: Contract cp st -> OpSize
contractOpSize = instrOpSize . cCode

-- | Estimate value operation size.
valueOpSize :: (UntypedValScope t) => Value t -> OpSize
valueOpSize = U.valueOpSize . untypeValue
