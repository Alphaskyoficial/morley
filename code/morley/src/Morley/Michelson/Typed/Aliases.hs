-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Morley.Michelson.Typed.Aliases
  ( Value
  , Operation
  ) where

import Morley.Michelson.Typed.Instr
import Morley.Michelson.Typed.Value

type Value = Value' Instr
type Operation = Operation' Instr
