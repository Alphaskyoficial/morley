-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Morley.Util.TypeTuple.Class
  ( RecFromTuple (..)
  ) where

-- | Building a record from tuple.
--
-- It differs from similar typeclass in "Data.Vinyl.FromTuple" module in that
-- it allows type inference outside-in - knowing desired 'Type' you know which
-- tuple should be provided - this improves error messages when constructing
-- concrete 'Type' objects.
class RecFromTuple r where
  type IsoRecTuple r :: Type
  recFromTuple :: IsoRecTuple r -> r
