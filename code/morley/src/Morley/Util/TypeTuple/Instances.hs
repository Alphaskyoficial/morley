-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-orphans #-}

module Morley.Util.TypeTuple.Instances () where

import Data.Vinyl.Core (Rec(..))

import Morley.Util.TypeTuple.Class
import Morley.Util.TypeTuple.TH

concatMapM deriveRecFromTuple (0 : [2..25])
-- ↑ We skip 1-ary tuple because it is GHC.Tuple.Unit, and we don't want it.

instance RecFromTuple (Rec f '[a]) where
  type IsoRecTuple (Rec f '[a]) = f a
  recFromTuple a = a :& RNil
