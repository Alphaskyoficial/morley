-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Utilities for numbers.
module Morley.Util.Num
  ( fromIntegralChecked
  ) where

-- | Convert between integral types, checking for overflows/underflows.
fromIntegralChecked :: (Integral a, Integral b) => a -> Either Text b
fromIntegralChecked a =
  let b = fromIntegral a
  in if toInteger a == toInteger b
    then Right b
    else Left
      if toInteger a > toInteger b
        then "Numeric overflow"
        else "Numeric underflow"
