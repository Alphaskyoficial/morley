-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Conversions between tuples and list-like types.
module Morley.Util.TypeTuple
  ( RecFromTuple (..)
  ) where

import Morley.Util.TypeTuple.Class
import Morley.Util.TypeTuple.Instances ()
