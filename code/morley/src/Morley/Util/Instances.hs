-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Missing instances from libraries.
module Morley.Util.Instances () where

import Data.Default (Default(..))
import Fmt (Buildable(..))

instance Default Natural where
  def = 0

instance Buildable Natural where
  build = build @Integer . fromIntegral

instance Buildable a => (Buildable (Identity a)) where
  build (Identity x) = build x
