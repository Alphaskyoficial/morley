# Morley: Developer tools for the Michelson Language

[![Hackage](https://img.shields.io/hackage/v/morley.svg)](https://hackage.haskell.org/package/morley)

Morley aims to make writing smart contracts in Michelson pleasant and effective.
It contains 3 major things:
1. An [executable](https://gitlab.com/morley-framework/morley/-/tree/master/code/morley#morley-executable) that lets you perform various operations on Michelson smart contracts.
2. A [library](https://gitlab.com/morley-framework/morley/-/tree/master/code/morley#morley-library) with core Tezos and Michelson data types, and functions such as Michelson typechecker and interpreter.
3. A superset of the Michelson language that we call the [Morley language](https://gitlab.com/morley-framework/morley/-/tree/master/code/morley#morley-language).
It relaxes Michelson parser rules, adds some syntax sugar and simple features.

## Morley executable

### How to install

There are three ways to get Morley executable:
- [Docker](https://docs.docker.com/) based (preferable).
  * Get [script](https://gitlab.com/morley-framework/morley/-/blob/master/scripts/morley.sh)
 (e. g. using `curl https://gitlab.com/morley-framework/morley/raw/master/scripts/morley.sh > morley.sh`)
  and run it `./morley.sh <args>`. This script will pull a docker image that contains the latest version of Morley executable from the `production` branch and run it with the given arguments.
    You'll need `coreutils` to be installed in order to use `morley.sh`.
  * Usage example:
    + `./morley.sh` to see help message
    + `./morley.sh run --contract add1.tz --storage 1 --parameter 1 --amount 1`
- [Stack](https://docs.haskellstack.org/en/stable/README/) based.
  * Clone this git repository and run `stack build` command,
    after that you can do `stack exec -- morley <args>` to run morley executable built from the source code.
  * Usage example:
    + `stack exec -- morley --help` to see help message
    + `stack exec -- morley originate --contract contracts/tezos_examples/attic/add1.tz --storage 1 --verbose`
- [Cabal](https://www.haskell.org/cabal/) based.
  * Clone this git repository, go to this directory and run `cabal new-update && cabal new-build` command,
    after that you can do `cabal new-run -- morley <args>` to run morley executable built from the source code.
  * Usage example:
    + `cabal new-run -- morley --help` to see help message
    + `cabal new-run -- morley originate --contract contracts/tezos_examples/attic/add1.tz --storage 1 --verbose`

### Usage

Morley executable does not interact with Tezos node and Tezos network.
It works in _emulated environment_ which is stored in a simple JSON file on disk.
The following commands depend on environment and may modify it:
- `originate` a contract.
- `transfer` tokens to a given address (and call a smart contract if it's the destination).
- `run` a contract. A given contract is being originated first, and then the transaction is being sent to it.

The following commands don't depend on environment:
- `optimize` a contract by replacing certain sequences of instructions with equivalent but more efficient ones.
- `typecheck` a contract.
- `repl` starts a REPL where you can type Michelson instructions and interpret them.
- `analyze` a contract and print some statistics about it.
- `print` a contract in vanilla Michelson format.
It can be useful in some cases:
  + You have a smart contract that uses [Morley extensions](https://gitlab.com/morley-framework/morley/-/tree/master/code/morley#morley-language) and want to convert it to vanilla Michelson format that can be handled by Tezos reference implementation.
  + You have a contract with inconsistent/ugly formatting and want to format it in uniform style.
  + You want to print a contract on a single line.
- `parse` a contract and return its representation in Haskell types.

You can get more info about these commands by running `morley <command> --help`.
Run `morley --help` to get the list of all commands.

## Morley library

Morley-the library is available on [Hackage](https://hackage.haskell.org/package/morley).
It consists of the following parts:

- The `Morley.Tezos.*` hierarchy ([`Morley.Tezos.Address`](http://hackage.haskell.org/package/morley/docs/Tezos-Address.html), [`Morley.Tezos.Core`](http://hackage.haskell.org/package/morley/docs/Tezos-Core.html), [`Morley.Tezos.Crypto`](http://hackage.haskell.org/package/morley/docs/Tezos-Crypto.html)) is designed to implement cryptographic primitives, string and byte formats, and any other functionality specific to the Tezos protocol which is required for testing/execution of Michelson contracts but is used not only by Michelson.
- [`Morley.Michelson.Untyped`](http://hackage.haskell.org/package/morley/docs/Michelson-Untyped.html) and [`Morley.Michelson.Typed`](http://hackage.haskell.org/package/morley/docs/Michelson-Typed.html) hierarchies define Haskell data types that assemble a Michelson contract. See [michelsonTypes.md](https://gitlab.com/morley-framework/morley/-/blob/master/code/morley/docs/michelsonTypes.md).
- [`Morley.Michelson.TypeCheck`](http://hackage.haskell.org/package/morley/docs/Michelson-TypeCheck.html): A typechecker that validates Michelson contracts according to the Michelson's typing rules. Essentially, it performs conversion from untyped representation to the typed one. See [morleyTypechecker.md](https://gitlab.com/morley-framework/morley/-/blob/master/code/morley/docs/morleyTypechecker.md).
- [`Morley.Michelson.Interpret`](http://hackage.haskell.org/package/morley/docs/Michelson-Interpret.html): An interpreter for Michelson contracts which doesn't perform any side effects. See [morleyInterpreter.md](https://gitlab.com/morley-framework/morley/-/blob/master/code/morley/docs/morleyInterpreter.md).
- [`Morley.Michelson.Macro`](http://hackage.haskell.org/package/morley/docs/Michelson-Macro.html) Types for macros, syntactic sugar, and other extensions that are described in the next chapter.
- [`Morley.Michelson.Parser`](http://hackage.haskell.org/package/morley/docs/Michelson-Parser.html) A parser to turn a `.tz` or `.mtz` file (`.mtz` is a Michelson contract with Morley extensions) into a Haskell ADT.
- [`Morley.Michelson.Runtime`](http://hackage.haskell.org/package/morley/docs/Michelson-Runtime.html): A high-level interface to Morley functionality, see [morleyRuntime.md](https://gitlab.com/morley-framework/morley/-/blob/master/code/morley/docs/morleyRuntime.md).
- The `Morley.Micheline.*` hierarchy ([`Morley.Micheline.Binary`](http://hackage.haskell.org/package/morley/docs/Morley-Micheline-Binary.html), [`Morley.Micheline.Class`](http://hackage.haskell.org/package/morley/docs/Morley-Micheline-Class.html),
[`Morley.Micheline.Expression`](http://hackage.haskell.org/package/morley/docs/Morley-Micheline-Expression.html), [`Morley.Micheline.Json`](http://hackage.haskell.org/package/morley/docs/Morley-Micheline-Json.html))
contains the representation of Micheline `Expression`s, conversion to/from Michelson values,
as well as encoding/decoding to/from JSON and binary format.
- The `Morley.Util.*` hierarchy defines various `Morley`-related utilities that are used all over the project.

## Morley language

Morley-the-language is a superset of the Michelson language, which means that each Michelson contract is also a valid Morley contract but not vice versa.
There are several extensions which make it more convenient to write Michelson contracts and test them.
For example, one can write inline assertions in their contracts for testing.
All the details can be found in [the document](https://gitlab.com/morley-framework/morley/-/blob/master/code/morley/docs/language/morleyLanguage.md) about these extensions.
A smart contract written in the Morley language can be transpiled to Michelson using the aforementioned `morley print` command.
