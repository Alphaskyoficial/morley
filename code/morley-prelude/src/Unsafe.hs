-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Unsafe utilities.
--
-- This module should be imported qualified.
module Unsafe
       ( module Universum.Unsafe
       ) where

import Universum.Unsafe
