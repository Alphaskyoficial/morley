-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | This module essentially replaces the default "Prelude" with "Universum".
--
-- It works because we are using the @base-noprelude@ package instead of @base@.

module Prelude
  ( module Control.Lens
  , module Universum
  , for
  -- * Overloaded boolean operators
  , module Boolean
  ) where

import Control.Lens
  (Lens, Lens', Traversal, Traversal', _1, _2, _3, _4, _5, over, preuse, preview, set, use, view,
  (%~), (&), (.~), (<&>), (^.), (^..), (^?))
import Data.Traversable (for)
import Morley.Prelude.Boolean as Boolean
import Universum hiding
  (Key, Lens, Lens', Nat, Traversal, Traversal', Val, _1, _2, _3, _4, _5, over, preuse, preview,
  readFile, set, use, view, writeFile, (%~), (&&), (&), (.~), (<&>), (^.), (^..), (^?), (||))
