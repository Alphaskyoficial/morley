Unreleased
==========
<!-- Append new entries here -->

* [!945](https://gitlab.com/morley-framework/morley/-/merge_requests/945)
  + Bump Stackage LTS version from 17.9 to 18.10.

0.4.2
=====
* [!867](https://gitlab.com/morley-framework/morley/-/merge_requests/867)
  + Hid Prelude's `&&` and `||` operators
  + Exported `Boolean` typeclass and polymorphic `&&` and `||` operators.

0.4.1
=====
* [!861](https://gitlab.com/morley-framework/morley/-/merge_requests/861)
  + Removed the re-export of some basic `microlens` operators from `universum`
    in favor of the ones from `lens` with the same name.
* [!779](https://gitlab.com/morley-framework/morley/-/merge_requests/779)
  + Export `for`.

0.4.0
=====

* [!814](https://gitlab.com/morley-framework/morley/-/merge_requests/814)
  + Reverted hiding `Type` from export list.
* [!781](https://gitlab.com/morley-framework/morley/-/merge_requests/781)
  + Replaced mixins and dependency on `base` with `base-noprelude`.

* Change the license to MIT.

0.3.0
=====

* Hide `Nat` export.

0.2.0.1
=======

* Update maintainer.

0.2.0
======

* Hide `readFile` and `writeFile`.
* Add `Unsafe` module which re-exports `Universum.Unsafe`.

0.1.0.4
=====

Initial release.
Re-exports `universum`.
