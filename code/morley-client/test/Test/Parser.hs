-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Parser
  ( test_bakerFeeParser
  , test_secretKeyEncriptionParser
  ) where

import Test.Hspec.Expectations (shouldBe, shouldSatisfy)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Morley.Client.TezosClient.Parser
import Morley.Client.TezosClient.Types (SecretKeyEncryption(..))
import Morley.Micheline
import Morley.Tezos.Core

test_bakerFeeParser :: TestTree
test_bakerFeeParser = testGroup "tezos-client baker fee parser"
  [ testCase "resulted fee is multiplied by 1e6" $
    parseBakerFeeFromOutput "Fee to the baker: ꜩ0.056008\n" 1 `shouldBe`
    Right [TezosMutez $ toMutez 56008]
  , testCase "multiples fees can be parsed" $
    parseBakerFeeFromOutput "Fee to the baker: ꜩ0.056008\n Fee to the baker: ꜩ0.056008\n" 2 `shouldBe`
    Right (replicate 2 $ TezosMutez $ toMutez 56008)
  , testCase "no valid baker fee in the output" $
    parseBakerFeeFromOutput "Notfee to the baker: ꜩ0.056008\n" 1 `shouldSatisfy`
    isLeft
  ]

test_secretKeyEncriptionParser :: TestTree
test_secretKeyEncriptionParser = testGroup "tezos-client baker fee parser"
  [ testCase "unencrypted type can be parsed" $
    parseSecretKeyEncryption
    "Secret Key: unencrypted:edsk4TybjbpfhcQ81R5FnxkoZy14ZyXRUzfbXrmPgqKRpcGPJanAgY" `shouldBe`
    Right UnencryptedKey
  , testCase "encrypted type can be parsed" $
    parseSecretKeyEncryption
    "Secret Key: encrypted:edesk1a9cTRUMXf6L5cZXMqojELRRfTEoLXc4JzT9B3PqnhWweJnuWgcSk93NBHXYGFxf1uKHFPihBgVzJNwUVHR"
    `shouldBe`
    Right EncryptedKey
  , testCase "ledger type can be parsed" $
    parseSecretKeyEncryption
    "Secret Key: ledger://live-fossa-eager-walrus/bip25519/0h/0h" `shouldBe`
    Right LedgerKey
  , testCase "nonsense cannot be parsed 1" $
    parseSecretKeyEncryption
    "Secret Key: kek://live-fossa-eager-walrus/bip25519/0h/0h" `shouldSatisfy`
    isLeft
  , testCase "nonsense cannot be parsed 2" $
    parseSecretKeyEncryption
    "Not a secret Key: ledger://live-fossa-eager-walrus/bip25519/0h/0h" `shouldSatisfy`
    isLeft
  ]
