-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.KeyRevealing
  ( test_keyRevealing
  ) where

import Test.HUnit (Assertion, assertFailure)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import qualified Lorentz as L
import Morley.Client
import Morley.Michelson.Runtime.GState (genesisAddress1)
import Morley.Michelson.Typed
import Morley.Tezos.Core (toMutez)
import Test.Util
import TestM

mockState :: MockState
mockState = defaultMockState
  { msContracts = one ( genesisAddress1
                      , dumbImplicitContractState
                          { csContractData = ImplicitContractData $ Just dumbManagerKey }
                      )
  }

test_keyRevealing :: TestTree
test_keyRevealing = testGroup "Mock test key revealing"
  [ testCase "Manager key for new address is revealed only once for transfer" $ handleSuccess $
    runMockTest chainOperationHandlers mockState $ do
      senderAddress <- genKey "sender"
      dummyTransfer genesisAddress1 senderAddress
      mbManagerKey <- getManagerKey senderAddress

      when (isJust mbManagerKey) $
        fail "Manager key was expected not to be revealed, but it's revealed."

      dummyTransfer senderAddress genesisAddress1
      mbManagerKey' <- getManagerKey senderAddress

      when (isNothing mbManagerKey') $
        fail "Manager key was expected to be revealed, but it's not revealed."

      local
        (const noRevealHandlers)
        (dummyTransfer senderAddress genesisAddress1)

  , testCase "Manager key for new address is revealed only once for origination" $ handleSuccess $
    runMockTest chainOperationHandlers mockState $ do
      originatorAddress <- genKey "originator"
      dummyTransfer genesisAddress1 originatorAddress

      mbManagerKey <- getManagerKey originatorAddress

      when (isJust mbManagerKey) $
        fail "Manager key was expected not to be revealed, but it's revealed."

      originateDummy originatorAddress

      mbManagerKey' <- getManagerKey originatorAddress
      when (isNothing mbManagerKey') $
        fail "Manager key was expected to be revealed, but it's not revealed."

      local
        (const noRevealHandlers)
        (originateDummy originatorAddress)

  , testCase "Transfer from contract fails with proper error message, without details about revealing" $
    (runMockTest chainOperationHandlers mockState $ do
      (_, addr) <- originateDummy genesisAddress1
      dummyTransfer addr genesisAddress1)
    &
    \case
      (Left err) ->
        case fromException @TezosClientError err of
          Just (ContractSender _ "transfer") -> pass
          _ -> assertFailure $ "Test failed with unexpected error: " <> displayException err
      (Right _)  -> assertFailure "Test expected to fail, but it passed"
  ]
  where
    dummyTransfer from to =
      void $ transfer from to (toMutez 10) DefEpName (toVal ()) Nothing

    originateDummy addr =
      lOriginateContract True "dummy" (AddressResolved addr) (toMutez 10)
      dumbLorentzContract () Nothing

-- | Handlers which don't allow to reveal key.
noRevealHandlers :: (Monad m) => TestHandlers m
noRevealHandlers = TestHandlers $ chainOperationHandlers
  { hRevealKey = \_ _ -> throwM $ UnexpectedClientCall "revealKey" }

dumbLorentzContract :: L.Contract Integer ()
dumbLorentzContract = L.defaultContract $
  L.drop L.# L.unit L.# L.nil L.# L.pair

handleSuccess :: Either SomeException a -> Assertion
handleSuccess (Left err) = assertFailure $ displayException err
handleSuccess (Right _)  = pass
