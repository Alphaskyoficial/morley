-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Morley client that connects with real Tezos network through RPC and tezos-client binary.
-- For more information please refer to README.
module Morley.Client
  ( -- * Command line parser
    parserInfo
  , clientConfigParser

  -- * Full client monad and environment
  , MorleyClientM
  , MorleyClientConfig (..)
  , MorleyClientEnv' (..)
  , MorleyClientEnv
  , runMorleyClientM
  , mkMorleyClientEnv
  -- ** Lens
  , mceTezosClientL
  , mceLogActionL
  , mceSecretKeyL
  , mceClientEnvL

  -- * Only-RPC client monad and environment
  , MorleyOnlyRpcM (..)
  , MorleyOnlyRpcEnv (..)
  , mkMorleyOnlyRpcEnv
  , runMorleyOnlyRpcM

  -- * High-level actions
  , module Morley.Client.Action

  -- * RPC
  , BlockId (..)
  , HasTezosRpc (..)
  , getContract
  , getImplicitContractCounter
  , getContractStorage
  , getBigMapValue
  , getHeadBlock
  , getCounter
  , getProtocolParameters
  , runOperation
  , preApplyOperations
  , forgeOperation
  , getContractScript
  , getContractBigMap
  , getBalance
  , runCode
  , getManagerKey

  -- ** Errors
  , ClientRpcError (..)
  , UnexpectedErrors (..)
  , IncorrectRpcResponse (..)
  , RunError (..)
  -- ** Getters
  , ValueDecodeFailure (..)
  , ValueNotFound (..)
  , readAllBigMapValues
  , readAllBigMapValuesMaybe
  , readContractBigMapValue
  , readBigMapValueMaybe
  , readBigMapValue
  -- ** AsRPC
  , AsRPC
  , deriveRPC
  , deriveRPCWithStrategy
  , deriveManyRPC
  , deriveManyRPCWithStrategy

  -- * @tezos-client@
  , Alias (..)
  , AliasHint (..)
  , AddressOrAlias (..)
  , addressResolved
  , HasTezosClient (..)
  , resolveAddress
  , TezosClientError (..)

  -- * Util
  , disableAlphanetWarning

  -- * Reexports
  , Opt.ParserInfo -- Needed for tests
  ) where

import qualified Options.Applicative as Opt

import Morley.Client.Action
import Morley.Client.Env
import Morley.Client.Full
import Morley.Client.Init
import Morley.Client.OnlyRPC
import Morley.Client.Parser
import Morley.Client.RPC
import Morley.Client.RPC.AsRPC
import Morley.Client.TezosClient
import Morley.Client.Util
