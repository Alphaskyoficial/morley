-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | An abstraction layer over RPC implementation.
-- The primary reason it exists is to make it possible to mock
-- RPC in tests.

module Morley.Client.RPC.Class
  ( HasTezosRpc (..)
  ) where

import Morley.Tezos.Address
import Morley.Tezos.Core (ChainId, Mutez)
import Morley.Tezos.Crypto (PublicKey(..))
import Morley.Util.ByteString

import Morley.Client.RPC.Types
import Morley.Micheline (Expression, TezosInt64)

-- | Type class that provides interaction with tezos node via RPC
class (Monad m, MonadCatch m) => HasTezosRpc m where
  getBlockHash :: BlockId -> m Text
  -- ^ Get hash of the given 'BlockId', mostly used to get hash of
  -- 'HeadId'
  getCounterAtBlock :: BlockId -> Address -> m TezosInt64
  -- ^ Get address counter, which is required for both transaction sending
  -- and contract origination.
  getBlockHeader :: BlockId -> m BlockHeader
  -- ^ Get the whole header of a block.
  getBlockConstants :: BlockId -> m BlockConstants
  -- ^ Get block constants that are required by other RPC calls.
  getBlockOperations :: BlockId -> m [[BlockOperation]]
  -- ^ Get all operations from the block with specified ID.
  getProtocolParametersAtBlock :: BlockId -> m ProtocolParameters
  -- ^ Get protocol parameters that are for limits calculations.
  runOperationAtBlock :: BlockId -> RunOperation -> m RunOperationResult
  -- ^ Perform operation run, this operation doesn't require proper signing.
  -- As a result it returns burned gas and storage diff (also list of originated
  -- contracts but their addresses are incorrect due to the fact that operation
  -- could be not signed properly) or indicates about operation failure.
  preApplyOperationsAtBlock :: BlockId -> [PreApplyOperation] -> m [RunOperationResult]
  -- ^ Preapply list of operations, each operation has to be signed with sender
  -- secret key. As a result it returns list of results each of which has information
  -- about burned gas, storage diff size and originated contracts.
  forgeOperationAtBlock :: BlockId -> ForgeOperation -> m HexJSONByteString
  -- ^ Forge operation in order to receive its hexadecimal representation.
  injectOperation :: HexJSONByteString -> m OperationHash
  -- ^ Inject operation, note that this operation has to be signed before
  -- injection. As a result it returns operation hash.
  getContractScriptAtBlock :: BlockId -> Address -> m OriginationScript
  -- ^ Get code and storage of the desired contract. Note that both code and storage
  -- are presented in low-level Micheline representation.
  -- If the storage contains a @big_map@, then the expression will contain the @big_map@'s ID,
  -- not its contents.
  getContractStorageAtBlock :: BlockId -> Address -> m Expression
  -- ^ Get storage of the desired contract at some block. Note that storage
  -- is presented in low-level Micheline representation.
  -- If the storage contains a @big_map@, then the expression will contain the @big_map@'s ID,
  -- not its contents.
  getContractBigMapAtBlock :: BlockId -> Address -> GetBigMap -> m GetBigMapResult
  -- ^ Get big map value by contract address.
  getBigMapValueAtBlock :: BlockId -> Natural -> Text -> m Expression
  -- ^ Get big map value at some block by the big map's ID and the hashed entry key.
  getBigMapValuesAtBlock :: BlockId -> Natural -> Maybe Natural -> Maybe Natural -> m Expression
  -- ^ Get all big map values at some block by the big map's ID and the optional offset and length.
  getBalanceAtBlock :: BlockId -> Address -> m Mutez
  -- ^ Get balance for given address.
  runCodeAtBlock :: BlockId -> RunCode -> m RunCodeResult
  -- ^ Emulate contract call. This RPC endpoint does the same as
  -- @tezos-client run script@ command does.
  getChainId :: m ChainId
  -- ^ Get current @ChainId@
  getManagerKeyAtBlock :: BlockId -> Address -> m (Maybe PublicKey)
  -- ^ Get manager key for given address.
  -- Returns @Nothing@ if this key wasn't revealed.
