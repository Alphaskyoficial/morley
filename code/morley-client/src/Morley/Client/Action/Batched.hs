-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Primitives for running batched operations with a neat interface.
module Morley.Client.Action.Batched
  ( OperationsBatch (..)
  , originateContractM
  , runTransactionM
  , runOperationsBatch
  ) where

import Fmt (Buildable(..))

import Morley.Client.Action.Common
import Morley.Client.Action.Operation
import Morley.Client.Logging
import Morley.Client.RPC.Class
import Morley.Client.RPC.Types
import Morley.Client.TezosClient
import Morley.Tezos.Address
import Morley.Util.Batching

{- | Where the batched operations occur.

Example:

@
runOperationsBatch mySender $ do
  addr <- originateContractM ...
  runTransactionM ...
  return addr
@

Note that this is not a 'Monad', rather an 'Applicative' - use
@-XApplicativeDo@ extension for nicer experience.
-}
newtype OperationsBatch a = OperationsBatch
  { unOperationsBatch
      :: BatchingM
          (Either TransactionData OriginationData)
          (Either () Address)
          BatchedOperationError
          a
  } deriving newtype (Functor, Applicative)

data BatchedOperationError
  = UnexpectedOperationResult

instance Buildable BatchedOperationError where
  build = \case
    UnexpectedOperationResult ->
      "Got unexpected operation type within result of batched operation"

-- | Perform transaction within a batch.
runTransactionM :: TransactionData -> OperationsBatch ()
runTransactionM td = OperationsBatch $
  Left td `submitThenParse` \case
    Left () -> pass
    Right _ -> Left UnexpectedOperationResult

-- | Perform origination within a batch.
originateContractM :: OriginationData -> OperationsBatch Address
originateContractM od = OperationsBatch $
  Right od `submitThenParse` \case
    Left _ -> Left UnexpectedOperationResult
    Right addr -> return addr

-- | Execute a batch.
runOperationsBatch
  :: ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => AddressOrAlias
  -> OperationsBatch a
  -> m (Maybe OperationHash, a)
runOperationsBatch sender (OperationsBatch batch) =
  unsafeRunBatching (runOperations sender) batch
