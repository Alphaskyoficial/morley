# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

.PHONY: all test-all stylish lint clean bench \
	test-binaries-bats \
	test-docker-bats \
	test-encoding-bats \
	test-morley-client-bats \
	clean-bats-tmp

# Build target from the common utility Makefile
MAKEU = $(MAKE) -f code/make/Makefile

ifeq (${MORLEY_USE_CABAL},1)
	build_all = cabal new-build --enable-tests --enable-benchmarks -O0 all
	test_all = cabal new-test -O0 all
	clean_all = cabal new-clean
	bench_all = cabal new-bench
	get_path = dirname $$(cabal list-bin $(1):exe:$(1))
else
	test_all = $(MAKEU) test PACKAGE=""
	build_all = $(MAKEU) PACKAGE=""
	clean_all = stack clean
	bench_all = stack bench
	get_path = dirname $$(stack exec scripts/which.sh $(1))
endif

# For bats test environment
NODE_ENDPOINT ?= https://granada.testnet.tezos.serokell.team
# Note that testnet moneybag can run out of tz. If this happened, someone should transfer it some
# more tz, its address: tz1Nbp1gNPMzn9MB9ZhBnfsajsaQBLYScdd5
MONEYBAG_SECRET_KEY ?= unencrypted:edsk48WyaFHvsqLYQBrCBiRUdnPa7VduZbePo5UAbyMJRN8Eob5PvL

# Set to anything non-empty to keep temp files
BATS_KEEP_TEMP_FILES ?=

ifeq ($(BATS_KEEP_TEMP_FILES),)
	clean_bats_tmp = $(MAKE) clean-bats-tmp
else
	clean_bats_tmp =
endif

bats_env = TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y \
	TASTY_CLEVELAND_NODE_ENDPOINT=$(NODE_ENDPOINT) \
	TASTY_CLEVELAND_MONEYBAG_SECRET_KEY=$(MONEYBAG_SECRET_KEY)

all:
	$(call build_all,)
test-all:
	$(call test_all,)
stylish:
	find code/ examples/ -name '.stack-work' -prune -o -name '*.hs' -exec stylish-haskell -i {} \;
lint:
	scripts/lint.sh
clean:
	$(call clean_all,)
bench:
	$(call bench_all,)
test-binaries-bats:
	PATH="$(shell $(call get_path,morley)):$$PATH" bats scripts/test-binaries.bats
test-docker-bats:
	bats scripts/test-docker.bats
	$(call clean_bats_tmp,)
test-encoding-bats:
	$(bats_env) PATH="$(shell $(call get_path,morley)):$$PATH" bats scripts/test-encoding.bats
test-morley-client-bats:
	$(bats_env) PATH="$(shell $(call get_path,morley-client)):$$PATH" bats --tap scripts/test-morley-client.bats
	cat test-morley-client.log
	$(call clean_bats_tmp,)
clean-bats-tmp:
	rm -rf tmp.*/ bats_db.json stacks/ test-morley-client.log || true
